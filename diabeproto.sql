-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 09-07-2019 a las 01:58:07
-- Versión del servidor: 5.7.23
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `diabeproto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad`
--

DROP TABLE IF EXISTS `actividad`;
CREATE TABLE IF NOT EXISTS `actividad` (
  `idActividad` int(11) NOT NULL AUTO_INCREMENT,
  `Tipo` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Frecuencia` varchar(35) COLLATE utf8_bin DEFAULT NULL,
  `Nutricion_idNutricion` int(11) NOT NULL,
  PRIMARY KEY (`idActividad`,`Nutricion_idNutricion`),
  KEY `fk_Actividad_Nutricion1_idx` (`Nutricion_idNutricion`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `actividad`
--

INSERT INTO `actividad` (`idActividad`, `Tipo`, `Frecuencia`, `Nutricion_idNutricion`) VALUES
(1, 'Salir a caminar ', '30 min al dia', 1),
(2, 'Correr', 'todos los dias 30 min', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `analisis_gen`
--

DROP TABLE IF EXISTS `analisis_gen`;
CREATE TABLE IF NOT EXISTS `analisis_gen` (
  `idAnalisis_gen` int(11) NOT NULL AUTO_INCREMENT,
  `Paciente_idPaciente` int(11) NOT NULL,
  `Paciente_Persona_idPersona` int(11) NOT NULL,
  `Url` varchar(3000) COLLATE utf8_bin DEFAULT NULL,
  `Url2` varchar(3000) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idAnalisis_gen`,`Paciente_idPaciente`,`Paciente_Persona_idPersona`),
  KEY `fk_Analisis_gen_Paciente1_idx` (`Paciente_idPaciente`,`Paciente_Persona_idPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `analisis_gen`
--

INSERT INTO `analisis_gen` (`idAnalisis_gen`, `Paciente_idPaciente`, `Paciente_Persona_idPersona`, `Url`, `Url2`) VALUES
(5, 1, 1, 'static/analisis/1/medical-record.jpg', NULL),
(6, 1, 1, NULL, 'static/analisis/1/Ode To Joy.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `analisis_odonto`
--

DROP TABLE IF EXISTS `analisis_odonto`;
CREATE TABLE IF NOT EXISTS `analisis_odonto` (
  `idAnalisis` int(11) NOT NULL AUTO_INCREMENT,
  `Url` varchar(3000) COLLATE utf8_bin DEFAULT NULL,
  `Url2` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Odontologia_idOdontologia` int(11) NOT NULL,
  PRIMARY KEY (`idAnalisis`,`Odontologia_idOdontologia`),
  KEY `fk_Analisis_odonto_Odontologia1_idx` (`Odontologia_idOdontologia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `antecedentes`
--

DROP TABLE IF EXISTS `antecedentes`;
CREATE TABLE IF NOT EXISTS `antecedentes` (
  `idAntecedentes` int(11) NOT NULL AUTO_INCREMENT,
  `Familiar` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Estado` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Enfermedad` varchar(60) COLLATE utf8_bin DEFAULT NULL,
  `Paciente_idPaciente` int(11) NOT NULL,
  `Paciente_Persona_idPersona` int(11) NOT NULL,
  PRIMARY KEY (`idAntecedentes`,`Paciente_idPaciente`,`Paciente_Persona_idPersona`),
  KEY `fk_Antecedentes_Paciente1_idx` (`Paciente_idPaciente`,`Paciente_Persona_idPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `antecedentes`
--

INSERT INTO `antecedentes` (`idAntecedentes`, `Familiar`, `Estado`, `Enfermedad`, `Paciente_idPaciente`, `Paciente_Persona_idPersona`) VALUES
(1, 'Abulito', 'Vivo', 'Diabetes ', 1, 1),
(2, 'Tio Abuelo', 'Finado', 'Cancer-diabetes', 2, 2),
(3, 'Mama', 'Vivo', 'Diabetes', 3, 3),
(4, 'Padre', 'Muerto', 'Cancer en los riñones', 4, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ant_no_pat`
--

DROP TABLE IF EXISTS `ant_no_pat`;
CREATE TABLE IF NOT EXISTS `ant_no_pat` (
  `idAnt_no_pat` int(11) NOT NULL AUTO_INCREMENT,
  `Paciente_idPaciente` int(11) NOT NULL,
  `Paciente_Persona_idPersona` int(11) NOT NULL,
  `Peso` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Obesidad` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Dieta` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Ejercicio` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Fumar` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Bebida` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Control` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Drogas` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Humo` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Viaje` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Trabajos` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Casa` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Sangre` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Mascotas` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Seguro` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Vacunacion` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Desparacitacion` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idAnt_no_pat`,`Paciente_idPaciente`,`Paciente_Persona_idPersona`),
  KEY `fk_Ant_no_pat_Paciente1_idx` (`Paciente_idPaciente`,`Paciente_Persona_idPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `ant_no_pat`
--

INSERT INTO `ant_no_pat` (`idAnt_no_pat`, `Paciente_idPaciente`, `Paciente_Persona_idPersona`, `Peso`, `Obesidad`, `Dieta`, `Ejercicio`, `Fumar`, `Bebida`, `Control`, `Drogas`, `Humo`, `Viaje`, `Trabajos`, `Casa`, `Sangre`, `Mascotas`, `Seguro`, `Vacunacion`, `Desparacitacion`) VALUES
(11, 1, 1, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL),
(12, 2, 2, 'si', 'si', 'no', 'no', 'no', 'si', 'si', 'si', 'si', 'si', 'si', 'no', 'no', 'si', 'si', 'si', NULL),
(13, 3, 3, 'no', 'si', 'no', 'no', 'no', 'no', 'no', 'no', 'si', 'si', 'si', 'si', 'si', 'no', 'no', 'no', NULL),
(14, 4, 4, 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ant_pat`
--

DROP TABLE IF EXISTS `ant_pat`;
CREATE TABLE IF NOT EXISTS `ant_pat` (
  `idAnt_pat` int(11) NOT NULL AUTO_INCREMENT,
  `Paciente_idPaciente` int(11) NOT NULL,
  `Paciente_Persona_idPersona` int(11) NOT NULL,
  `Diabetes` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Cardiopatia` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Hipotiroidismo` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Dislipidemia` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Hepaticas` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Convulsion` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Cancer` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Transfucionales` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Quirurgicas` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Hipertension` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Insuficiencia` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Hipertiroidismo` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Marcapasos` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Renales` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Artritis` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Dentales` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Traumaticos` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Alergicos` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idAnt_pat`,`Paciente_idPaciente`,`Paciente_Persona_idPersona`),
  KEY `fk_Ant_pat_Paciente1_idx` (`Paciente_idPaciente`,`Paciente_Persona_idPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `ant_pat`
--

INSERT INTO `ant_pat` (`idAnt_pat`, `Paciente_idPaciente`, `Paciente_Persona_idPersona`, `Diabetes`, `Cardiopatia`, `Hipotiroidismo`, `Dislipidemia`, `Hepaticas`, `Convulsion`, `Cancer`, `Transfucionales`, `Quirurgicas`, `Hipertension`, `Insuficiencia`, `Hipertiroidismo`, `Marcapasos`, `Renales`, `Artritis`, `Dentales`, `Traumaticos`, `Alergicos`) VALUES
(1, 1, 1, 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si'),
(2, 2, 2, 'no', 'no', 'no', 'no', 'si', 'si', 'no', 'si', 'no', 'si', 'no', 'si', 'no', 'si', 'no', 'si', 'no', 'no'),
(3, 3, 3, 'no', 'si', 'no', 'si', 'si', 'si', 'si', 'si', 'no', 'si', 'no', 'si', 'si', 'no', 'no', 'no', 'si', 'si'),
(4, 4, 4, 'si', 'no', 'no', 'no', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'si', 'no', 'no', 'no', 'no');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calendario`
--

DROP TABLE IF EXISTS `calendario`;
CREATE TABLE IF NOT EXISTS `calendario` (
  `idCalendario` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(60) COLLATE utf8_bin DEFAULT NULL,
  `Fecha` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `Doctor_idDoctor` int(11) NOT NULL,
  PRIMARY KEY (`idCalendario`,`Doctor_idDoctor`),
  KEY `fk_Calendario_Doctor1_idx` (`Doctor_idDoctor`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `calendario`
--

INSERT INTO `calendario` (`idCalendario`, `Nombre`, `Fecha`, `Doctor_idDoctor`) VALUES
(12, 'Oscar Aguilar Nopal', '22:15 2018/12/31', 3),
(13, 'Mauricio Arturo Ramos Rodriguez', '16:30 2019/02/05', 1),
(14, 'Maria de los santos Perez Evil', '18:40 2019/02/13', 1),
(15, 'Mauricio Arturo Ramos Rodriguez', '14:48 2019/01/18', 1),
(16, 'Mauricio Arturo Ramos Rodriguez', '22:48 2019/01/30', 1),
(17, 'Oscar Aguilar Nopal', '12:48 2019/01/17', 1),
(18, 'Roberto Merino Flores', '22:21 2019/07/11', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dieta`
--

DROP TABLE IF EXISTS `dieta`;
CREATE TABLE IF NOT EXISTS `dieta` (
  `idDieta` int(11) NOT NULL AUTO_INCREMENT,
  `Desayuno` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Colacion1` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Comida` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Colacion2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Cena` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Nutricion_idNutricion` int(11) NOT NULL,
  PRIMARY KEY (`idDieta`,`Nutricion_idNutricion`),
  KEY `fk_Dieta_Nutricion1_idx` (`Nutricion_idNutricion`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `dieta`
--

INSERT INTO `dieta` (`idDieta`, `Desayuno`, `Colacion1`, `Comida`, `Colacion2`, `Cena`, `Nutricion_idNutricion`) VALUES
(1, 'Papas fritas', 'dedos de queso fritos', 'Ensalada con aderezo cesar', 'Nachos extra queso', 'Pechuga azada con verdura hervida', 1),
(2, 'Pizza', 'Ensalada de frutos rojos', 'Cemitas de carnitas', 'Tacos de carne azada', 'Cereal con leche', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doctor`
--

DROP TABLE IF EXISTS `doctor`;
CREATE TABLE IF NOT EXISTS `doctor` (
  `idDoctor` int(11) NOT NULL,
  `usrdoc` varchar(8) COLLATE utf8_bin NOT NULL,
  `psw` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idDoctor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `doctor`
--

INSERT INTO `doctor` (`idDoctor`, `usrdoc`, `psw`) VALUES
(1, 'Juan', '$2y$10$JJKtnT.mCRH5k8ZFojqbE.xz8Ufzy/SOJcM0t2nn.lVXKU17F03O6'),
(2, 'Pedro', '$2y$10$qijUDcOlaZzegrf0.C6/ROR9VZlBX5/0sKsGQ62YGkv0yzIL41cq2'),
(3, 'Martha', '$2y$10$8NCQH.mfWSMLnGefNDXtE.Stb20RImKJV3qTRO37dgULUAW7KueWa'),
(4, 'Lupe', '$2y$10$30JNuomVvAIzLemk1Ae4k.pxvQTleY/LFMsjCJxEweiNigz1k1r96');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frecuencia`
--

DROP TABLE IF EXISTS `frecuencia`;
CREATE TABLE IF NOT EXISTS `frecuencia` (
  `idFrecuencia` int(11) NOT NULL AUTO_INCREMENT,
  `Frutas` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `Verduras` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `Cereales` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `AOA` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `Lacteos` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `Aceites` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `Azucares` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `Postres` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `Jugos` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `AguaN` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `AguaS` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `Cafe` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `Te` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `Otros` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `Alcohol` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `Nutricion_idNutricion` int(11) NOT NULL,
  PRIMARY KEY (`idFrecuencia`,`Nutricion_idNutricion`),
  KEY `fk_Frecuencia_Nutricion1_idx` (`Nutricion_idNutricion`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `frecuencia`
--

INSERT INTO `frecuencia` (`idFrecuencia`, `Frutas`, `Verduras`, `Cereales`, `AOA`, `Lacteos`, `Aceites`, `Azucares`, `Postres`, `Jugos`, `AguaN`, `AguaS`, `Cafe`, `Te`, `Otros`, `Alcohol`, `Nutricion_idNutricion`) VALUES
(1, '2 veces', '1 vez ', '4 veces', 'ninguna', '3 veces', '2 veces', '3 veces ', '4 veces ', '4 veces ', '9 veces', '2 veces', '4 veces', '3 veces', NULL, '1 ves', 1),
(2, '5 veces', '4 veces', '6 veces', '2 veces', '3 veces', '5 veces', '7 veces', '8 veces', '3 veces', '10 veces', '2 veces', '8 veces', '7 veces', NULL, '1 vez', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grafica`
--

DROP TABLE IF EXISTS `grafica`;
CREATE TABLE IF NOT EXISTS `grafica` (
  `idGraf` int(11) NOT NULL AUTO_INCREMENT,
  `Paciente_idPaciente` int(11) NOT NULL,
  `Paciente_Persona_idPersona` int(11) NOT NULL,
  `Glucosa` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `Fecha` date DEFAULT NULL,
  PRIMARY KEY (`idGraf`,`Paciente_idPaciente`,`Paciente_Persona_idPersona`),
  KEY `fk_Grafica_Paciente1_idx` (`Paciente_idPaciente`,`Paciente_Persona_idPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `grafica`
--

INSERT INTO `grafica` (`idGraf`, `Paciente_idPaciente`, `Paciente_Persona_idPersona`, `Glucosa`, `Fecha`) VALUES
(1, 1, 1, '800', '2019-01-12'),
(2, 1, 1, '790', '2019-01-17'),
(3, 1, 1, '78', '2019-01-25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medicamentos`
--

DROP TABLE IF EXISTS `medicamentos`;
CREATE TABLE IF NOT EXISTS `medicamentos` (
  `idMedicamentos` int(11) NOT NULL AUTO_INCREMENT,
  `Paciente_idPaciente` int(11) NOT NULL,
  `Paciente_Persona_idPersona` int(11) NOT NULL,
  `Medicamento` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Dosis` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Horario` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `Observaciones` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idMedicamentos`,`Paciente_idPaciente`,`Paciente_Persona_idPersona`),
  KEY `fk_Medicamentos_Paciente1_idx` (`Paciente_idPaciente`,`Paciente_Persona_idPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `medicamentos`
--

INSERT INTO `medicamentos` (`idMedicamentos`, `Paciente_idPaciente`, `Paciente_Persona_idPersona`, `Medicamento`, `Dosis`, `Horario`, `Observaciones`) VALUES
(1, 3, 3, 'dfsdf', 'sdfds', 'sdf', NULL),
(2, 1, 1, 'aspirina', '2 pastillas', 'cada 8 horas', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nutricion`
--

DROP TABLE IF EXISTS `nutricion`;
CREATE TABLE IF NOT EXISTS `nutricion` (
  `idNutricion` int(11) NOT NULL AUTO_INCREMENT,
  `Diagnostico` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Peso` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `Cintura` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `Cadera` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `Notas` varchar(8000) COLLATE utf8_bin DEFAULT NULL,
  `Paciente_idPaciente` int(11) NOT NULL,
  `Paciente_Persona_idPersona` int(11) NOT NULL,
  PRIMARY KEY (`idNutricion`,`Paciente_idPaciente`,`Paciente_Persona_idPersona`),
  KEY `fk_Nutricion_Paciente1_idx` (`Paciente_idPaciente`,`Paciente_Persona_idPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `nutricion`
--

INSERT INTO `nutricion` (`idNutricion`, `Diagnostico`, `Peso`, `Cintura`, `Cadera`, `Notas`, `Paciente_idPaciente`, `Paciente_Persona_idPersona`) VALUES
(1, 'Perdida de apetito', '87kg', '86cm', '89cm', 'Como comida chatarra', 1, 1),
(2, 'Perdida de peso ', '63kg', '76cm', '76cm', 'No tiene buenos habitos alimenticios', 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `odontologia`
--

DROP TABLE IF EXISTS `odontologia`;
CREATE TABLE IF NOT EXISTS `odontologia` (
  `idOdontologia` int(11) NOT NULL AUTO_INCREMENT,
  `Motivo` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Diagnostico` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Enfermedades` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `Alergias` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Notas` varchar(8000) COLLATE utf8_bin DEFAULT NULL,
  `Paciente_idPaciente` int(11) NOT NULL,
  `Paciente_Persona_idPersona` int(11) NOT NULL,
  PRIMARY KEY (`idOdontologia`,`Paciente_idPaciente`,`Paciente_Persona_idPersona`),
  KEY `fk_Odontologia_Paciente1_idx` (`Paciente_idPaciente`,`Paciente_Persona_idPersona`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

DROP TABLE IF EXISTS `paciente`;
CREATE TABLE IF NOT EXISTS `paciente` (
  `idPaciente` int(11) NOT NULL AUTO_INCREMENT,
  `Persona_idPersona` int(11) NOT NULL,
  `Diagnostico` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Padecimiento` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `Exploracion` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Notas` varchar(8000) COLLATE utf8_bin DEFAULT NULL,
  `Interconsultas` varchar(8000) COLLATE utf8_bin DEFAULT NULL,
  `idPodologo` int(11) DEFAULT NULL,
  `idNutriologo` int(11) DEFAULT NULL,
  `idPsicologo` int(11) DEFAULT NULL,
  `idOdontologo` int(11) DEFAULT NULL,
  `foto` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idPaciente`,`Persona_idPersona`),
  KEY `fk_Paciente_Persona1_idx` (`Persona_idPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`idPaciente`, `Persona_idPersona`, `Diagnostico`, `Padecimiento`, `Exploracion`, `Notas`, `Interconsultas`, `idPodologo`, `idNutriologo`, `idPsicologo`, `idOdontologo`, `foto`) VALUES
(1, 1, 'Diabetes Tipo A', 'Dolor en los musculos', 'Examen de sangre', ' Come comida chatarra fue con otro nutriologo en el 2009 \"CASI MUERE\"', NULL, 2, 3, 4, NULL, 'static/foto/1/crines rojochica.jpg'),
(2, 2, 'Diabetes tipo Z', 'Dolor de Estomago', 'Examen de orina', ' Exceso de azucar en el estomagod', NULL, NULL, 3, NULL, NULL, NULL),
(3, 3, 'Diabetes tipo G', 'Dolor de talones', 'Tacto físico', 'Demasiada azúcar en los dedos de los pies', NULL, 2, NULL, NULL, NULL, NULL),
(4, 4, 'Diabetes tipo F', 'Alucinaciones', 'Pruebas psciometricas', 'Ve personas muertas', NULL, NULL, NULL, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

DROP TABLE IF EXISTS `persona`;
CREATE TABLE IF NOT EXISTS `persona` (
  `idPersona` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(60) COLLATE utf8_bin DEFAULT NULL,
  `Fecha` date DEFAULT NULL,
  `Edad` varchar(3) COLLATE utf8_bin DEFAULT NULL,
  `Genero` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `Lugar` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Residencia` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Domicilio` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Ocupacion` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Estado` varchar(9) COLLATE utf8_bin DEFAULT NULL,
  `Escolaridad` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `Religion` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `Telefono` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `Email` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`idPersona`, `Nombre`, `Fecha`, `Edad`, `Genero`, `Lugar`, `Residencia`, `Domicilio`, `Ocupacion`, `Estado`, `Escolaridad`, `Religion`, `Telefono`, `Email`) VALUES
(1, 'Mauricio Arturo Ramos Rodriguez', '2018-11-12', '25', 'Masculino', 'Puebla', 'Puebla', 'Otomi 8A, via chiautempan', 'Estudiante', 'Soltero', 'Licenciatura', 'Catolico', '6-36-11-20', 'mau_90@gmail.com'),
(2, 'Oscar Aguilar Nopal', '2018-12-14', '25', 'Masculino', 'Hidalgo', 'Puebla', 'Privada 24 poniente, lázaro cárdenas', 'Estudiante', 'Soltero', 'Maestria ', 'Cristiano', '2221034277', 'st.nu,ozkr117@gmail.com'),
(3, 'Roberto Merino Flores', '2018-12-21', '24', 'Homosexual', 'Puebla', 'Puebla', 'Av. siempre viva #345, angelopolis', 'Obrero', 'Casado', 'Primaria', 'Budista', '3-46-78-90', 'amidamaru_90@outlook.com'),
(4, 'Maria de los santos Perez Evil', '2018-12-03', '34', 'Femenino', 'Veracruz', 'Puebla', 'Calle sin asignacion numero 7, fuentes fantas', 'Cocinera', 'Casada', 'Bachilletato', 'Catolica', '2236429364', 'perez_evil34@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pie_der`
--

DROP TABLE IF EXISTS `pie_der`;
CREATE TABLE IF NOT EXISTS `pie_der` (
  `idPie_der` int(11) NOT NULL AUTO_INCREMENT,
  `Plantar` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Dorsal` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Talar` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Onicrocriptosis` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Onicomicosis` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Onicograifosis` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Bullosis` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Ulceras` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Necrosis` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Grietas` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Lesion` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Anhidrosis` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Tiñas` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Infecciones` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Dedosgar` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Dedosmar` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Hallux` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Infraductos` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Supraductos` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Hipercargas` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Pulsope` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Pulsoti` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Llenado` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Edena` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Sensacion` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Podologia_idPodologia` int(11) NOT NULL,
  PRIMARY KEY (`idPie_der`),
  KEY `fk_Pie_der_Podologia1_idx` (`Podologia_idPodologia`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `pie_der`
--

INSERT INTO `pie_der` (`idPie_der`, `Plantar`, `Dorsal`, `Talar`, `Onicrocriptosis`, `Onicomicosis`, `Onicograifosis`, `Bullosis`, `Ulceras`, `Necrosis`, `Grietas`, `Lesion`, `Anhidrosis`, `Tiñas`, `Infecciones`, `Dedosgar`, `Dedosmar`, `Hallux`, `Infraductos`, `Supraductos`, `Hipercargas`, `Pulsope`, `Pulsoti`, `Llenado`, `Edena`, `Sensacion`, `Podologia_idPodologia`) VALUES
(1, '5', '6', '8', '7', '4', '3', '7', '9', '7', '5', '9', '6', '0', '9', '7', NULL, '7', '4', '7', '4', '3', '2', '1', '8', '3', 1),
(2, '1', '8', '6', '6', '4', '6', '9', '5', '2', '5', '8', '6', '4', '2', '4', '6', '8', '6', '3', '6', '8', '5', '0', '9', '7', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pie_izq`
--

DROP TABLE IF EXISTS `pie_izq`;
CREATE TABLE IF NOT EXISTS `pie_izq` (
  `idPie_der` int(11) NOT NULL AUTO_INCREMENT,
  `Plantar` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Dorsal` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Talar` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Onicrocriptosis` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Onicomicosis` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Onicograifosis` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Bullosis` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Ulceras` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Necrosis` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Grietas` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Lesion` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Anhidrosis` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Tiñas` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Infecciones` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Dedosgar` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Dedosmar` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Hallux` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Infraductos` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Supraductos` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Hipercargas` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Pulsope` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Pulsoti` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Llenado` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Edena` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Sensacion` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `Podologia_idPodologia` int(11) NOT NULL,
  PRIMARY KEY (`idPie_der`),
  KEY `fk_Pie_izq_Podologia1_idx` (`Podologia_idPodologia`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `pie_izq`
--

INSERT INTO `pie_izq` (`idPie_der`, `Plantar`, `Dorsal`, `Talar`, `Onicrocriptosis`, `Onicomicosis`, `Onicograifosis`, `Bullosis`, `Ulceras`, `Necrosis`, `Grietas`, `Lesion`, `Anhidrosis`, `Tiñas`, `Infecciones`, `Dedosgar`, `Dedosmar`, `Hallux`, `Infraductos`, `Supraductos`, `Hipercargas`, `Pulsope`, `Pulsoti`, `Llenado`, `Edena`, `Sensacion`, `Podologia_idPodologia`) VALUES
(1, '6', '7', '8', '9', '7', '5', '8', '0', '7', '6', '4', '2', '4', '6', '7', '9', '7', '5', '3', '6', '8', '5', '3', '1', '0', 1),
(2, '8', '6', '4', '6', '8', '56', '3', '2', '5', '7', '9', '6', '5', '4', '2', '5', '8', '6', '3', '2', '6', '9', '5', '7', '4', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `podologia`
--

DROP TABLE IF EXISTS `podologia`;
CREATE TABLE IF NOT EXISTS `podologia` (
  `idPodologia` int(11) NOT NULL AUTO_INCREMENT,
  `Motivo` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Diagnostico` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Observaciones` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Tratamiento` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Recomendaciones` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Notas` varchar(8000) COLLATE utf8_bin DEFAULT NULL,
  `Paciente_idPaciente` int(11) NOT NULL,
  `Paciente_Persona_idPersona` int(11) NOT NULL,
  PRIMARY KEY (`idPodologia`,`Paciente_idPaciente`,`Paciente_Persona_idPersona`),
  KEY `fk_Podologia_Paciente1_idx` (`Paciente_idPaciente`,`Paciente_Persona_idPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `podologia`
--

INSERT INTO `podologia` (`idPodologia`, `Motivo`, `Diagnostico`, `Observaciones`, `Tratamiento`, `Recomendaciones`, `Notas`, `Paciente_idPaciente`, `Paciente_Persona_idPersona`) VALUES
(1, 'Uña enterrada', 'Operacion de extraccion ', 'Peligro de muerte', 'Operacion a pie abierto', 'No usar talco', 'NO se lava los pies', 1, 1),
(2, 'Dedos engagrenados', 'Perdida de pie', 'No tiene sensibilidad', 'Cirugia', 'No comer carne roja ', 'Amputamiento de pie completo', 3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `psicologia`
--

DROP TABLE IF EXISTS `psicologia`;
CREATE TABLE IF NOT EXISTS `psicologia` (
  `idPsicologia` int(11) NOT NULL AUTO_INCREMENT,
  `Notas` varchar(8000) COLLATE utf8_bin DEFAULT NULL,
  `Paciente_idPaciente` int(11) NOT NULL,
  `Paciente_Persona_idPersona` int(11) NOT NULL,
  PRIMARY KEY (`idPsicologia`,`Paciente_idPaciente`,`Paciente_Persona_idPersona`),
  KEY `fk_Psicologia_Paciente1_idx` (`Paciente_idPaciente`,`Paciente_Persona_idPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `psicologia`
--

INSERT INTO `psicologia` (`idPsicologia`, `Notas`, `Paciente_idPaciente`, `Paciente_Persona_idPersona`) VALUES
(1, 'EL paciente esta muy Cu-Cu', 1, 1),
(2, 'No duerme bien y cree q es astronauta', 4, 4);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD CONSTRAINT `fk_Actividad_Nutricion1` FOREIGN KEY (`Nutricion_idNutricion`) REFERENCES `nutricion` (`idNutricion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `analisis_gen`
--
ALTER TABLE `analisis_gen`
  ADD CONSTRAINT `fk_Analisis_gen_Paciente1` FOREIGN KEY (`Paciente_idPaciente`,`Paciente_Persona_idPersona`) REFERENCES `paciente` (`idPaciente`, `Persona_idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `analisis_odonto`
--
ALTER TABLE `analisis_odonto`
  ADD CONSTRAINT `fk_Analisis_odonto_Odontologia1` FOREIGN KEY (`Odontologia_idOdontologia`) REFERENCES `odontologia` (`idOdontologia`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `antecedentes`
--
ALTER TABLE `antecedentes`
  ADD CONSTRAINT `fk_Antecedentes_Paciente1` FOREIGN KEY (`Paciente_idPaciente`,`Paciente_Persona_idPersona`) REFERENCES `paciente` (`idPaciente`, `Persona_idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ant_no_pat`
--
ALTER TABLE `ant_no_pat`
  ADD CONSTRAINT `fk_Ant_no_pat_Paciente1` FOREIGN KEY (`Paciente_idPaciente`,`Paciente_Persona_idPersona`) REFERENCES `paciente` (`idPaciente`, `Persona_idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ant_pat`
--
ALTER TABLE `ant_pat`
  ADD CONSTRAINT `fk_Ant_pat_Paciente1` FOREIGN KEY (`Paciente_idPaciente`,`Paciente_Persona_idPersona`) REFERENCES `paciente` (`idPaciente`, `Persona_idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `calendario`
--
ALTER TABLE `calendario`
  ADD CONSTRAINT `fk_Calendario_Doctor1` FOREIGN KEY (`Doctor_idDoctor`) REFERENCES `doctor` (`idDoctor`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dieta`
--
ALTER TABLE `dieta`
  ADD CONSTRAINT `fk_Dieta_Nutricion1` FOREIGN KEY (`Nutricion_idNutricion`) REFERENCES `nutricion` (`idNutricion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `frecuencia`
--
ALTER TABLE `frecuencia`
  ADD CONSTRAINT `fk_Frecuencia_Nutricion1` FOREIGN KEY (`Nutricion_idNutricion`) REFERENCES `nutricion` (`idNutricion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `grafica`
--
ALTER TABLE `grafica`
  ADD CONSTRAINT `fk_Grafica_Paciente1` FOREIGN KEY (`Paciente_idPaciente`,`Paciente_Persona_idPersona`) REFERENCES `paciente` (`idPaciente`, `Persona_idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `medicamentos`
--
ALTER TABLE `medicamentos`
  ADD CONSTRAINT `fk_Medicamentos_Paciente1` FOREIGN KEY (`Paciente_idPaciente`,`Paciente_Persona_idPersona`) REFERENCES `paciente` (`idPaciente`, `Persona_idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `nutricion`
--
ALTER TABLE `nutricion`
  ADD CONSTRAINT `fk_Nutricion_Paciente1` FOREIGN KEY (`Paciente_idPaciente`,`Paciente_Persona_idPersona`) REFERENCES `paciente` (`idPaciente`, `Persona_idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `odontologia`
--
ALTER TABLE `odontologia`
  ADD CONSTRAINT `fk_Odontologia_Paciente1` FOREIGN KEY (`Paciente_idPaciente`,`Paciente_Persona_idPersona`) REFERENCES `paciente` (`idPaciente`, `Persona_idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD CONSTRAINT `fk_Paciente_Persona1` FOREIGN KEY (`Persona_idPersona`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pie_der`
--
ALTER TABLE `pie_der`
  ADD CONSTRAINT `fk_Pie_der_Podologia1` FOREIGN KEY (`Podologia_idPodologia`) REFERENCES `podologia` (`idPodologia`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pie_izq`
--
ALTER TABLE `pie_izq`
  ADD CONSTRAINT `fk_Pie_izq_Podologia1` FOREIGN KEY (`Podologia_idPodologia`) REFERENCES `podologia` (`idPodologia`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `podologia`
--
ALTER TABLE `podologia`
  ADD CONSTRAINT `fk_Podologia_Paciente1` FOREIGN KEY (`Paciente_idPaciente`,`Paciente_Persona_idPersona`) REFERENCES `paciente` (`idPaciente`, `Persona_idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `psicologia`
--
ALTER TABLE `psicologia`
  ADD CONSTRAINT `fk_Psicologia_Paciente1` FOREIGN KEY (`Paciente_idPaciente`,`Paciente_Persona_idPersona`) REFERENCES `paciente` (`idPaciente`, `Persona_idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
