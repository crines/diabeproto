<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class bases_model extends CI_Model {

  function __construct()
    {parent::__construct();}

  public function agregahash($data){
    $cadena="update doctor set psw ='".$data['pass']."' WHERE doctor.idDoctor = '".$data['id']."'";
    $this->db->query($cadena);
  }

  public function obtenerDoc($data){
    $cadena="select * from Doctor where usrdoc='".$data['user']."'";
     $query = $this->db->query($cadena);
     if ($query->num_rows() > 0)
         {return $query;
     }else{
       return FALSE;
     }
  }

  public function validaLogin($data){
   $cadena="select * from Doctor where usrdoc='".$data['user']."' and psw='".$data['password']."'";
    $query = $this->db->query($cadena);
    if ($query->num_rows() > 0)
        {return $query;
    }else{
      return FALSE;
    }
  }

  public function getPacientes()
  {
    $cadena="select  p.Nombre, pa.Persona_idPersona from persona as p inner join paciente as pa on p.idPersona=pa.Persona_idPersona order by idPaciente";
    $query = $this->db->query($cadena);
    return $query;
  }
  public function getPacientesPod($id)
  {
    $cadena="select  p.Nombre, pa.Persona_idPersona from persona as p inner join paciente as pa on p.idPersona=pa.Persona_idPersona WHERE pa.idPodologo = '".$id."' order by idPaciente";
    $query = $this->db->query($cadena);
    return $query;
  }
  public function getPacientesNut($id)
  {
    $cadena="select  p.Nombre, pa.Persona_idPersona from persona as p inner join paciente as pa on p.idPersona=pa.Persona_idPersona WHERE pa.idNutriologo = '".$id."' order by idPaciente";
    $query = $this->db->query($cadena);
    return $query;
  }
  public function getPacientesPsy($id)
  {
    $cadena="select  p.Nombre, pa.Persona_idPersona from persona as p inner join paciente as pa on p.idPersona=pa.Persona_idPersona WHERE pa.idPsicologo = '".$id."' order by idPaciente";
    $query = $this->db->query($cadena);
    return $query;
  }

  public function getidDoc($doc){
    $cadena= "select idDoctor FROM doctor WHERE usrdoc = '".$doc."'";
    $query = $this->db->query($cadena);
    return $query;
  }



/*---------------------------------------------------------------
----------------------------------------------------------------
--------------------------------------------------------------*/
  /*funciones para buscadorv2*/
  public function getperson($busca){
    $cadena= "select * FROM persona as p JOIN paciente as pa JOIN ant_pat as pat JOIN ant_no_pat as npat JOIN antecedentes as ant WHERE pa.Persona_idPersona='".$busca."' and p.idPersona='".$busca."'
    and pat.Paciente_Persona_idPersona='".$busca."' and npat.Paciente_Persona_idPersona='".$busca."'and ant.Paciente_Persona_idPersona='".$busca."'";
    $query = $this->db->query($cadena);
    if ($query->num_rows() > 0)
        {return $query;
    }else{
      return FALSE;
    }
  }

public function getFicha($busca){
  $cadena= "select * FROM persona as p JOIN paciente as pa WHERE pa.Persona_idPersona='".$busca."' and p.idPersona='".$busca."' ";
  $query = $this->db->query($cadena);
  if ($query->num_rows() > 0)
      {return $query;
  }else{
    return FALSE;
  }
}

public function getAnte($busca){
  $cadena= "select * FROM ant_pat as pat JOIN ant_no_pat as npat JOIN antecedentes as ant WHERE pat.Paciente_Persona_idPersona='".$busca."' and npat.Paciente_Persona_idPersona='".$busca."'and ant.Paciente_Persona_idPersona='".$busca."' ";
  $query = $this->db->query($cadena);
  if ($query->num_rows() > 0)
      {return $query;
  }else{
    return FALSE;
  }
}


  public function getmedicament($busca){
      $cadena= "select * FROM medicamentos WHERE Paciente_Persona_idPersona='".$busca."'";
      $query = $this->db->query($cadena);
      if ($query->num_rows() > 0)
          {return $query;
      }else{
        return FALSE;
      }
    }

    public function Agmedicamen($data){
      $campos = array(
        'Medicamento'=> $data['nombre'],
        'Dosis'=> $data['dosis'],
        'Horario'=> $data['horario'],
        'Observaciones'=> $data['observaciones'],
        'Paciente_idPaciente'=> $data['idpaci'],
        'Paciente_Persona_idPersona'=> $data['idp']
      );
      $this->db->insert('medicamentos',$campos);
    }
    public function Edmedicamen($data){
      $campos = array(
        'Medicamento'=> $data['nombre'],
        'Dosis'=> $data['dosis'],
        'Horario'=> $data['horario'],
        'Observaciones'=> $data['observaciones'],
      );
      $this->db->update('medicamentos', $campos, array('idMedicamentos'=>$data['idm'], 'Paciente_idPaciente'=> $data['idpaci'], 'Paciente_Persona_idPersona'=> $data['idp']));
    }


    public function deleteMedicam($data){
      $cadena="delete FROM medicamentos WHERE medicamentos.idMedicamentos='".$data."'";
      $this->db->query($cadena);
    }

    public function ObtidM(){
      $cadena="select idMedicamentos from medicamentos ORDER BY idMedicamentos DESC LIMIT 1";
      $cadenaid = $this->db->query($cadena);
        if ($cadenaid->num_rows() > 0)
            {return $cadenaid;
        }else{
          return FALSE;
        }
    }
    /*-------agregar quitar tipo de doctor--------------*/
    public function agregaMpodo($data){
      $cadena="update paciente set idPodologo ='".$data['tipo']."' WHERE paciente.Persona_idPersona = '".$data['idp']."'";
      $this->db->query($cadena);
    }

    public function agregaMnutri($data){
      $cadena="update paciente set idNutriologo ='".$data['tipo']."' WHERE paciente.Persona_idPersona = '".$data['idp']."'";
      $this->db->query($cadena);
    }
    public function agregaMpsico($data){
      $cadena="update paciente set idPsicologo ='".$data['tipo']."' WHERE paciente.Persona_idPersona = '".$data['idp']."'";
      $this->db->query($cadena);
    }
    public function agregaModon($data){
      $cadena="update paciente set idOdontologo ='".$data['tipo']."' WHERE paciente.Persona_idPersona = '".$data['idp']."'";
      $this->db->query($cadena);
    }
    //---------quitar
    public function quitaMpodo($data){
      $cadena="update paciente set idPodologo=null WHERE paciente.Persona_idPersona = '".$data['idp']."'";
      $this->db->query($cadena);
    }

    public function quitaMnutri($data){
      $cadena="update paciente set idNutriologo=null WHERE paciente.Persona_idPersona = '".$data['idp']."'";
      $this->db->query($cadena);
    }
    public function quitaMpsico($data){
      $cadena="update paciente set idPsicologo=null WHERE paciente.Persona_idPersona = '".$data['idp']."'";
      $this->db->query($cadena);
    }
    public function quitaModon($data){
      $cadena="update paciente set idOdontologo=null WHERE paciente.Persona_idPersona = '".$data['idp']."'";
      $this->db->query($cadena);
    }

    /*-------------------------Nutricion------------------------------------
    ---------------------------------------------------------------
    --------------------------------------------------------------*/

    public function getpersonutri($busca){
      $cadena= "select * FROM nutricion JOIN persona WHERE nutricion.Paciente_Persona_idPersona='".$busca."' and persona.idPersona='".$busca."'";
      $query = $this->db->query($cadena);
      if ($query->num_rows() > 0)
          {return $query;
      }else{
        return FALSE;
      }
    }

    public function getActividad($busca){
      $cadena= "select * FROM actividad JOIN nutricion WHERE nutricion.Paciente_Persona_idPersona='".$busca."' and Nutricion_idNutricion='".$busca."'";
      $query = $this->db->query($cadena);
      if ($query->num_rows() > 0)
          {return $query;
      }else{
        return FALSE;
      }
    }

    public function getDieta($busca){
  $cadena= "select * FROM dieta JOIN nutricion WHERE nutricion.Paciente_Persona_idPersona='".$busca."' and Nutricion_idNutricion='".$busca."'";
      $query = $this->db->query($cadena);
      if ($query->num_rows() > 0)
          {return $query;
      }else{
        return FALSE;
      }
    }

    public function getFrecuencia($busca){
  $cadena= "select * FROM frecuencia JOIN nutricion WHERE nutricion.Paciente_Persona_idPersona='".$busca."' and Nutricion_idNutricion='".$busca."'";
      $query = $this->db->query($cadena);
      if ($query->num_rows() > 0)
          {return $query;
      }else{
        return FALSE;
      }
    }

    /*-------------------------Podologia------------------------------------
    ---------------------------------------------------------------
    --------------------------------------------------------------*/

    public function getpersonpodo($busca){
      $cadena= "select * FROM podologia JOIN persona WHERE podologia.Paciente_Persona_idPersona='".$busca."' and persona.idPersona='".$busca."'";
      $query = $this->db->query($cadena);
      if ($query->num_rows() > 0)
          {return $query;
      }else{
        return FALSE;
      }
    }

    public function getpieD($busca){
      $cadena= "select * FROM pie_der JOIN podologia WHERE podologia.Paciente_Persona_idPersona='".$busca."' and Podologia_idPodologia='".$busca."'";
      $query = $this->db->query($cadena);
      if ($query->num_rows() > 0)
          {return $query;
      }else{
        return FALSE;
      }
    }

    public function getpieI($busca){
  $cadena= "select * FROM pie_izq JOIN podologia WHERE podologia.Paciente_Persona_idPersona='".$busca."' and Podologia_idPodologia='".$busca."'";
      $query = $this->db->query($cadena);
      if ($query->num_rows() > 0)
          {return $query;
      }else{
        return FALSE;
      }
    }

    /*-------------------------Psicologia------------------------------------
    ---------------------------------------------------------------
    --------------------------------------------------------------*/

    public function getpersonpsico($busca){
      $cadena= "select * FROM psicologia JOIN persona WHERE psicologia.Paciente_Persona_idPersona='".$busca."' and persona.idPersona='".$busca."'";
      $query = $this->db->query($cadena);
      if ($query->num_rows() > 0)
          {return $query;
      }else{
        return FALSE;
      }
    }
    /*-------------------------------------------------------------
    ---------------------------------------------------------------
    --------------------------------------------------------------*/
    public function getidnu(){
      $cadena="select idNutricion from nutricion ORDER BY idNutricion DESC LIMIT 1";
      $cadenaid = $this->db->query($cadena);
        if ($cadenaid->num_rows() > 0)
            {return $cadenaid;
        }else{
          return FALSE;
        }
      }

      public function getidpo(){
        $cadena="select idPodologia from podologia ORDER BY idPodologia DESC LIMIT 1";
        $cadenaid = $this->db->query($cadena);
          if ($cadenaid->num_rows() > 0)
              {return $cadenaid;
          }else{
            return FALSE;
          }
        }


    public function getid(){
      $cadena="select idPersona from persona ORDER BY idPersona DESC LIMIT 1";
      $cadenaid = $this->db->query($cadena);
        if ($cadenaid->num_rows() > 0)
            {return $cadenaid;
        }else{
          return FALSE;
        }
      }

      public function getidpas(){
        $cadena="select idPaciente from paciente ORDER BY idPaciente DESC LIMIT 1";
        $cadenaid = $this->db->query($cadena);
          if ($cadenaid->num_rows() > 0)
              {return $cadenaid;
          }else{
            return FALSE;
          }
        }

  public function SetNotas($data){
    $cadena="update paciente set Notas ='".$data['notas']."' WHERE paciente.Persona_idPersona = '".$data['id']."'";
    $this->db->query($cadena);

}
public function SetInter($data){
  $cadena="update paciente set interconsultas ='".$data['interconsultas']."' WHERE paciente.Persona_idPersona = '".$data['id']."'";
  $this->db->query($cadena);

}

public function insertarper($per){
$campos = array(
  'Nombre'=> $per['nombre'],
  'Fecha'=> $per['fecha'],
  'Edad'=> $per['edad'],
  'Genero'=> $per['genero'],
  'Lugar'=> $per['nacimiento'],
  'Residencia'=> $per['residencia'],
  'Domicilio'=> $per['domicilio'],
  'Ocupacion'=> $per['ocupacion'],
  'Estado'=> $per['estado'],
  'Escolaridad'=> $per['escolaridad'],
  'Religion'=> $per['religion'],
  'Telefono'=> $per['telefono'],
  'Email'=> $per['email']
);
$this->db->insert('persona',$campos);
}

public function insertarpas($pas){
$campos = array(
  'Persona_idPersona'=> $pas['idp'],
  'Diagnostico'=> $pas['Diagnostico'],
  'Padecimiento'=> $pas['Padecimiento'],
  'Exploracion'=> $pas['Exploracion'],
  'Notas'=> $pas['Notas'],
  'Interconsultas'=> $pas['Interconsultas']
);
$this->db->insert('paciente',$campos);
}

public function insertarpat($pat){
$campos = array(
  'Paciente_Persona_idPersona'=>$pat['idp'],
  'Paciente_idPaciente'=>$pat['idpas'],
  'Diabetes'=>$pat['Diabetes'],
  'Cardiopatia'=>$pat['Cardiopatia'],
  'Hipotiroidismo'=>$pat['Hipotiroidismo'],
  'Dislipidemia'=>$pat['Dislipidemia'],
  'Hepaticas'=>$pat['Hepaticas'],
  'Convulsion'=>$pat['Convulsion'],
  'Cancer'=>$pat['Cancer'],
  'Transfucionales'=>$pat['Transfucionales'],
  'Quirurgicas'=>$pat['Quirurgicos'],
  'Hipertension'=>$pat['Hipertension'],
  'Insuficiencia'=>$pat['Insuficiencia'],
  'Hipertiroidismo'=>$pat['Hipertiroidismo'],
  'Marcapasos'=>$pat['Marcapasos'],
  'Renales'=>$pat['Renales'],
  'Artritis'=>$pat['Artritis'],
  'Dentales'=>$pat['Dentales'],
  'Traumaticos'=>$pat['Traumaticos'],
  'Alergicos'=>$pat['Alergicos']
);
$this->db->insert('ant_pat',$campos);
}

public function insertarpatno($patno){
  $campos = array(
    'Paciente_Persona_idPersona'=>$patno['idp'],
    'Paciente_idPaciente'=>$patno['idpas'],
    'Peso'=>$patno['Peso'],
    'Obesidad'=>$patno['Obesidad'],
    'Dieta'=>$patno['Dieta'],
    'Ejercicio'=>$patno['Ejercicio'],
    'Fumar'=>$patno['Fumar'],
    'Bebida'=>$patno['Bebida'],
    'Control'=>$patno['Control'],
    'Drogas'=>$patno['Drogas'],
    'Humo'=>$patno['Humo'],
    'Viaje'=>$patno['Viaje'],
    'Trabajos'=>$patno['Trabajos'],
    'Casa'=>$patno['Casa'],
    'Sangre'=>$patno['Sangre'],
    'Mascotas'=>$patno['Mascotas'],
    'Seguro'=>$patno['Seguro'],
    'Vacunacion'=>$patno['Vacunacion'],
    'Desparacitacion'=>$patno['Desparacitacion']
  );
  $this->db->insert('ant_no_pat',$campos);
  }

public function insertarat($at){
$campos = array(
  'Paciente_Persona_idPersona'=>$at['idp'],
  'Paciente_idPaciente'=>$at['idpas'],
  'Familiar'=> $at['Familiar'],
  'Estado'=> $at['Estado'],
  'Enfermedad'=> $at['Enfermedad']
);
$this->db->insert('antecedentes',$campos);
}

public function Uploimg($data){
  $cadena="insert analisis_gen set Url ='".$data['dir']."', Paciente_Persona_idPersona = '".$data['id']."', Paciente_idPaciente = '".$data['idp']."'";
  $this->db->query($cadena);
}
public function Uplopdf($data){
  $cadena="insert analisis_gen set Url2 ='".$data['dir']."', Paciente_Persona_idPersona = '".$data['id']."', Paciente_idPaciente = '".$data['idp']."'";
  $this->db->query($cadena);
}

public function ViewImg($busca){
  $cadena="select Url from analisis_gen WHERE Paciente_Persona_idPersona='".$busca."' and Url!='' ";
    $query = $this->db->query($cadena);
    if ($query->num_rows() > 0)
        {return $query;
    }else{
      return FALSE;
    }
}

public function Viewpdf($busca){
  $cadena="select Url2 from analisis_gen WHERE Paciente_Persona_idPersona='".$busca."' and Url2!='' ";
    $query = $this->db->query($cadena);
    if ($query->num_rows() > 0)
        {return $query;
    }else{
      return FALSE;
    }
}

public function Uplofoto($data){
  $campos = array(
    'foto'=> $data['dir'],
  );
  $this->db->update('paciente', $campos, array('idPaciente'=> $data['idp'], 'Persona_idPersona'=> $data['id']));
}


public function getidpaci($id){
  $cadena="select idPaciente from paciente where Persona_idPersona = '".$id."'";
  $query = $this->db->query($cadena);
  if ($query->num_rows() > 0)
      {return $query;
  }else{
    return FALSE;
  }
}


//-------------funciones para grafica-----------
public function getDatGraf($dat){
  $cadena= "select * FROM grafica WHERE Paciente_Persona_idPersona='".$dat['per']."' and Paciente_idPaciente='".$dat['pac']."' ";
  $query = $this->db->query($cadena);
  if ($query->num_rows() > 0)
      {return $query;
  }else{
    return FALSE;
  }
}

public function AgDatgraf($data){
  $campos = array(
    'Glucosa'=> $data['glu'],
    'Fecha'=> $data['fecha'],
    'Paciente_idPaciente'=> $data['idpaci'],
    'Paciente_Persona_idPersona'=> $data['idp']
  );
  $this->db->insert('grafica',$campos);
}

public function ObtidGr(){
  $cadena="select idGraf from grafica ORDER BY idGraf DESC LIMIT 1";
  $cadenaid = $this->db->query($cadena);
    if ($cadenaid->num_rows() > 0)
        {return $cadenaid;
    }else{
      return FALSE;
    }
}

public function EdDatgraf($data){
  $campos = array(
    'Glucosa'=> $data['glu'],
    'Fecha'=> $data['fecha'],
  );
  $this->db->update('grafica', $campos, array('idGraf'=>$data['idm'], 'Paciente_idPaciente'=> $data['idpaci'], 'Paciente_Persona_idPersona'=> $data['idp']));

}

public function deletedatosG($data){

  $cadena="delete FROM grafica WHERE grafica.idGraf='".$data."'";
  $this->db->query($cadena);
}



//------------------funciones para grafica--------------

public function SetCitas($data){
  $campos = array(
    'Nombre'=> $data['nombre'],
    'Fecha'=> $data['fecha'],
    'Doctor_idDoctor'=> $data['doc']
  );
  $this->db->insert('calendario',$campos);
}

public function ViewCitas($busca){
  $cadena="select idCalendario, Fecha, Nombre from calendario WHERE Doctor_idDoctor='".$busca."'";
    $query = $this->db->query($cadena);
    if ($query->num_rows() > 0)
        {return $query;
    }else{
      return FALSE;
    }
}

public function getidcita($id){
  $cadena= "select idCalendario FROM calendario WHERE Doctor_idDoctor = '".$id."'  ORDER BY idCalendario DESC LIMIT 1";
  $query = $this->db->query($cadena);
  return $query;
}
public function deletecitaBD($data){
  $cadena="delete FROM calendario WHERE calendario.idCalendario='".$data."'";
  $this->db->query($cadena);
}
//insertar datos Nutricion
public function insertarnutri($data){
  $campos = array(
    'Paciente_Persona_idPersona'=>$data['idp'],
    'Paciente_idPaciente'=>$data['idpas'],
    'Diagnostico'=> $data['Diagnostico'],
    'Peso'=> $data['Peso'],
    'Cintura'=> $data['Cintura'],
    'Cadera'=> $data['Cadera'],
    'Notas'=> $data['Notas']
  );
  $this->db->insert('nutricion',$campos);
}

//actividad
public function insertaracti($data){
  $campos = array(
    'Nutricion_idNutricion'=>$data['idNutricion'],
    'Tipo'=> $data['Tipo'],
    'Frecuencia'=> $data['Frecuencia']
  );
  $this->db->insert('actividad',$campos);
}
//Dieta
public function insertardieta($data){
  $campos = array(
    'Nutricion_idNutricion'=>$data['idNutricion'],
    'Desayuno'=> $data['Desayuno'],
    'Colacion1'=> $data['Colacion1'],
    'Comida'=> $data['Comida'],
    'Colacion2'=> $data['Colacion2'],
    'Cena'=> $data['Cena']
  );
  $this->db->insert('dieta',$campos);
}
//frecuencia
public function insertarfrec($data){
  $campos = array(
    'Nutricion_idNutricion'=>$data['idNutricion'],
    'Frutas'=> $data['Frutas'],
    'Verduras'=> $data['Verduras'],
    'Cereales'=> $data['Cereales'],
    'AOA'=> $data['AOA'],
    'Lacteos'=> $data['Lacteos'],
    'Aceites'=> $data['Aceites'],
    'Azucares'=> $data['Azucares'],
    'Postres'=> $data['Postres'],
    'Jugos'=>$data['Jugos'],
    'AguaN'=>$data['AguaN'],
    'AguaS'=>$data['AguaS'],
    'Cafe'=> $data['Cafe'],
    'Te'=> $data['Te'],
    'Otros'=> $data['Otros'],
    'Alcohol'=> $data['Alcohol']
  );
  $this->db->insert('frecuencia',$campos);
}

//insersion podologia
public function insertarpodo($data){
  $campos = array(
    'Paciente_Persona_idPersona'=>$data['idp'],
    'Paciente_idPaciente'=>$data['idpas'],
    'Motivo'=> $data['Motivo'],
    'Diagnostico'=> $data['Diagnostico'],
    'Observaciones'=> $data['Observaciones'],
    'Tratamiento'=> $data['Tratamiento'],
    'Recomendaciones'=> $data['Recomendaciones'],
    'Notas'=>$data['Notas']
  );
  $this->db->insert('podologia',$campos);
}

    public function insertarpied($data){
        $campos = array(
            'Podologia_idPodologia'=>$data['idPodologia'],
            'Plantar'  => $data['Plantar'],
            'Dorsal'  => $data['Dorsal'],
            'Talar'  => $data['Talar'],
            'Onicrocriptosis'  => $data['Onicrocriptosis'],
            'Onicomicosis'  => $data['Onicomicosis'],
            'Onicograifosis'  => $data['Onicograifosis'],
            'Bullosis'  => $data['Bullosis'],
            'Ulceras'  => $data['Ulceras'],
            'Necrosis'  => $data['Necrosis'],
            'Grietas'  => $data['Grietas'],
            'Lesion'  => $data['Lesion'],
            'Anhidrosis' => $data['Anhidrosis'],
            'Tiñas'  => $data['Tiñas'],
            'Infecciones'  => $data['Infecciones'],
            'Dedosmar'  => $data['Dedosmar'],
            'Hallux'  => $data['Hallux'],
            'Infraductos'  => $data['Infraductos'],
            'Supraductos'  => $data['Supraductos'] ,
            'Hipercargas'  => $data['Hipercargas'] ,
            'Pulsope'  => $data['Pulsope'],
            'pulsoti'  => $data['Pulsoti'],
            'Llenado'  => $data['Llenado'],
            'Edena'  => $data['Edena'],
            'Sensacion'  => $data['Sensacion']
        );
        $this->db->insert('pie_der',$campos);
     }

     public function insertarpiei($data){
         $campos = array(
             'Podologia_idPodologia'=>$data['idPodologia'],
             'Plantar'  => $data['Plantar'],
             'Dorsal'  => $data['Dorsal'],
             'Talar'  => $data['Talar'],
             'Onicrocriptosis'  => $data['Onicrocriptosis'],
             'Onicomicosis'  => $data['Onicomicosis'],
             'Onicograifosis'  => $data['Onicograifosis'],
             'Bullosis'  => $data['Bullosis'],
             'Ulceras'  => $data['Ulceras'],
             'Necrosis'  => $data['Necrosis'],
             'Grietas'  => $data['Grietas'],
             'Lesion'  => $data['Lesion'],
            'Anhidrosis' => $data['Anhidrosis'],
             'Tiñas'  => $data['Tiñas'],
             'Infecciones'  => $data['Infecciones'],
             'Dedosmar'  => $data['Dedosmar'],
             'Hallux'  => $data['Hallux'],
             'Infraductos'  => $data['Infraductos'],
             'Supraductos'  => $data['Supraductos'] ,
             'Hipercargas'  => $data['Hipercargas'] ,
             'Pulsope'  => $data['Pulsope'],
             'pulsoti'  => $data['Pulsoti'],
             'Llenado'  => $data['Llenado'],
             'Edena'  => $data['Edena'],
             'Sensacion'  => $data['Sensacion']
         );
         $this->db->insert('pie_izq',$campos);
      }

      public function insertarpsi($data){
        $campos = array(
          'Paciente_Persona_idPersona'=>$data['idp'],
          'Paciente_idPaciente'=>$data['idpas'],
          'Notas'=> $data['Notas']
        );
        $this->db->insert('psicologia',$campos);
      }

      public function listado(){
        $cadena="select Nombre,idPersona from persona";
        $query = $this->db->query($cadena);
        return $query;
      }
}
