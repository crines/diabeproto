
  <?php // doctor tipo 1
   if(($this->session->userdata('usr')>=TRUE) && ($this->session->userdata('tipo')== 1) ){?>
    <nav class="navbar navbar-expand-lg navbar-dark pdd" style="background-color: #8FC8E7;">
      <a class="navbar-brand princ3" onclick="anim(this)" href="<?php echo base_url();?>index.php/welcome/bienvenida"><?php echo $this->session->userdata('user') ?></a>
      <button class="navbar-toggler d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
       data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <a class="navbar-brand princ" onclick="anim(this)" href="<?php echo base_url();?>index.php/welcome/bienvenida"><?php echo $this->session->userdata('user') ?></a>

      <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
        <?php if($da=='2'){?>
        <label class="navbar-brand princ2 label-menu">MG</label>
        <?php } ?>
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <?php

          if($da=='1'){
            ?>
            <li class="nav-item active princ2" onclick="anim(this)">
                <a class="nav-link" href="<?php echo base_url();?>index.php/welcome/buscadorv2">MG</a>
            </li>
          <li class="nav-item active princ2" onclick="anim(this)">
              <a class="nav-link" href="<?php echo base_url();?>index.php/welcome/nutri">Nutrición</a>
          </li>
          <li class="nav-item active princ2" onclick="anim(this)">
            <a class="nav-link" href="<?php echo base_url();?>index.php/welcome/podo">Podología</a>
          </li>
          <li class="nav-item active princ2" onclick="anim(this)">
            <a class="nav-link" href="<?php echo base_url();?>index.php/welcome/odonto">Odontología</a>
          </li>
          <li class="nav-item active princ2" onclick="anim(this)">
            <a class="nav-link" href="<?php echo base_url();?>index.php/welcome/psico">Psicología</a>
          </li>
          <?php
        }?>

        </ul>

        <ul class="navbar-nav navbar-center">
          <?php if($da=='2'){?>
          <li class="nav-item active historia">
            <a class="nav-link" href="<?php echo base_url();?>index.php/welcome/registro">Añadir nueva historia</a>
          </li>
          <?php } ?>
          <?php if($da=='3'){?>
          <li class="nav-item active historia">
            <a class="nav-link" href="<?php echo base_url();?>index.php/welcome/buscadorv2/" onclick="goBack()">Regresar</a>
          </li>
          <?php } ?>
        </ul>

        <form class="form-inline">
          <div class="">
            <a  class="nav-link animated infinite pulse slower wvs" id="sidebarCollapse" href="#">
                <i class="far fa-calendar-check fa-2x white"></i>
                <input type="hidden" name="sib" id="sib" value="0">
            </a>
          </div>
          <?php if($da=='1'){?>
              <a class="nav-link animated infinite pulse slower wvs" href="<?php echo base_url();?>index.php/welcome/grafica/" onclick="goForward()" style="color:#ffffff;">
                <i class="fas fa-chart-line fa-2x"></i>
              </a>
            <?php } ?>
          <a  class="nav-link" href="<?php echo base_url();?>index.php/welcome/cerrarSesion" style="color:#ffffff;">
            <i class="fas fa-sign-out-alt fa-2x"></i>
          </a>
        </form>
      </div>
    </nav>
<?php }?>
<?php //-------------------------------------- doctor tipo 2-------------------------------------------------------
 if(($this->session->userdata('usr')>=TRUE) && ($this->session->userdata('tipo')== 2) ){?>
  <nav class="navbar navbar-expand-lg navbar-dark pdd" style="background-color: #8FC8E7;">
    <a class="navbar-brand princ3" onclick="anim(this)" href="<?php echo base_url();?>index.php/welcome/bienvenida"><?php echo $this->session->userdata('user') ?></a>
    <button class="navbar-toggler d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
     data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03"
      aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <a class="navbar-brand princ" onclick="anim(this)" href="<?php echo base_url();?>index.php/welcome/bienvenida"><?php echo $this->session->userdata('user') ?></a>


    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
      <label class="navbar-brand princ2 label-menu">Podología</label>
      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        <?php

        if($da=='1'){
          ?>

        <?php
      }?>

      </ul>

      <ul class="navbar-nav navbar-center">
        <?php if($da=='2'){?>
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo base_url();?>index.php/welcome/registro">Añadir nueva historia</a>
        </li>
        <?php } ?>
        <?php if($da=='3'){?>
        <li class="nav-item active historia">
          <a class="nav-link" href="<?php echo base_url();?>index.php/welcome/buscadorv2/" onclick="goBack()">Regresar</a>
        </li>
        <?php } ?>
      </ul>

      <form class="form-inline">
        <div class="">
          <a  class="nav-link animated infinite pulse slower wvs" id="sidebarCollapse" href="#">
              <i class="far fa-calendar-check fa-2x white"></i>
              <input type="hidden" name="sib" id="sib" value="0">
          </a>
        </div>
        <a  class="nav-link" href="<?php echo base_url();?>index.php/welcome/cerrarSesion" style="color:#ffffff;">
          <i class="fas fa-sign-out-alt fa-2x"></i>
        </a>
      </form>
    </div>
  </nav>
<?php }?>

  <?php // ------------------------------doctor tipo 3------------------------------
   if(($this->session->userdata('usr')>=TRUE) && ($this->session->userdata('tipo')== 3) ){?>
    <nav class="navbar navbar-expand-lg navbar-dark pdd" style="background-color: #8FC8E7;">
      <a class="navbar-brand princ3" onclick="anim(this)" href="<?php echo base_url();?>index.php/welcome/bienvenida"><?php echo $this->session->userdata('user') ?></a>
      <button class="navbar-toggler d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
       data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <a class="navbar-brand princ" onclick="anim(this)" href="<?php echo base_url();?>index.php/welcome/bienvenida"><?php echo $this->session->userdata('user') ?></a>

      <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
          <label class="navbar-brand princ2 label-menu">Nutriología</label>
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <?php

          if($da=='1'){
            ?>
          <?php
        }?>

        </ul>

        <ul class="navbar-nav navbar-center">
          <?php if($da=='2'){?>
          <li class="nav-item active historia">
            <a class="nav-link" href="<?php echo base_url();?>index.php/welcome/registro">Añadir nueva historia</a>
          </li>
          <?php } ?>
          <?php if($da=='3'){?>
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url();?>index.php/welcome/buscadorv2/" onclick="goBack()">Regresar</a>
          </li>
          <?php } ?>
        </ul>

        <form class="form-inline">
          <div class="">
            <a  class="nav-link animated infinite pulse slower wvs" id="sidebarCollapse" href="#">
                <i class="far fa-calendar-check fa-2x white"></i>
                <input type="hidden" name="sib" id="sib" value="0">
            </a>
          </div>
          <a  class="nav-link" href="<?php echo base_url();?>index.php/welcome/cerrarSesion" style="color:#ffffff;">
            <i class="fas fa-sign-out-alt fa-2x"></i>
          </a>
        </form>
      </div>
    </nav>
<?php }?>

  <?php // -----------------------------doctor tipo 4-----------------------------------------
   if(($this->session->userdata('usr')>=TRUE) && ($this->session->userdata('tipo')== 4) ){?>
    <nav class="navbar navbar-expand-lg navbar-dark pdd" style="background-color: #8FC8E7;">
      <a class="navbar-brand princ3" onclick="anim(this)" href="<?php echo base_url();?>index.php/welcome/bienvenida"><?php echo $this->session->userdata('user') ?></a>
      <button class="navbar-toggler d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
       data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <a class="navbar-brand princ" onclick="anim(this)" href="<?php echo base_url();?>index.php/welcome/bienvenida"><?php echo $this->session->userdata('user') ?></a>

      <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
        <label class="navbar-brand princ2 label-menu">Psicología</label>
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <?php

          if($da=='1'){
            ?>

          <?php
        }?>

        </ul>

        <ul class="navbar-nav navbar-center">
          <?php if($da=='2'){?>
          <li class="nav-item active historia">
            <a class="nav-link" href="<?php echo base_url();?>index.php/welcome/registro">Añadir nueva historia</a>
          </li>
          <?php } ?>
          <?php if($da=='3'){?>
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url();?>index.php/welcome/buscadorv2/" onclick="goBack()">Regresar</a>
          </li>
          <?php } ?>
        </ul>

        <form class="form-inline">
          <div class="">
            <a  class="nav-link animated infinite pulse slower wvs" id="sidebarCollapse" href="#">
                <i class="far fa-calendar-check fa-2x white"></i>
                <input type="hidden" name="sib" id="sib" value="0">
            </a>
          </div>
          <a  class="nav-link" href="<?php echo base_url();?>index.php/welcome/cerrarSesion" style="color:#ffffff;">
            <i class="fas fa-sign-out-alt fa-2x"></i>
          </a>
        </form>
      </div>
    </nav>
<?php }?>
