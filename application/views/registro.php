<body>
<?php if(($this->session->userdata('usr')>=TRUE)){?>
<div class="container-fluid pt-3 fondor">

  <div class="container-fluid pt-5">
    <div >
      <center>
        <h1 class="blue">Nueva Historia</h1>
      </center>
    </div>
  </div>
    <div class="container">
        <div>
      <div class="accordion" id="accordionExample"><!-- inicio contenedor-->
              <div class="card accordion2" onclick="butEnab(this)" ><!-- inicio card-->
                <a class="card-link" data-toggle="collapse" href="#collapseOne" value="1" id="butEnab">
                <div class="card-header" id="headingOner">
                  <h5 class="blue mb-0 titlecard">Ficha de Identificación</h5>
                </div>
                </a>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOner" data-parent="#accordionExample">
                  <div class="card-body grad"><!-- contenido de la pestaña-->

                    <div class="form-group">
                            <label for="exampleInputEmail1">Nombre</label>
                            <input type="text" name="nombre" class="form-control" id="nombre"  placeholder="Nombre"  value="" data-placement="right" required>
                    </div>
                    <div class="form-group">
                            <label for="exampleInputEmail1">Fecha de Nacimiento</label>
                            <input type="date" name="fecha" class="form-control" id="fecha"  placeholder="Fecha"  value="" data-placement="right" required>
                    </div>
                    <div class="form-group">
                            <label for="exampleInputEmail1">Edad</label>
                            <input type="number" name="edad" class="form-control" id="edad"  placeholder="Edad"  value="" data-placement="right" required>
                    </div>
                    <div class="form-group">
                           <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadio1" name="genero" value="Masculino" class="custom-control-input" checked>
                                <label class="custom-control-label" for="customRadio1" >Masculino</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadio2" name="genero" value="Femenino" class="custom-control-input">
                                <label class="custom-control-label" for="customRadio2">Femenino</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadio3" name="genero" value="Otros" class="custom-control-input">
                                <label class="custom-control-label" for="customRadio3">Otros</label>
                          </div>
                    </div>
                    <div class="form-group">
                            <label for="exampleInputEmail1">Lugar de Nacimiento</label>
                            <input type="text" class="form-control" name="nacimiento" id="nacimiento"  placeholder="Puebla"  value="" data-placement="right" required>
                    </div>
                    <div class="form-group">
                            <label for="exampleInputEmail1">Lugar de Residencia</label>
                            <input type="text" class="form-control" name="residencia" id="residencia"  placeholder="Puebla"  value="" data-placement="right" required>
                    </div>
                    <div class="form-group">
                            <label for="exampleInputEmail1">Domicilio</label>
                            <input type="text" class="form-control" name="domicilio" id="domicilio"  placeholder="calle ..."  value="" data-placement="right" required>
                    </div>
                    <div class="form-group">
                            <label for="exampleInputEmail1">Ocupación</label>
                            <input type="text" class="form-control"  name="ocupacion" id="ocupacion"  placeholder="Trabajador"  value="" data-placement="right" required>
                    </div>
                    <div class="form-group">
                           <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadio4" name="estado" value="Casado" class="custom-control-input">
                                <label class="custom-control-label" for="customRadio4">Casado(a)</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadio5" name="estado" value="Soltero(a)" class="custom-control-input" checked>
                                <label class="custom-control-label" for="customRadio5">Soltero(a)</label>
                          </div>
                    </div>
                    <div class="form-group">
                            <label for="exampleInputEmail1">Escolaridad</label>
                            <input type="text" class="form-control"  name="escolaridad" id="escolaridad"  placeholder="Universidad"  value="" data-placement="right" required>
                    </div>
                    <div class="form-group">
                            <label for="exampleInputEmail1">Religión</label>
                            <input type="text" class="form-control" name="religion" id="religion"  placeholder="Catolico"  value="" data-placement="right" required>
                    </div>
                    <div class="form-group">
                            <label for="exampleInputEmail1">Teléfono</label>
                            <input type="number" class="form-control"  name="telefono" id="telefono"  placeholder="222-222-2222"  value="" data-placement="right" required>
                    </div>
                    <div class="form-group">
                            <label for="exampleInputEmail1">Correo Electrónico</label>
                            <input type="email" class="form-control"  name="correo" id="correo" aria-describedby="emailHelp" placeholder="correo@mail.com"  value="" data-placement="right" required>
                    </div>
                </div><!--fin de contenido de la pestaña-->
              </div><!--fin contenedor 2-->
            </div><!-- fin card-->
            <button id="guardaficha" name="guardaficha" onclick="guardaficha(this)"  class="btn btn-primary" disabled>Continuar Registro</button>
          </div>
        </div>
      </div>
</div>

 <?php }?>
</body>
