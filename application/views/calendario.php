<!-- Sidebar  -->
<nav id="sidebar">
    <div id="dismiss">
        <i class="fas fa-arrow-left"></i>
    </div>

    <div class="sidebar-header">
        <h3 class="white">Agenda</h3>
    </div>

    <ul class="list-unstyled components">
        <!-- Button trigger modal modal en demas vistas-->
        <button type="button" class="btn btn-primary" data-toggle="modal"
         data-target="#exampleModal">Crear cita</button>

    </ul>

    <ul>
      <div class="table-responsive">
        <div class="container">
              <div class="row appd" id="tablacita">
                <?php if($citas!=FALSE){
                          foreach ($citas->result() as $rowcita){
                 ?>
                 <div class="col-8 border" id="<?php echo "dcit"."$rowcita->idCalendario";?>">

                    <label for="" class="font-min"><?php echo "$rowcita->Fecha";?></label>
                     <label for="" class="font-min"><?php echo "$rowcita->Nombre"; ?></label>
                  </div>
                  <div class="col-4" id="<?php echo "bcit"."$rowcita->idCalendario";?>">
                    <button id="citaborrar" type="button" name="button" value="<?php echo "$rowcita->idCalendario";?>" onClick="delcita(this)"><i class="fas fa-trash fa-1x blue"></i></button>
                   </div>
                <?php
                }
              } ?>
            </div>
          </div>
        </div>
    </ul>

</nav>
<!-- FIN Sidebar  -->
