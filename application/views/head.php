<!DOCTYPE html>
<!--cabecera para el index-->
<html lang="es">
<head>
  <title>Web DiabeMedica</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="<?php echo base_url();?>img/logo.png">
  <!-- bootstrap -->
<link rel="stylesheet" href="<?php echo base_url();?>static/bootstrap/css/bootstrap.min.css">
<!--select2 -->
<link rel="stylesheet" href="<?php echo base_url();?>static/select2/css/select2.min.css">
<!-- calendario -->
<link rel="stylesheet" href="<?php echo base_url();?>static/gijgo-combined-1.9.11/css/gijgo.min.css">
<!-- Scrollbar Custom CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

<!--
<script src="<?php echo base_url();?>static/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>static/bootstrap/assets/js/vendor/popper.min.js"></script>
<script src="<?php echo base_url();?>static/bootstrap/assets/js/vendor/jquery-slim.min.js"></script>
-->
<!--estilos propios-->
<!--estilos propios-->
  <link href="<?php echo base_url();?>static/css/estilos.min.css" rel="stylesheet">
<!--estilos para sidebar-->
  <link href="<?php echo base_url();?>static/css/sidebar.min.css" rel="stylesheet">
  <!-- waves -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static\waves\waves.min.css" />

  <!-- animate -->
  <link href="<?php echo base_url();?>static/css/animate.css" rel="stylesheet">
  <!-- fontawesome -->
  <link href="<?php echo base_url();?>static/fontawesome/css/all.css" rel="stylesheet">

  <!-- fuentes -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,600italic,400italic,300italic" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- chart.js -->
    <script src="<?php echo base_url();?>/charts/Chart.bundle.min.js"></script>
    <script src="<?php echo base_url();?>/charts/Chart.bundle.js"></script>
  <style>
    .imm{
      width: 100%;
      height: 100%;
      padding: 50px;
      background-image: url("<?php echo base_url();?>img/fondo-min-min.jpg");
      background-position: center;
      background-size: cover;
      text-align: center;
    }

    .fondor{
      width: 100%;
      /*height: 100%;*/
      /*background-image: url("<?php echo base_url();?>img/fondo2.png");
      */
      background-image: linear-gradient(#ffffff, #8FC8E7);
      background-position: center;
      background-size: cover;
      text-align: center;
      margin-top: 55px;
      padding-bottom: 170px;
    }


    .pleca{
      padding-left: 10px;
      padding-right: 10px;
      padding-top: 30px;
      padding-bottom: 30px;
      background-image: url("<?php echo base_url();?>img/pleca1.png");
      background-position: center;
      background-size: cover;
    }
    .pdd{
      padding: 0px;
    }

    .princ{
      width: 130px;
      background-image: url("<?php echo base_url();?>img/contenedor1.png");
      background-position: center;
      background-size: cover;
      text-align: center;
    }
    .princ3{
      display:none;
    }
    @media only screen and (max-width: 765px) {
        .princ{
        display:none;
        }
        .princ3{
          width: 130px;
          background-image: url("<?php echo base_url();?>img/contenedor1.png");
          background-position: center;
          background-size: cover;
          text-align: center;
          display: block;
        }
    }
    .princ2{
      width: 130px;
      background-image: url("<?php echo base_url();?>img/contenedor2.png");
      background-position: center;
      background-size: cover;
      text-align: center;
    }
    .historia{
      background-color: #00668F;
    }

    .mainn{
      width: 100%;
      height: 100%;
    }
  </style>



</head>
