<footer class="sticky-bottom pb-3 pl-5 pl- text-light "  style="background-color: #8FC8E7;">
  <!-- Copyright -->
  <div class="container footer-copyright text-center">Desarrollado por</div>
  <div class="container footer-copyright text-center">©Mauricio Arturo Rodriguez Rosas</div>
  <div class="container footer-copyright text-center">©Oscar Aguilar Nopal</div>
  <!-- Copyright -->

</footer>

</html>

<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.6/chosen.jquery.js"></script>
-->
<!-- waves -->
<script type="text/javascript" src="<?php echo base_url();?>static\waves\waves.min.js"></script>
<!-- jQuery CDN - Slim version (=without AJAX) -->
<!-- POPPER-->
<script>window.jQuery || document.write('<script src="<?php echo base_url();?>static/jquery/dist/jquery.min.js"><\/script>')</script>
<script src="<?php echo base_url();?>static/bootstrap/assets/js/vendor/popper.min.js"></script>
<!-- Bootstrap-->
<script src="<?php echo base_url();?>static/bootstrap/js/bootstrap.min.js"></script>
<!-- select2-->
<script src="<?php echo base_url();?>static/select2/js/select2.min.js"></script>
<!-- jQuery Custom Scroller CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<!--libreria de calendario-->
<script src="<?php echo base_url();?>static/gijgo-combined-1.9.11/js/gijgo.min.js"></script>

<script src="<?php echo base_url();?>static/js/dash.js" charset="utf-8"></script>
