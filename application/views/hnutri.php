<body class="">
<?php if(($this->session->userdata('usr')>=TRUE)){?>
<div class="main imm">

<div class="pleca">

  <form  method="POST" action="<?php echo base_url();?>index.php/welcome/nutri">
      <h2 style="color:#ffffff;" for="pacientes">Selecciona un paciente:</h2>
      <div class="input-group mb-3">
        <span class="busqpaciente"  id="inputGroupFileAddon01">Paciente:</span>
         <select class="js-example-responsive js-example-language" lang="es" id="pacientes" name="pacientes" style="width: 90%;">
           <option value="">seleccione...</option>
         <?php foreach($pacientes->result() as $fila) { ?>
          <option value="<?=$fila->Persona_idPersona?>"><?=$fila ->Nombre?></option><?php } ?>
         </select>
         <div class="input-group-append">
            <button class="busqpaciente" type="submit" value="Buscar" name="buscar">Buscar</button>
          </div>
           </div>
         </form>
       </div>

</div>
<!-- Button trigger modal en views/calendario/... -->
        <!-- Modal -->
        <div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">cita</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div>
                  <div class="form-group">
                    <label for="recipient-fecha" class="col-form-label">Fecha:</label>
                    <input id="inputcalendar" width="312" name="fechacita" />
                  </div>
                  <div class="form-group">
                    <label class="busqpaciente" class="col-form-label">Paciente</label>
                    <select class="js-example-responsive js-example-language" lang="es" id="pacientescal" name="pacientescita" style="width: 90%;">
                      <option value="">seleccione...</option>
                    <?php foreach($pacientes->result() as $fila) { ?>
                     <option value="<?=$fila ->Nombre?>"><?=$fila ->Nombre?></option><?php } ?>
                    </select>
                  </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                  <button class="btn btn-blue" id="Creacita" onclick="creacita(this)" name="agregar">Guardar</button>

              </div>
            </div>
            </div>
          </div>
        </div>



    <?php } else
   redirect('/Welcome/index/', 'refresh');
 ?>
</body>
