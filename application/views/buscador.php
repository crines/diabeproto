<body>
<?php if(($this->session->userdata('usr')>=TRUE) && ($this->session->userdata('check')>=TRUE) ){?>

  <div class="container-fluid pt-3 ">
    <div class="maincontainer example">

      <div class="container-fluid">
      <div class="accordion" id="accordionExample">
        <div class="row">


        <div class="col-md-4">
              <div class="card accordion1">
                <a class="card-link">
                <div class="card-header" id="headingOnePos">
                      <h2 class="mb-0 titlecard">
                        <?php   foreach ($pacientes->result() as $fila){

                       }
                       echo "$fila->Nombre";?>
                     </h2>
                     <br>
                     <div class="">
                       <?php if(($fila->foto)!=null){ ?>
                         <img src="<?php echo base_url().$fila->foto; ?>" alt="" style="width:100px; height:100px;">
                         <div>
                         <form enctype="multipart/form-data" method="post" action="<?php echo base_url();?>index.php/welcome/foto/"
                           >
                           <label for="imagen">cambiar foto:</label>
                            <input name="imagen" type="file" style="font-size: 10px;" />
                            <input type="submit" value="subir" />

                          </form>
                        </div>
                       <?php }else{ ?>
                         <form enctype="multipart/form-data" method="post" action="<?php echo base_url();?>index.php/welcome/foto/"
                           >
                            <label for="imagen">foto:</label>
                            <input name="imagen" type="file"/>
                            <input type="submit" value="subir" />

                          </form>
                       <?php } ?>
                     </div>
                </div>
                  </a>
                <div>
                  <div class="card-body grad">
                    <br> <h3 class="white"><i class="fa fa-stethoscope fa-fw"></i><?php   echo "Diagnóstico: $fila->Diagnostico";?></h3><br>
                    <div class="">
                      <input type="hidden" name="idpaci" value="<?php    echo "$fila->idPaciente"; ?>">
                      <input type="hidden" name="idp" value="<?php    echo "$fila->idPersona"; ?>">
                      <?php if(($fila->idPodologo)!=null){?>
                        <label class="checkbox-inline"><input type="checkbox" id="podo" onclick="marktipo(this)" value="2" checked>Podología:</label>
                      <?php }else{?>
                        <label class="checkbox-inline"><input type="checkbox" id="podo" onclick="marktipo(this)" value="2">Podología:</label>
                      <?php } ?>

                      <?php if(($fila->idNutriologo)!=null){?>
                        <label class="checkbox-inline"><input type="checkbox" id="nutri" onclick="marktipo(this)" value="3" checked>Nutrilogía:</label>
                      <?php }else{?>
                        <label class="checkbox-inline"><input type="checkbox" id="nutri" onclick="marktipo(this)" value="3">Nutrilogía:</label>
                      <?php } ?>

                      <?php if(($fila->idPsicologo)!=null){?>
                        <label class="checkbox-inline"><input type="checkbox" id="psico" onclick="marktipo(this)" value="4" checked>Psicología:</label>
                      <?php }else{?>
                        <label class="checkbox-inline"><input type="checkbox" id="psico" onclick="marktipo(this)" value="4">Psicología:</label>
                      <?php } ?>

                      <?php if(($fila->idOdontologo)!=null){?>
                        <label class="checkbox-inline"><input type="checkbox" id="odon" onclick="marktipo(this)" value="5" checked>Odontología:</label>
                      <?php }else{?>
                        <label class="checkbox-inline"><input type="checkbox" id="odon" onclick="marktipo(this)" value="5">Odontología:</label>
                      <?php } ?>
                    </div>
                    <div class="container pt-5 pb-5">
                    <div class="accordion" id="accordionExample2">
                            <div class="card">
                                <a class="card-link" data-toggle="collapse" href="#collapseOne1">
                              <div class="card-header" id="headingOne1">
                                    <h5 class="mb-0 blue"><i class="fas fa-user-circle"></i> Ficha de Identificación</h5>
                              </div>
                                </a>
                              <div id="collapseOne1" class="collapse" aria-labelledby="headingOne1" data-parent="#accordionExample2">
                                <div class="card-body">
                                    <div class="container pt-2 justify">
                                      <p><i class="fa fa-calendar fa-fw"></i><?php    echo "Fecha de nacimiento: $fila->Fecha";?></p>
                                      <p><i class="fa fa-address-card-o fa-fw"></i><?php    echo "Edad: $fila->Edad"; ?></p>
                                      <p><i class="fa fa-user fa-fw"></i><?php    echo "Género: $fila->Genero"; ?></p>
                                      <p><i class="fa fa-map-marker fa-fw"></i><?php    echo "Lugar de nacimiento: $fila->Lugar"; ?></p>
                                      <p><i class="fa fa-map-marker fa-fw"></i><?php    echo "Lugar de Residencia: $fila->Residencia"; ?></p>
                                      <p><i class="fa fa-home fa-fw"></i><?php    echo "Domicilio: $fila->Domicilio"; ?></p>
                                      <p><i class="fa fa-briefcase fa-fw"></i><?php    echo "Ocupación: $fila->Ocupacion"; ?></p>
                                      <p><i class="fa fa-bookmark fa-fw"></i><?php    echo "Estado Civil: $fila->Estado"; ?></p>
                                      <p><i class="fa fa-book fa-fw"></i><?php    echo "Escolaridad: $fila->Escolaridad"; ?></p>
                                      <p><i class="fa fa-shekel fa-fw"></i><?php    echo "Religión: $fila->Religion"; ?></p>
                                      <p><i class="fa fa-phone fa-fw"></i><?php    echo "Teléfono: $fila->Telefono"; ?></p>
                                      <p><i class="fa fa-envelope fa-fw"></i><?php    echo "E-mail: $fila->Email"; ?></kp>
                                      <p><i class="fa fa-list-alt fa-fw"></i><?php    echo "Padecimiento: $fila->Padecimiento"; ?></p>
                                      <p><i class="fa fa-stethoscope fa-fw"></i><?php    echo "Exploración: $fila->Exploracion"; ?></p>
                                    </div>
                                  </div>
                              </div>
                            </div>
                            <div class="card">
                              <a class="card-link" data-toggle="collapse" href="#collapseOne2">
                              <div class="card-header" id="headingOne2">
                                    <h5 class="mb-0 blue"><i class="far fa-sticky-note"></i> Notas</h5>
                              </div>
                                </a>
                              <div id="collapseOne2" class="collapse" aria-labelledby="headingOne2" data-parent="#accordionExample2">
                                <div class="card-body grad">
                                    <div class="container pt-2 justify">
                                      <!-- conseguir id de persona y paciente-->
                                      <input type="hidden" name="idpaci" value="<?php    echo "$fila->idPaciente"; ?>">
                                      <input type="hidden" name="idp" value="<?php    echo "$fila->idPersona"; ?>">
                                      <p style="background-color: #ffffff; color:#000000"><textarea name="notas" id="editor1" rows="10" cols="25"> <?php    echo "$fila->Notas"; ?></textarea></p>
                                       <button name="nota-button"  id="nota-button">Guardar nota</button>

                                    </div>
                                </div>
                              </div>
                            </div>
                            <div class="card">
                              <a class="card-link" data-toggle="collapse" href="#collapseOne3">
                              <div class="card-header" id="headingOne3">
                                    <h5 class="mb-0 blue"><i class="far fa-sticky-note"></i>Interconsultas</h5>
                              </div>
                                </a>
                              <div id="collapseOne3" class="collapse" aria-labelledby="headingOne3" data-parent="#accordionExample2">
                                <div class="card-body grad">
                                    <div class="container pt-2 justify">
                                      <p style="background-color: #ffffff; color:#000000"><textarea name="interconsultas" id="editor2" rows="10" cols="25"> <?php    echo "$fila->Interconsultas"; ?></textarea></p>
                                       <button name="inter-button" id="inter-button">Guardar interconsultas</button>
                                    </div>
                                </div>
                              </div>
                            </div>
                      </div>
                    </div>
                    </div>
                </div>
              </div>
              </div>

              <br />
              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-12">
                    <div class="card accordion1">
                      <a class="card-link" data-toggle="collapse" href="#collapseOne">
                        <div class="card-header" id="headingOne">
                            <h2 class="titlecard"><i class="fa fa-medkit fa-fw"></i>Medicamentos</h2>
                        </div>
                      </a>

                      <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body grad">
                          <button name="agregame" id="agregame" style="margin:6px;"><i class="fas fa-plus fa-1x blue"></i> Nuevo Medicamento</button>
                          <button name="cer" id="cer" style="display:none; padding:1px 7px;"><i class="fas fa-times fa-1x blue"></i> Cerrar formulario</button>
                          <!--
                          <button name="editame" id="editame">editar</button>
                          <button name="eliminame" id="eliminame">eliminar</button>
                        -->
                          <div class="table-responsive">
                            <table class="table table-bordered">
                            <tr name="agre" id="agre" style="display:none">
                              <input type="hidden" name="idpaci" value="<?php    echo "$fila->idPaciente"; ?>">
                              <input type="hidden" name="idp" value="<?php    echo "$fila->idPersona"; ?>">
                              <th> <input placeholder="Nombre Medicamento" type="text" name="nombremed" id="nombremed" size="15" value="" data-placement="top">
                              <td><input placeholder="Dosis" type="text" name="dosismed" id="dosismed" size="15" value="" data-placement="top"></td>
                              <td><input placeholder="Horario" type="text" name="horariomed" id="horariomed" size="15" value="" data-placement="top"></td>
                              <td><textarea placeholder="Observaciones" type="text" name="Observacionesmed" id="Observacionesmed" cols="14" value="" data-placement="top"></textarea></td>
                              <td> <button name="guardarme" id="guardarme"><i class="far fa-save fa-2x blue"></i></button> </td>
                            </tr>
                          </table>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                      <th scope="col">Nombre del Medicamento</th>
                                      <th scope="col">Dosis</th>
                                      <th scope="col">Horario</th>
                                      <th scope="col">Observaciones</th>
                                    </tr>
                                  </thead>
                                  <tbody class="bodymedica">
                                    <?php
                                    $i=0;
                                    if($medicament!=FALSE){
                                      //$i=0;

                                     foreach ($medicament->result() as $fila2){?>

                                    <tr id="<?php echo "me"."$fila2->idMedicamentos";?>">
                                      <?php $i=$i+1; ?>
                                      <th scope="row"><input type="text" name="<?php echo $i;?>" id="<?php echo $i;?>" size="13" value="<?php echo "$fila2->Medicamento";?>" disabled> </th>
                                      <?php $i=$i+1; ?>
                                      <td> <input type="text" name="<?php echo $i;?>" id="<?php echo $i;?>" size="13" value="<?php echo "$fila2->Dosis"; ?>" disabled> </td>
                                      <?php $i=$i+1; ?>
                                      <td> <input type="text" name="<?php echo $i;?>" id="<?php echo $i;?>" size="13" value="<?php echo "$fila2->Horario";?>" disabled> </td>
                                      <?php $i=$i+1; ?>
                                      <td> <textarea type="text" name="<?php echo $i;?>" id="<?php echo $i;?>" cols="13" value="<?php echo "$fila2->Observaciones"; ?>" disabled><?php echo "$fila2->Observaciones"; ?></textarea> </td>
                                      <?php $i=$i+1; ?>
                                      <td style="border:0;">
                                         <button id="<?php echo "i".$i;?>" value="<?php echo $i;?>" onclick="edit(this)"><i class="far fa-edit fa-2x blue"></i></button>
                                         <button id="<?php echo "j".$i;?>" value="<?php echo "$fila2->idMedicamentos";?>" onclick="saveEdit(this)" style="display:none"><i class="far fa-save fa-2x blue"></i></button>
                                        </td>
                                      <td style="border:0;"> <button id="<?php echo "z".$i;?>" value="<?php echo "$fila2->idMedicamentos";?>" onclick="alerta(this)"><i class="fas fa-trash fa-2x blue"></i></button> </td>
                                    </tr>


                                <?php }
                               ?>


                               <input type="hidden" id="ultimoM" name="ultimoM" value="<?php echo "$fila2->idMedicamentos";?>" disabled>
                                 </tbody>
                             <?php } ?>
                             <input type="hidden" name="eddi" id="eddi" value="" disabled>
                             <input type="hidden" name="im" id="im" value="<?php echo $i;?>" disabled>
                                  </table>
                                  <!-- Modal -->
                        <div class="modal fade" id="seguro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Seguro que quiere eliminar</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <input type="text" id="check" name="" disabled>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary" onclick="delect(this)">eliminar</button>
                              </div>
                            </div>
                          </div>
                        </div>




                            </div>
                          </div>
                      </div>
                  </div>
                     </div>
                     <br />
                    <div class="col-md-12">
                      <div id="accordionExample1"><!-- inicio contenedor-->
                      <div class="card accordion1">
                        <a class="card-link" data-toggle="collapse" href="#collapseTwo">
                        <div class="card-header" id="headingTwo">
                          <h2 class="titlecard"><i class="fa fa-tint fa-fw"></i>Antecedentes Patológicos</h2>
                        </div>
                        </a>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body grad">
                          <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                      <th scope="col">Diabetes</th>
                                      <th scope="col">Cardiopatía</th>
                                      <th scope="col">Hipotiroidismo</th>
                                      <th scope="col">Dislipidemia</th>
                                      <th scope="col">Hepáticas</th>
                                      <th scope="col">Convulsión</th>
                                      <th scope="col">Cáncer</th>
                                      <th scope="col">Transfusionales</th>
                                      <th scope="col">Quirúrgicas</th>
                                      <th scope="col">Hipertensión</th>
                                      <th scope="col">Insuficiencia</th>
                                      <th scope="col">Hipertiroidismo</th>
                                      <th scope="col">Marcapasos</th>
                                      <th scope="col">Renales</th>
                                      <th scope="col">Artritis</th>
                                      <th scope="col">Dentales</th>
                                      <th scope="col">Traumáticos</th>
                                      <th scope="col">Alérgicos</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php if($antecedentes!=FALSE){
                                              foreach ($antecedentes->result() as $fila3){
                                     ?>
                                    <tr>
                                      <th scope="row"><?php echo "$fila3->Diabetes";?></th>
                                      <td><?php echo "$fila3->Cardiopatia"; ?></td>
                                      <td><?php echo "$fila3->Hipotiroidismo";?></td>
                                      <td><?php echo "$fila3->Dislipidemia"; ?></td>
                                      <td><?php echo "$fila3->Hepaticas";?></td>
                                      <td><?php echo "$fila3->Convulsion"; ?></td>
                                      <td><?php echo "$fila3->Cancer";?></td>
                                      <td><?php echo "$fila3->Transfucionales"; ?></td>
                                      <td><?php echo "$fila3->Quirurgicas";?></td>
                                      <td><?php echo "$fila3->Hipertension"; ?></td>
                                      <td><?php echo "$fila3->Insuficiencia";?></td>
                                      <td><?php echo "$fila3->Hipertiroidismo"; ?></td>
                                      <td><?php echo "$fila3->Marcapasos";?></td>
                                      <td><?php echo "$fila3->Renales"; ?></td>
                                      <td><?php echo "$fila3->Artritis";?></td>
                                      <td><?php echo "$fila3->Dentales"; ?></td>
                                      <td><?php echo "$fila3->Traumaticos";?></td>
                                      <td><?php echo "$fila3->Alergicos"; ?></td>
                                    </tr>
                                    <?php
                                    }
                                  } ?>
                                </tbody>
                                  </table>
                            </div>
                       </div><!--fin de card-body-->
                     </div>
                    </div><!--fin de card-->
                    </div><!--fin de contenedor-->
                    </div>
                      <br>
                      <div class="col-md-12">
                        <div id="accordionExample2"><!-- inicio contenedor-->
                      <div class="card accordion1">
                        <a class="card-link" data-toggle="collapse" href="#collapseThree">
                        <div class="card-header" id="headingThree">
                          <h2 class="titlecard"><i class="fa fa-navicon fa-fw"></i>Antecedentes No Patológicos</h2>
                        </div>
                        </a>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body grad">
                          <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                      <th scope="col">Peso</th>
                                      <th scope="col">Obesidad</th>
                                      <th scope="col">Dieta</th>
                                      <th scope="col">Ejercicio</th>
                                      <th scope="col">Fumar</th>
                                      <th scope="col">Bebida</th>
                                      <th scope="col">Control</th>
                                      <th scope="col">Drogas</th>
                                      <th scope="col">Humo</th>
                                      <th scope="col">Viaje</th>
                                      <th scope="col">Trabajos</th>
                                      <th scope="col">Casa</th>
                                      <th scope="col">Sangre</th>
                                      <th scope="col">Mascotas</th>
                                      <th scope="col">Seguro</th>
                                      <th scope="col">Vacunación</th>
                                      <th scope="col">Desparasitación</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php if($antecedentes!=FALSE){
                                              foreach ($antecedentes->result() as $fila3){
                                     ?>
                                    <tr>
                                      <th scope="row"><?php echo "$fila3->Peso";?></th>
                                      <td><?php echo "$fila3->Obesidad"; ?></td>
                                      <td><?php echo "$fila3->Dieta";?></td>
                                      <td><?php echo "$fila3->Ejercicio"; ?></td>
                                      <td><?php echo "$fila3->Fumar";?></td>
                                      <td><?php echo "$fila3->Bebida"; ?></td>
                                      <td><?php echo "$fila3->Control";?></td>
                                      <td><?php echo "$fila3->Drogas"; ?></td>
                                      <td><?php echo "$fila3->Humo";?></td>
                                      <td><?php echo "$fila3->Viaje"; ?></td>
                                      <td><?php echo "$fila3->Trabajos";?></td>
                                      <td><?php echo "$fila3->Casa"; ?></td>
                                      <td><?php echo "$fila3->Sangre";?></td>
                                      <td><?php echo "$fila3->Mascotas"; ?></td>
                                      <td><?php echo "$fila3->Seguro";?></td>
                                      <td><?php echo "$fila3->Vacunacion"; ?></td>
                                      <td><?php echo "$fila3->Desparacitacion";?></td>
                                    </tr>
                                    <?php
                                    }
                                  } ?>
                                  </tbody>
                                  </table>
                            </div>
                        </div><!-- inicio card-body-->
                      </div>
                      </div><!-- inicio card-->
                    </div><!-- inicio contenedor-->
                      </div>
                      <br>
                      <div class="col-md-12">
                          <div class="card accordion1">
                            <a class="card-link" data-toggle="collapse" href="#collapseSix">
                              <div class="card-header" id="headingSix">
                                  <h2 class="titlecard"><i class="fa fa-medkit fa-fw"></i>Antecedentes Heredofamiliares</h2>
                              </div>
                            </a>
                            <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                              <div class="card-body grad">
                                <div class="table-responsive">
                                  <table class="table table-bordered">
                                      <thead>
                                          <tr>
                                            <th scope="col">Familiar</th>
                                            <th scope="col">Estado</th>
                                            <th scope="col">Enfermedad</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <?php if($antecedentes!=FALSE){
                                                    foreach ($antecedentes->result() as $fila3){
                                           ?>
                                          <tr>
                                            <th scope="row"><?php echo "$fila3->Familiar";?></th>
                                            <td><?php echo "$fila3->Estado"; ?></td>
                                            <td><?php echo "$fila3->Enfermedad";?></td>
                                          </tr>
                                          <?php
                                          }
                                        } ?>
                                        </tbody>
                                        </table>
                                  </div>

                              </div>
                            </div>
                        </div>
                      </div>

                            <br>
                              <div class="col-md-12">
                                <div class="card accordion1">
                                  <a class="card-link" data-toggle="collapse" href="#collapseFour">
                                    <div class="card-header" id="headingFour">
                                        <h2 class="titlecard"><i class="fa fa-medkit fa-fw"></i>Subir análisis clínico</h2>
                                    </div>
                                  </a>
                                  <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                                    <div class="card-body grad">
                                     <form enctype="multipart/form-data" method="post" action="<?php echo base_url();?>index.php/welcome/uploimg/"
                                       >
                                        <label for="imagen">Imagen:</label>
                                        <input name="imagen" type="file"/>
                                        <input type="submit" value="subir" />

                                      </form>
                                              <!--    <div class="input-group mb-3">
                                            <div class="custom-file">
                                              <input type="file" class="custom-file-input" id="inputGroupFile02">
                                              <label class="custom-file-label" for="inputGroupFile02">Choose file</label>
                                            </div>
                                            <div class="input-group-append">
                                              <span class="input-group-text" id="">Upload</span>
                                            </div>
                                          </div>-->
                                                </div>
                                              </div>
                                          </div>
                              </div>
                              <br>
                    <div class="col-md-12">
                      <div class="card accordion1">
                        <a class="card-link" data-toggle="collapse" href="#collapseFive">
                          <div class="card-header" id="headingFive">
                              <h2 class="titlecard"><i class="fa fa-medkit fa-fw"></i>Análisis clínicos</h2>
                          </div>
                        </a>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                        <div class="card-body grad">

                         <p>Imágenes:</p>

                          <div class="row">
                             <?php if($imagen!=null){ ?>
                              <?php
                                 $j=0;

                                 foreach($imagen->result() as $row1){
                                  $img= array( $j =>$row1->Url);
                                  //$pdf= array($jj =>$row1->Url2);
                                  ?>

                                        <div class="column espacio">
                                        <a data-toggle="modal" data-target="#exampleModalCenter" >
                                          <!--<img src="<?php echo base_url().$img[$j];?>" style="width:100%; height:100%" onclick="onClick(this)" class="hover-shadow cursor">
                                          -->
                                          <object data="<?php echo base_url().$img[$j];?>" style="width:100%; height:100%;"
                                            onclick="onClick(this)" class="hover-shadow cursor"></object>
                                        </a>
                                      </div>

                                  <?php
                                      $j=$j+1;
                                          }
                                          ?>
                            <?php } ?>
                                <?php if($pdf!=null){
                                  $jj=0;
                                  foreach($pdf->result() as $row2){
                                   //$img= array( $j =>$row1->Url);
                                   $pdf1= array($jj =>$row2->Url2);?>
                                   <div class="column espacio">
                                   <button class="transpa" value="<?php echo base_url().$pdf1[$jj];?>" onclick="verpdf(this)">Abrir
                                     <object  data="<?php echo base_url().$pdf1[$jj];?>" style="width:100%; height:100%;" onclick="">
                                     </object>
                                   </button>
                                 </div>

                                <?php $jj=$jj+1;
                                      }
                              } ?>
                            </div>


                            <!-- Modal -->
                            <div id="modal01" class="w3-modal" onclick="this.style.display='none'">
                              <span class="w3-button w3-hover-red w3-xlarge w3-display-topright">&times;</span>
                              <div class="w3-modal-content w3-animate-zoom">
                                <object id="img01" style="width:100%; height:100%"></object>
                              </div>
                            </div>

                            <!-- Modal -->
                            <div id="modal02" class="w3-modal-2" onclick="this.style.display='none'">
                              <span class="w3-button w3-hover-red w3-xlarge w3-display-topright">&times;</span>
                              <div class="w3-modal-content w3-animate-zoom">

                                <embed id="pdf01" type="application/pdf" src="#" style="width:100%; height:500px">
                              </div>
                            </div>



                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
               </div>
               </div>
        </div>
      </div>
    </div>
  </div>


</body>
<?php } else
redirect('/Welcome/index/', 'refresh');
?>
