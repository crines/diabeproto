<body>
<?php if(($this->session->userdata('usr')>=TRUE)){?>
<div class="fondor">

  <div class=" container pt-5">
    <div >
      <center>
        <h1 class="blue">Nuevo Paciente</h1>
      </center>
    </div>
  </div>
    <div class="container">

        <div>
          <input type="hidden" name="idp" value="<?php echo $idp;?>">
      <div class="accordion" id="accordionExample"><!-- inicio contenedor-->
              <div class="card accordion2" onclick="butEnab2(this)" ><!-- inicio card-->
                <a class="card-link" data-toggle="collapse" href="#collapseOne">
                <div class="card-header" id="headingOner">
                  <h5 class="blue mb-0 titlecard">Paciente</h5>
                </div>
                </a>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOner" data-parent="#accordionExample">
                  <div class="card-body grad"><!-- contenido de la pestaña-->

                    <div class="form-group">
                            <label for="exampleInputEmail1">Diagnóstico</label>
                            <input type="text" name="diagnostico" class="form-control" id="diagnostico"  placeholder="Diagnóstico">
                    </div>
                    <div class="form-group">
                            <label for="exampleInputEmail1">Padecimiento</label>
                            <input type="text" name="padecimiento" class="form-control" id="padecimiento"  placeholder="Padecimiento">
                    </div>
                    <div class="form-group">
                            <label for="exampleInputEmail1">Exploración</label>
                            <input type="text" class="form-control" name="exploracion" id="exploracion"  placeholder="Exploración">
                    </div>
                    <div class="form-group">
                            <label for="exampleInputEmail1">Notas</label>
                            <input type="text" class="form-control" name="notas" id="notas"  placeholder="Notas">
                    </div>
                    <div class="form-group">
                            <label for="exampleInputEmail1">Interconsultas</label>
                            <input type="text" class="form-control" name="interconsultas" id="interconsultas"  placeholder="Interconsultas">
                    </div>
                </div><!--fin de contenido de la pestaña-->
              </div><!--fin contenedor 2-->
            </div><!-- fin card-->
              <button id="guardafichapaci" onclick="guardafichapaci(this)" class="btn btn-primary" disabled>Continuar Registro</button>
</div>
  </div>


</div>

</div>
 <?php }?>
</body>
