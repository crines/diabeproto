<?php
if($tipo!=null){
 if($graf!=FALSE){ ?>
<section>

<div class="container">

  <canvas id="myChart" width="100%"></canvas>
</div>

  <!--se crea script que su usara para la grafica -->
    <script>

    //se toma el canvas donde se desplegara la grafica
    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: '<?=$tipo?>',
        //type: 'bar',
        // The data for our dataset
        data: {
          //  labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio","Agosto","Septiembre", "Octubre","Noviembre", "Diciembre"],
          // se toman los valores de la consulta
          <?php $fecha= $gluc ="";
         foreach($graf->result() as $row){
          $fecha .= "'$row->Fecha', ";
          $gluc.="$row->Glucosa, ";
        }?>

          labels: [<?=$fecha?>],

            datasets: [{
                label: "Glucosa",
                borderColor: 'rgb(0, 0, 0)',
                //data: [0, 10, 5, 2, 20, 30, 45,30, 10,40,15,15],
                borderWidth: 2,
                hoverBackgroundColor: "rgba(192,250,139,0.4)",
                hoverBorderColor: "rgba(0,0,0,1)",
                data: [<?=$gluc?>],
                <?php if($tipo=="bar" || $tipo=="doughnut" || $tipo=="pie" || $tipo=="polarArea"){  ?>
                  backgroundColor:["rgb(255, 99, 132)","rgb(54, 162, 235)","rgb(255, 205, 86)","rgb(255, 127, 68)","rgb(217, 12, 232)","rgb(0, 66, 255)",
                  "rgb(6, 214, 121)","rgb(221, 255, 13)","rgb(24, 255, 224)","rgba(133, 255, 40, 0.43)","rgb(192, 81, 23)","rgb(58, 9, 195)"]
                <?php }else{ ?>
                    backgroundColor: "rgba(255,158,60,0.2)",
                     <?php } ?>

            }]
        },

        // Configuration options go here
        options: {
                      maintainAspectRatio: false,
                      scales: {
                        yAxes: [{
                          stacked: true,
                        }]

                      }
        }
    });
    chart.canvas.parentNode.style.height = '350px';
    </script>


</section>
<?php }else {
   echo "<script>alert('no hay datos');</script>";
}
} ?>
