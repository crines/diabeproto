<body>
<?php if(($this->session->userdata('usr')>=TRUE)){?>
<div class="fondor">

  <div class=" container pt-5">
    <div >
      <center>
        <h1 class="blue">Antecedentes</h1>
      </center>
    </div>
  </div>
    <div class="container">
                  <?php
                    foreach($idPaciente->result() as $row){
                        $idpas= "$row->idPaciente";
                      }?>
        <div>
          <input type="hidden" name="idpas" value="<?php echo $idpas;?>">
          <input type="hidden" name="idPersona" value="<?php echo $idPersona;?>">
      <div class="accordion" id="accordionExample"><!-- inicio contenedor-->
              <div class="card accordion2" onclick="butEnab3(this)"><!-- inicio card-->
                <a class="card-link" data-toggle="collapse" href="#collapseOne">
                <div class="card-header" id="headingOner">
                  <h5 class="blue mb-0 titlecard">Todos los Antecedentes</h5>
                </div>
                </a>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOner" data-parent="#accordionExample">
                  <div class="card-body grad"><!-- contenido de la pestaña-->


                                                     <br><h5 class="blue mb-0 titlecard">Antecedentes Patológicos</h5><br>
                                                     <input type="checkbox" name="Diabetes" value="si">Diabetes</input><br>
                                                     <input type="checkbox" name="Cardiopatia" value="si">Cardiopatía</input><br>
                                                     <input type="checkbox" name="Hipotiroidismo" value="si">Hipotiroidismo</input><br>
                                                     <input type="checkbox" name="Dislipidemia" value="si">Dislipidemia</input><br>
                                                     <input type="checkbox" name="Hepaticas" value="si">Hepáticas</input><br>
                                                     <input type="checkbox" name="Convulsion" value="si">Convulsión</input><br>
                                                     <input type="checkbox" name="Cancer" value="si">Cáncer</input><br>
                                                     <input type="checkbox" name="Transfusionales" value="si">Transfusionales</input><br>
                                                     <input type="checkbox" name="Quirurgicos" value="si">Quirúrgicos</input><br>
                                                     <input type="checkbox" name="Hipertension" value="si">Hipertensión</input><br>
                                                     <input type="checkbox" name="Insuficiencia" value="si">Insuficiencia</input><br>
                                                     <input type="checkbox" name="Hipertiroidismo" value="si">Hipertiroidismo</input><br>
                                                     <input type="checkbox" name="Marcapasos" value="si">Marcapasos</input><br>
                                                     <input type="checkbox" name="Renales" value="si">Renales</input><br>
                                                     <input type="checkbox" name="Artritis" value="si">Artritis</input><br>
                                                     <input type="checkbox" name="Dentales" value="si">Dentales</input><br>
                                                     <input type="checkbox" name="Traumaticos" value="si">Traumáticos</input><br>
                                                     <input type="checkbox" name="Alergico" value="si">Alérgicos</input><br><br>

                                                     <h5 class="blue mb-0 titlecard">Antecedentes No Patológicos</h5><br>
                                                    <input type="checkbox" name="Peso" value="si">¿Sabe cuanto pesa?</input><br>
                                                    <input type="checkbox" name="Obesidad" value="si">¿Se considera usted obeso?</input><br>
                                                    <input type="checkbox" name="Dieta" value="si">¿Hace algun tipo de dieta?</input><br>
                                                    <input type="checkbox" name="Ejercicio" value="si">¿Hace ejercicio?</input><br>
                                                    <input type="checkbox" name="Fumar" value="si">¿Fuma?</input><br>
                                                    <input type="checkbox" name="Bebida" value="si">¿Ingiere Bebidas alcoholicas?</input><br>
                                                    <input type="checkbox" name="control" value="si">¿Puede controlar la ingesta?</input><br>
                                                    <input type="checkbox" name="Drogas" value="si">¿Consume algun tipo de drogas?</input><br>
                                                    <input type="checkbox" name="Humo" value="si">¿Ha estado expuesto(a) a humo de leña o a polvos?</input><br>
                                                    <input type="checkbox" name="Viaje" value="si">¿Ha viajado recientemente?</input><br>
                                                    <input type="checkbox" name="Trabajados" value="si">¿Ha tenido otros trabajos anteriormente?</input><br>
                                                    <input type="checkbox" name="Casa" value="si">¿Su casa es propia?</input><br>
                                                    <input type="checkbox" name="Sangre" value="si">¿Conoce su tipo de sangre?</input><br>
                                                    <input type="checkbox" name="Mascotas" value="si">¿Convive con animales domesticos?</input><br>
                                                    <input type="checkbox" name="Seguro" value="si">¿Tiene seguridad Social?</input><br>
                                                    <input type="checkbox" name="vacunacion" value="si">¿Su esquema de vacunación esta completo?</input><br>
                                                    <input type="checkbox" name="Desparacitacion" value="si">¿Esta desparacitado?</input><br><br>

                                                     <h5 class="blue mb-0 titlecard">Antecedentes Heredofamiliares</h5><br>
                                                     <div class="form-group">
                                                             <label for="exampleInputEmail1">Parentesco </label>
                                                             <input type="text" class="form-control" name="Parentesco" id="Parentesco"  placeholder="Padres, Abuelos"><br>
                                                     </div>
                                                     <div class="form-group">
                                                            <div class="custom-control custom-radio custom-control-inline">
                                                                 <input type="radio" id="customRadio6" name="vivir" class="custom-control-input" value="Vivo" checked ><br>
                                                                 <label class="custom-control-label" for="customRadio6">Vivo</label>
                                                           </div>
                                                           <div class="custom-control custom-radio custom-control-inline">
                                                                 <input type="radio" id="customRadio7" name="vivir" class="custom-control-input" value="Finado"><br>
                                                                 <label class="custom-control-label" for="customRadio7">Finado</label>
                                                           </div>
                                                     </div>
                                                     <div class="form-group">
                                                             <label for="exampleInputEmail1">Enfermedad o causa de defunción</label>
                                                             <input type="text" class="form-control" name="Enfermedad" id="Enfermedad"  placeholder="Enfermedad"><br>
                                                     </div>

                </div><!--fin de contenido de la pestaña-->
              </div><!--fin contenedor 2-->
            </div><!-- fin card-->
  <button id="guardafichaante" onclick="guardafichaante(this)" class="btn btn-primary" disabled>Guardar Paciente</button>
</div>
  </div>


</div>

</div>
 <?php }?>
</body>
