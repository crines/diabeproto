<body class="">
<?php if(($this->session->userdata('usr')>=TRUE)){?>
<div class="main imm">

<div class="pleca">
  <center>
    <h2 style="color:#ffffff;" for="pacientes">Actualizar Datos</h2>
    <?php foreach ($persona->result() as $fila){?>
      <form class="form-group" action="index.html" method="post">
          <div id="accordion">
              <div class="card">
                    <div class="card-header" id="headingOne">
                      <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          Datos Generales del getPacientes
                        </button>
                      </h5>
                    </div>

                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">

                                          <div class="form-group">
                                                  <label for="exampleInputEmail1">Nombre</label>
                                                  <input type="text" name="nombre" class="form-control" id="nombre"  placeholder="Nombre"  value="<?=$fila->idPersona?>" data-placement="right" required>
                                          </div>
                                          <div class="form-group">
                                                  <label for="exampleInputEmail1">Fecha de Nacimiento</label>
                                                  <input type="date" name="fecha" class="form-control" id="fecha"  placeholder="Fecha"  value="<?=$fila->idPersona?>" data-placement="right" required>
                                          </div>
                                          <div class="form-group">
                                                  <label for="exampleInputEmail1">Edad</label>
                                                  <input type="number" name="edad" class="form-control" id="edad"  placeholder="Edad"  value="<?=$fila->idPersona?>" data-placement="right" required>
                                          </div>
                                          <div class="form-group">
                                                 <div class="custom-control custom-radio custom-control-inline">
                                                      <input type="radio" id="customRadio1" name="genero" value="Masculino" class="custom-control-input" checked>
                                                      <label class="custom-control-label" for="customRadio1" >Masculino</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                      <input type="radio" id="customRadio2" name="genero" value="Femenino" class="custom-control-input">
                                                      <label class="custom-control-label" for="customRadio2">Femenino</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                      <input type="radio" id="customRadio3" name="genero" value="Otros" class="custom-control-input">
                                                      <label class="custom-control-label" for="customRadio3">Otros</label>
                                                </div>
                                          </div>
                                          <div class="form-group">
                                                  <label for="exampleInputEmail1">Lugar de Nacimiento</label>
                                                  <input type="text" class="form-control" name="nacimiento" id="nacimiento"  placeholder="Puebla"  value="<?=$fila->idPersona?>" data-placement="right" required>
                                          </div>
                                          <div class="form-group">
                                                  <label for="exampleInputEmail1">Lugar de Residencia</label>
                                                  <input type="text" class="form-control" name="residencia" id="residencia"  placeholder="Puebla"  value="<?=$fila->idPersona?>" data-placement="right" required>
                                          </div>
                                          <div class="form-group">
                                                  <label for="exampleInputEmail1">Domicilio</label>
                                                  <input type="text" class="form-control" name="domicilio" id="domicilio"  placeholder="calle ..."  value="<?=$fila->idPersona?>" data-placement="right" required>
                                          </div>
                                          <div class="form-group">
                                                  <label for="exampleInputEmail1">Ocupación</label>
                                                  <input type="text" class="form-control"  name="ocupacion" id="ocupacion"  placeholder="Trabajador"  value="<?=$fila->idPersona?>" data-placement="right" required>
                                          </div>
                                          <div class="form-group">
                                                 <div class="custom-control custom-radio custom-control-inline">
                                                      <input type="radio" id="customRadio4" name="estado" value="Casado" class="custom-control-input">
                                                      <label class="custom-control-label" for="customRadio4">Casado(a)</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                      <input type="radio" id="customRadio5" name="estado" value="Soltero(a)" class="custom-control-input" checked>
                                                      <label class="custom-control-label" for="customRadio5">Soltero(a)</label>
                                                </div>
                                          </div>
                                          <div class="form-group">
                                                  <label for="exampleInputEmail1">Escolaridad</label>
                                                  <input type="text" class="form-control"  name="escolaridad" id="escolaridad"  placeholder="Universidad"  value="<?=$fila->idPersona?>" data-placement="right" required>
                                          </div>
                                          <div class="form-group">
                                                  <label for="exampleInputEmail1">Religión</label>
                                                  <input type="text" class="form-control" name="religion" id="religion"  placeholder="Catolico"  value="<?=$fila->idPersona?>" data-placement="right" required>
                                          </div>
                                          <div class="form-group">
                                                  <label for="exampleInputEmail1">Teléfono</label>
                                                  <input type="number" class="form-control"  name="telefono" id="telefono"  placeholder="222-222-2222"  value="<?=$fila->idPersona?>" data-placement="right" required>
                                          </div>
                                          <div class="form-group">
                                                  <label for="exampleInputEmail1">Correo Electrónico</label>
                                                  <input type="email" class="form-control"  name="correo" id="correo" aria-describedby="emailHelp" placeholder="correo@mail.com"  value="<?=$fila->idPersona?>" data-placement="right" >
                                          </div>
                    </div>
                  </div>
              </div>

              <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                      <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                       Especialidad Podologia
                      </button>
                    </h5>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
              </div>

              <div class="card">
              <div class="card-header" id="headingThree">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Especialidad Nutricion
                  </button>
                </h5>
              </div>
              <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
              </div>

              <div class="card">
              <div class="card-header" id="headingFour">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                    Especialidad Psicología
                  </button>
                </h5>
              </div>
              <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
              </div>

        </div>
        <button type="submit" name="button" value="Submit">Actuzalizar Datos</button>
      <?php }; ?>
      </form>
  </center>
       </div>
</div>
    <?php } else
   redirect('/Welcome/index/', 'refresh');
 ?>
</body>
