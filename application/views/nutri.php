<body>
<?php if(($this->session->userdata('usr')>=TRUE) && ($this->session->userdata('check')>=TRUE) ){?>

  <div class="container-fluid pt-3 ">
    <div class="maincontainer example">

      <div class="container-fluid">
      <div class="accordion" id="accordionExample">
        <div class="row">


        <div class="col-md-4">
              <div class="card accordion1">
                <a class="card-link">
                <div class="card-header" id="headingOnePos">
                      <h2 class="mb-0 titlecard">
                        <?php   foreach ($pacientes->result() as $fila){
                       }
                       echo "$fila->Nombre";?>
                     </h2>
                </div>
                  </a>
                <div>
                  <div class="card-body grad">
                    <div class="Center">
                        <br><?php   echo "Diagnostico: $fila->Diagnostico";?><br>
                        <br><?php   echo "Peso: $fila->Peso";?><br>
                        <br><?php   echo "Cintura: $fila->Cintura";?><br>
                        <br><?php   echo "Cadera: $fila->Cadera";?><br>
                    </div>
                    <div class="container pt-5 pb-5">
                    <div class="accordion" id="accordionExample2">
                            <div class="card">
                                <a class="card-link" data-toggle="collapse" href="#collapseOne1">
                              <div class="card-header" id="headingOne1">
                                    <h5 class="mb-0 blue"><i class="fas fa-user-circle"></i> Ficha de Identificación</h5>
                              </div>
                                </a>
                              <div id="collapseOne1" class="collapse" aria-labelledby="headingOne1" data-parent="#accordionExample2">
                                <div class="card-body">
                                    <div class="container pt-2 justify">
                                      <p><i class="fa fa-calendar fa-fw"></i><?php    echo "Fecha de nacimiento: $fila->Fecha";?></p>
                                      <p><i class="fa fa-address-card-o fa-fw"></i><?php    echo "Edad: $fila->Edad"; ?></p>
                                      <p><i class="fa fa-user fa-fw"></i><?php    echo "Genero: $fila->Genero"; ?></p>
                                      <p><i class="fa fa-map-marker fa-fw"></i><?php    echo "Lugar de nacimiento: $fila->Lugar"; ?></p>
                                      <p><i class="fa fa-map-marker fa-fw"></i><?php    echo "Lugar de Residencia: $fila->Residencia"; ?></p>
                                      <p><i class="fa fa-home fa-fw"></i><?php    echo "Domicilio: $fila->Domicilio"; ?></p>
                                      <p><i class="fa fa-briefcase fa-fw"></i><?php    echo "Ocupacion: $fila->Ocupacion"; ?></p>
                                      <p><i class="fa fa-bookmark fa-fw"></i><?php    echo "Estado Civil: $fila->Estado"; ?></p>
                                      <p><i class="fa fa-book fa-fw"></i><?php    echo "Escolaridad: $fila->Escolaridad"; ?></p>
                                      <p><i class="fa fa-shekel fa-fw"></i><?php    echo "Religion: $fila->Religion"; ?></p>
                                      <p><i class="fa fa-phone fa-fw"></i><?php    echo "Telefono: $fila->Telefono"; ?></p>
                                      <p><i class="fa fa-envelope fa-fw"></i><?php    echo "E-mail: $fila->Email"; ?></kp>
                                      <p><i class="fa fa-list-alt fa-fw"></i><?php    echo "Padecimiento: $fila->Padecimiento"; ?></p>
                                      <p><i class="fa fa-stethoscope fa-fw"></i><?php    echo "Exploracion: $fila->Exploracion"; ?></p>
                                    </div>
                                  </div>
                              </div>
                            </div>
                            <div class="card">
                              <a class="card-link" data-toggle="collapse" href="#collapseOne2">
                              <div class="card-header" id="headingOne2">
                                    <h5 class="mb-0 blue"><i class="far fa-sticky-note"></i> Notas</h5>
                              </div>
                                </a>
                              <div id="collapseOne2" class="collapse" aria-labelledby="headingOne2" data-parent="#accordionExample2">
                                <div class="card-body grad">
                                    <div class="container pt-2 justify">
                                      <!-- conseguir id de persona y paciente-->
                                      <input type="hidden" name="idpaci" value="<?php    echo "$fila->Paciente_idPaciente"; ?>">
                                      <input type="hidden" name="idp" value="<?php    echo "$fila->idPersona"; ?>">
                                      <p style="background-color: #ffffff; color:#000000"><textarea name="notas" id="editor1" rows="10" cols="25"> <?php    echo "$fila->Notas"; ?></textarea></p>
                                       <button name="nota-button"  id="nota-button">Guardar nota</button>

                                    </div>
                                </div>
                              </div>
                            </div>
                      </div>
                    </div>
                    </div>
                </div>
              </div>
              </div>

              <br />
              <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                      <div id="accordionExample1"><!-- inicio contenedor-->
                      <div class="card accordion1">
                        <a class="card-link" data-toggle="collapse" href="#collapseTwo">
                        <div class="card-header" id="headingTwo">
                          <h2 class="titlecard"><i class="fas fa-dumbbell"></i> Actividad Diaria</h2>
                        </div>
                        </a>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body grad">
                          <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                      <th scope="col">Tipo</th>
                                      <th scope="col">Frecuencia</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php if($actividad!=FALSE){
                                              foreach ($actividad->result() as $fila3){
                                     ?>
                                    <tr>
                                      <th scope="row"><?php echo "$fila3->Tipo";?></th>
                                      <td><?php echo "$fila3->Frecuencia"; ?></td>
                                    </tr>
                                    <?php
                                    }
                                  } ?>
                                </tbody>
                                  </table>
                            </div>
                       </div><!--fin de card-body-->
                     </div>
                    </div><!--fin de card-->
                    </div><!--fin de contenedor-->
                    </div>
                      <br>
                      <div class="col-md-12">
                        <div id="accordionExample2"><!-- inicio contenedor-->
                      <div class="card accordion1">
                        <a class="card-link" data-toggle="collapse" href="#collapseThree">
                        <div class="card-header" id="headingThree">
                          <h2 class="titlecard"><i class="fas fa-info-circle"></i> Dieta</h2>
                        </div>
                        </a>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body grad">
                          <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                      <th scope="col">Desayuno</th>
                                      <th scope="col">Colacion 1</th>
                                      <th scope="col">Comida</th>
                                      <th scope="col">Colacion 2</th>
                                      <th scope="col">Cena</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php if($dieta!=FALSE){
                                              foreach ($dieta->result() as $fila3){
                                     ?>
                                    <tr>
                                      <th scope="row"><?php echo "$fila3->Desayuno";?></th>
                                      <td><?php echo "$fila3->Colacion1"; ?></td>
                                      <td><?php echo "$fila3->Comida";?></td>
                                      <td><?php echo "$fila3->Colacion2"; ?></td>
                                      <td><?php echo "$fila3->Cena";?></td>
                                    </tr>
                                    <?php
                                    }
                                  } ?>
                                  </tbody>
                                  </table>
                            </div>
                        </div><!-- inicio card-body-->
                      </div>
                      </div><!-- inicio card-->
                    </div><!-- inicio contenedor-->
                      </div>
                      <br>
                      <div class="col-md-12">
                          <div class="card accordion1">
                            <a class="card-link" data-toggle="collapse" href="#collapseSix">
                              <div class="card-header" id="headingSix">
                                  <h2 class="titlecard"><i class="fas fa-utensils"></i> Habitos Alimenticios</h2>
                              </div>
                            </a>
                            <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                              <div class="card-body grad">
                                <div class="table-responsive">
                                  <table class="table table-bordered">
                                      <thead>
                                          <tr>
                                            <th scope="col">Frutas</th>
                                            <th scope="col">Verduras</th>
                                            <th scope="col">Cereales</th>
                                            <th scope="col">AOA</th>
                                            <th scope="col">lacteos</th>
                                            <th scope="col">Aceites</th>
                                            <th scope="col">Azucares</th>
                                            <th scope="col">Postres</th>
                                            <th scope="col">Agua Natural</th>
                                            <th scope="col">Agua de Sabor</th>
                                            <th scope="col">Cafe</th>
                                            <th scope="col">Te</th>
                                            <th scope="col">Otros</th>
                                            <th scope="col">Alcohol</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <?php if($frecuencia!=FALSE){
                                                    foreach ($frecuencia->result() as $fila3){
                                           ?>
                                          <tr>
                                            <th scope="row"><?php echo "$fila3->Frutas";?></th>
                                            <td><?php echo "$fila3->Verduras"; ?></td>
                                            <td><?php echo "$fila3->Cereales";?></td>
                                            <td><?php echo "$fila3->AOA"; ?></td>
                                            <td><?php echo "$fila3->Lacteos";?></td>
                                            <td><?php echo "$fila3->Aceites"; ?></td>
                                            <td><?php echo "$fila3->Azucares";?></td>
                                            <td><?php echo "$fila3->Postres"; ?></td>
                                            <td><?php echo "$fila3->AguaN";?></td>
                                            <td><?php echo "$fila3->AguaS"; ?></td>
                                            <td><?php echo "$fila3->Cafe";?></td>
                                            <td><?php echo "$fila3->Te"; ?></td>
                                            <td><?php echo "$fila3->Otros";?></td>
                                            <td><?php echo "$fila3->Alcohol"; ?></td>
                                          </tr>
                                          <?php
                                          }
                                        } ?>
                                        </tbody>
                                        </table>
                                  </div>

                              </div>
                            </div>
                        </div>
                      </div>

                            <br>
                  </div>
               </div>
               </div>
        </div>
      </div>
    </div>
  </div>


</body>
<?php } else
redirect('/Welcome/index/', 'refresh');
?>
