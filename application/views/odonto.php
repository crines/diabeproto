<body>
<?php if(($this->session->userdata('usr')>=TRUE)){?>
  <div class="container-fluid ">
    <div class="maincontainer example">

      <div class="container-fluid pt-5 pb-5">
      <div class="accordion" id="accordionExample">
        <div class="row">


        <div class="col-md-4">
              <div class="card accordion1">
                <a class="card-link">
                <div class="card-header" id="headingOnePos">
                      <h2 class="mb-0 titlecard">
                        Odontologia
                     </h2>
                </div>
                  </a>
                <div>
                  <div class="card-body grad">
                    <br> <h3 class="white"><i class="fa fa-stethoscope fa-fw"></i>Diagnostico</h3><br>
                    <div class="container pt-5 pb-5">
                    <div class="accordion" id="accordionExample2">
                            <div class="card">
                                <a class="card-link" data-toggle="collapse" href="#collapseOne1">
                              <div class="card-header" id="headingOne1">
                                    <h5 class="mb-0 blue"><i class="fas fa-user-circle"></i> Ficha de Identificación</h5>
                              </div>
                                </a>
                              <div id="collapseOne1" class="collapse" aria-labelledby="headingOne1" data-parent="#accordionExample2">
                                <div class="card-body">
                                    <div class="container pt-2 justify">

                                    </div>
                                  </div>
                              </div>
                            </div>
                            <div class="card">
                              <a class="card-link" data-toggle="collapse" href="#collapseOne2">
                              <div class="card-header" id="headingOne2">
                                    <h5 class="mb-0 blue"><i class="far fa-sticky-note"></i> Notas</h5>
                              </div>
                                </a>
                              <div id="collapseOne2" class="collapse" aria-labelledby="headingOne2" data-parent="#accordionExample2">
                                <div class="card-body grad">
                                    <div class="container pt-2 justify">

                                    </div>
                                </div>
                              </div>
                            </div>
                      </div>
                    </div>
                    </div>
                </div>
              </div>
              </div>

              <br />
              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-12">
                    <div class="card accordion1">
                      <a class="card-link" data-toggle="collapse" href="#collapseOne">
                        <div class="card-header" id="headingOne">
                            <h2 class="titlecard"><i class="fa fa-medkit fa-fw"></i>Odontologia</h2>
                        </div>
                      </a>
                      <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body grad">

                          </div>
                      </div>
                  </div>
                     </div>
                     <br />
                    <div class="col-md-12">
                      <div id="accordionExample1"><!-- inicio contenedor-->
                      <div class="card accordion1">
                        <a class="card-link" data-toggle="collapse" href="#collapseTwo">
                        <div class="card-header" id="headingTwo">
                          <h2 class="titlecard"><i class="fa fa-tint fa-fw"></i>Odontologia</h2>
                        </div>
                        </a>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body grad">

                       </div><!--fin de card-body-->
                     </div>
                    </div><!--fin de card-->
                    </div><!--fin de contenedor-->
                    </div>
                      <br>
                      <div class="col-md-12">
                        <div id="accordionExample2"><!-- inicio contenedor-->
                      <div class="card accordion1">
                        <a class="card-link" data-toggle="collapse" href="#collapseThree">
                        <div class="card-header" id="headingThree">
                          <h2 class="titlecard"><i class="fa fa-navicon fa-fw"></i>Odontologia</h2>
                        </div>
                        </a>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body grad">

                        </div><!-- inicio card-body-->
                      </div>
                      </div><!-- inicio card-->
                    </div><!-- inicio contenedor-->
                      </div>
                      <br>
                      <div class="col-md-12">
                          <div class="card accordion1">
                            <a class="card-link" data-toggle="collapse" href="#collapseSix">
                              <div class="card-header" id="headingSix">
                                  <h2 class="titlecard"><i class="fa fa-medkit fa-fw"></i>Odontologia</h2>
                              </div>
                            </a>
                            <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                              <div class="card-body grad">


                              </div>
                            </div>
                        </div>
                      </div>

                            <br>
                              <div class="col-md-12">
                                <div class="card accordion1">
                                  <a class="card-link" data-toggle="collapse" href="#collapseFour">
                                    <div class="card-header" id="headingFour">
                                        <h2 class="titlecard"><i class="fa fa-medkit fa-fw"></i>Subir analisis clinicos</h2>
                                    </div>
                                  </a>
                                  <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                                    <div class="card-body grad">
                                     <form enctype="multipart/form-data" method="post" action="<?php echo base_url();?>index.php/welcome/uploimg/<?=$fila->idPersona?>">
                                        <label for="imagen">Imagen:</label>
                                        <input name="imagen" type="file"/>
                                        <input type="submit" value="subir" />
                                      </form>
                                              <!--    <div class="input-group mb-3">
                                            <div class="custom-file">
                                              <input type="file" class="custom-file-input" id="inputGroupFile02">
                                              <label class="custom-file-label" for="inputGroupFile02">Choose file</label>
                                            </div>
                                            <div class="input-group-append">
                                              <span class="input-group-text" id="">Upload</span>
                                            </div>
                                          </div>-->
                                                </div>
                                              </div>
                                          </div>
                              </div>
                              <br>
                              <div class="col-md-12">
                                <div class="card accordion1">
                                  <a class="card-link" data-toggle="collapse" href="#collapseFive">
                                    <div class="card-header" id="headingFive">
                                        <h2 class="titlecard"><i class="fa fa-medkit fa-fw"></i>analisis clinicos</h2>
                                    </div>
                                  </a>
                                  <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                                    <div class="card-body grad">

                                                       <p>imagenes:</p>

                                                            <?php
                                                             if($imagen!=null){
                                                               foreach($imagen->result() as $row1)
                                                                $img1=$row1->img;?>
                                                                <img onclick="javascript:this.width=905;this.height=1199" ondblclick="javascript:this.width=300;this.height=300" src="<?php echo base_url().$img1;?>" width="300"/>
                                                                <?php } ?>
                                                              </div>
                                                            </div>
                                                        </div>
                              </div>
                  </div>
               </div>
               </div>
        </div>
      </div>
    </div>
  </div>
  </body>
<?php } else
redirect('/Welcome/index/', 'refresh');
?>
