<?php if(($this->session->userdata('usr')>=TRUE)  && ($this->session->userdata('check')>=TRUE) ){?>
<div class="container-fluid pt-3 ">
  <h2>Datos</h2>
  <div class="grafos">
    <p name="agregagrafi" id="agregagrafi" style="margin:6px;"><i class="fas fa-plus fa-1x blue"></i> Nuevo Registro</p>

    <div class="table-responsive">
      <table class="table table-bordered">
      <tr name="agregra" id="agregrafi">
        <input type="hidden" name="idpacig" value="<?php    echo "$pacien"; ?>">
        <input type="hidden" name="idpg" value="<?php    echo "$person"; ?>">
        <th> <div class="input-group mb-3">
          <div class="input-group-prepend">
          <label class="input-group-text" for="glu1">Glucosa:</label>
          </div>
          <input type="text" class="form-control" id="glu1" placeholder="mg/dl" name="glu1">
        </div></th>
        <td><div class="input-group mb-3">
          <div class="input-group-prepend">
          <label class="input-group-text" for="dia1">día:</label>
          </div>
          <input type="date" class="form-control" id="dia1" placeholder="Fecha" name="dia1">
        </div></td>
        <td> <button name="chartse" id="chartse"><i class="far fa-save fa-2x blue"></i></button> </td>
      </tr>
    </table>

      <table class="table table-bordered">
        <thead>
            <tr>
              <th scope="col">Glucosa</th>
              <th scope="col">Fecha</th>
            </tr>
          </thead>
          <tbody class="bodygra">
            <?php
            $k=0;
            if($graficas!=FALSE){
              //$i=0;

             foreach ($graficas->result() as $fila3){?>

            <tr id="<?php echo "graf"."$fila3->idGraf";?>">
              <?php $k=$k+1; ?>
              <th><input type="text" name="<?php echo $k;?>" id="<?php echo $k;?>" value="<?php echo "$fila3->Glucosa";?>" disabled> </th>
              <?php $k=$k+1; ?>
              <td> <input type="text" name="<?php echo $k;?>" id="<?php echo $k;?>" value="<?php echo "$fila3->Fecha"; ?>" disabled> </td>
              <?php $k=$k+1; ?>
              <td style="border:0;">
                 <button class="btt-graf-e" id="<?php echo "r".$k;?>" value="<?php echo $k;?>" onclick="editG(this)"><i class="far fa-edit fa-2x blue"></i></button>
                 <button class="btt-graf" id="<?php echo "t".$k;?>" value="<?php echo "$fila3->idGraf";?>" onclick="saveEditG(this)" style="display:none"><i class="far fa-save fa-2x blue"></i></button>
                 <button class="btt-graf" id="<?php echo "y".$k;?>" value="<?php echo "$fila3->idGraf";?>" onclick="alertaG(this)"><i class="fas fa-trash fa-2x blue"></i></button>
                </td>
              <td style="border:0;"> </td>
            </tr>


        <?php }
       ?>
         </tbody>
     <?php } ?>
     <input type="hidden" name="eddig" id="eddig" value="" disabled>
     <input type="hidden" name="img" id="img" value="<?php echo $k;?>" disabled>
     <input type="hidden" id="checkg" name="" disabled>
      </table>


    </div>
    <div class="as-console">

    </div>
          <form class="" action="<?php echo base_url();?>index.php/welcome/grafica/" method="post">


              <div class="input-group mb-3">
               <div class="input-group-prepend">
                 <label class="input-group-text" for="inputGroupSelect01">Gráfica: </label>
               </div>
               <select class="custom-select tipo" id="tipo" name="tipo">
                 <option value="line">Línea</option>
                 <option value="bar">Barra</option>
                 <option value="pie">Pastel</option>
                 <option value="doughnut">Dona</option>
                 <option value="polarArea">Polar area</option>
               </select>
              </div>
              <button name="graficacool" id="graficacool" type="submit">Graficar</button>
              </form>



  </div>
</div>
 <?php }?>
