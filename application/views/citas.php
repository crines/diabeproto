<!-- Button trigger modal en views/calendario/... -->
        <!-- Modal -->
        <div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">cita</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div>
                  <div class="form-group">
                    <label for="recipient-fecha" class="col-form-label">Fecha:</label>
                    <input id="inputcalendar" width="312" name="fechacita" />
                  </div>
                  <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Paciente</label>
                    <select class="busqpaciente" id="pacientescali" name="pacientescita" style="width: 90%;">
                      <option value="">seleccione...</option>
                    <?php foreach($pacientes->result() as $fila) { ?>
                     <option value="<?=$fila ->Nombre?>"><?=$fila ->Nombre?></option><?php } ?>
                    </select>
                  </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                  <button class="btn btn-blue" id="Creacita" onclick="creacita(this)" name="agregar">Guardar</button>

              </div>
            </div>
            </div>
          </div>
        </div>
