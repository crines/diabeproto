<body>
<?php if(($this->session->userdata('usr')>=TRUE) && ($this->session->userdata('check')>=TRUE) ){?>

  <div class="container-fluid pt-3 ">
    <div class="maincontainer example">

      <div class="container-fluid">
      <div class="accordion" id="accordionExample">
        <div class="row">


        <div class="col-md-4">
              <div class="card accordion1">
                <a class="card-link">
                <div class="card-header" id="headingOnePos">
                      <h2 class="mb-0 titlecard">
                        <?php   foreach ($pacientes->result() as $fila){
                       }
                       echo "$fila->Nombre";?>
                     </h2>
                </div>
                  </a>
                <div>
                  <div class="card-body grad">
                    <div class="Center">
                        <br><?php   echo "Motivo: $fila->Motivo";?><br>
                        <br><?php   echo "Diagnostico: $fila->Diagnostico";?><br>
                        <br><?php   echo "Observaciones: $fila->Observaciones";?><br>
                        <br><?php   echo "Tratamiento: $fila->Tratamiento";?><br>
                        <br><?php   echo "Recomendaciones: $fila->Recomendaciones";?><br>
                    </div>
                    <div class="container pt-5 pb-5">
                    <div class="accordion" id="accordionExample2">
                            <div class="card">
                                <a class="card-link" data-toggle="collapse" href="#collapseOne1">
                              <div class="card-header" id="headingOne1">
                                    <h5 class="mb-0 blue"><i class="fas fa-user-circle"></i> Ficha de Identificación</h5>
                              </div>
                                </a>
                              <div id="collapseOne1" class="collapse" aria-labelledby="headingOne1" data-parent="#accordionExample2">
                                <div class="card-body">
                                    <div class="container pt-2 justify">
                                      <p><i class="fa fa-calendar fa-fw"></i><?php    echo "Fecha de nacimiento: $fila->Fecha";?></p>
                                      <p><i class="fa fa-address-card-o fa-fw"></i><?php    echo "Edad: $fila->Edad"; ?></p>
                                      <p><i class="fa fa-user fa-fw"></i><?php    echo "Genero: $fila->Genero"; ?></p>
                                      <p><i class="fa fa-map-marker fa-fw"></i><?php    echo "Lugar de nacimiento: $fila->Lugar"; ?></p>
                                      <p><i class="fa fa-map-marker fa-fw"></i><?php    echo "Lugar de Residencia: $fila->Residencia"; ?></p>
                                      <p><i class="fa fa-home fa-fw"></i><?php    echo "Domicilio: $fila->Domicilio"; ?></p>
                                      <p><i class="fa fa-briefcase fa-fw"></i><?php    echo "Ocupacion: $fila->Ocupacion"; ?></p>
                                      <p><i class="fa fa-bookmark fa-fw"></i><?php    echo "Estado Civil: $fila->Estado"; ?></p>
                                      <p><i class="fa fa-book fa-fw"></i><?php    echo "Escolaridad: $fila->Escolaridad"; ?></p>
                                      <p><i class="fa fa-shekel fa-fw"></i><?php    echo "Religion: $fila->Religion"; ?></p>
                                      <p><i class="fa fa-phone fa-fw"></i><?php    echo "Telefono: $fila->Telefono"; ?></p>
                                      <p><i class="fa fa-envelope fa-fw"></i><?php    echo "E-mail: $fila->Email"; ?></kp>
                                      <p><i class="fa fa-list-alt fa-fw"></i><?php    echo "Padecimiento: $fila->Padecimiento"; ?></p>
                                      <p><i class="fa fa-stethoscope fa-fw"></i><?php    echo "Exploracion: $fila->Exploracion"; ?></p>
                                    </div>
                                  </div>
                              </div>
                            </div>
                            <div class="card">
                              <a class="card-link" data-toggle="collapse" href="#collapseOne2">
                              <div class="card-header" id="headingOne2">
                                    <h5 class="mb-0 blue"><i class="far fa-sticky-note"></i> Notas</h5>
                              </div>
                                </a>
                              <div id="collapseOne2" class="collapse" aria-labelledby="headingOne2" data-parent="#accordionExample2">
                                <div class="card-body grad">
                                    <div class="container pt-2 justify">
                                      <!-- conseguir id de persona y paciente-->
                                      <input type="hidden" name="idpaci" value="<?php    echo "$fila->Paciente_idPaciente"; ?>">
                                      <input type="hidden" name="idp" value="<?php    echo "$fila->idPersona"; ?>">
                                      <p style="background-color: #ffffff; color:#000000"><textarea name="notas" id="editor1" rows="10" cols="25"> <?php    echo "$fila->Notas"; ?></textarea></p>
                                       <button name="nota-button"  id="nota-button">Guardar nota</button>

                                    </div>
                                </div>
                              </div>
                            </div>
                      </div>
                    </div>
                    </div>
                </div>
              </div>
              </div>

              <br />
              <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                      <div id="accordionExample1"><!-- inicio contenedor-->
                      <div class="card accordion1">
                        <a class="card-link" data-toggle="collapse" href="#collapseTwo">
                        <div class="card-header" id="headingTwo">
                          <h2 class="titlecard"><i class="fas fa-shoe-prints"></i> Pie Derecho</h2>
                        </div>
                        </a>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body grad">
                          <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                      <th scope="col">Plantar</th>
                                      <th scope="col">Dorsal</th>
                                      <th scope="col">Talar</th>
                                      <th scope="col">Onicrocriptosis</th>
                                      <th scope="col">Onicomicosis</th>
                                      <th scope="col">Onicograifosis</th>
                                      <th scope="col">Bullosis</th>
                                      <th scope="col">Ulceras</th>
                                      <th scope="col">Necrosis</th>
                                      <th scope="col">Grietas</th>
                                      <th scope="col">Lesion</th>
                                      <th scope="col">Anhidrosis</th>
                                      <th scope="col">Tiñas</th>
                                      <th scope="col">Infecciones</th>
                                      <th scope="col">Dedosgar</th>
                                      <th scope="col">Dedosmar</th>
                                      <th scope="col">Hallux</th>
                                      <th scope="col">Infraductos</th>
                                      <th scope="col">Supraductos</th>
                                      <th scope="col">Hipercargas</th>
                                      <th scope="col">Pulsope</th>
                                      <th scope="col">Pulsoti</th>
                                      <th scope="col">Llenado</th>
                                      <th scope="col">Edena</th>
                                      <th scope="col">Sensacion</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php if($derecha!=FALSE){
                                              foreach ($derecha->result() as $fila3){
                                     ?>
                                    <tr>
                                      <th scope="row"><?php echo "$fila3->Plantar";?></th>
                                      <td><?php echo "$fila3->Dorsal"; ?></td>
                                      <td><?php echo "$fila3->Talar"; ?></td>
                                      <td><?php echo "$fila3->Onicrocriptosis"; ?></td>
                                      <td><?php echo "$fila3->Onicomicosis"; ?></td>
                                      <td><?php echo "$fila3->Onicograifosis"; ?></td>
                                      <td><?php echo "$fila3->Bullosis"; ?></td>
                                      <td><?php echo "$fila3->Ulceras"; ?></td>
                                      <td><?php echo "$fila3->Necrosis"; ?></td>
                                      <td><?php echo "$fila3->Grietas"; ?></td>
                                      <td><?php echo "$fila3->Lesion"; ?></td>
                                      <td><?php echo "$fila3->Anhidrosis"; ?></td>
                                      <td><?php echo "$fila3->Tiñas"; ?></td>
                                      <td><?php echo "$fila3->Infecciones"; ?></td>
                                      <td><?php echo "$fila3->Dedosgar"; ?></td>
                                      <td><?php echo "$fila3->Dedosmar"; ?></td>
                                      <td><?php echo "$fila3->Hallux"; ?></td>
                                      <td><?php echo "$fila3->Infraductos"; ?></td>
                                      <td><?php echo "$fila3->Supraductos"; ?></td>
                                      <td><?php echo "$fila3->Hipercargas"; ?></td>
                                      <td><?php echo "$fila3->Pulsope"; ?></td>
                                      <td><?php echo "$fila3->Pulsoti"; ?></td>
                                      <td><?php echo "$fila3->Llenado"; ?></td>
                                      <td><?php echo "$fila3->Edena"; ?></td>
                                      <td><?php echo "$fila3->Sensacion"; ?></td>
                                    </tr>
                                    <?php
                                    }
                                  } ?>
                                </tbody>
                                  </table>
                            </div>
                       </div><!--fin de card-body-->
                     </div>
                    </div><!--fin de card-->
                    </div><!--fin de contenedor-->
                    </div>
                      <br>
                      <div class="col-md-12">
                        <div id="accordionExample2"><!-- inicio contenedor-->
                      <div class="card accordion1">
                        <a class="card-link" data-toggle="collapse" href="#collapseThree">
                        <div class="card-header" id="headingThree">
                          <h2 class="titlecard"><i class="fas fa-shoe-prints"></i> Pie Izquiero</h2>
                        </div>
                        </a>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body grad">
                          <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                      <th scope="col">Plantar</th>
                                      <th scope="col">Dorsal</th>
                                      <th scope="col">Talar</th>
                                      <th scope="col">Onicrocriptosis</th>
                                      <th scope="col">Onicomicosis</th>
                                      <th scope="col">Onicograifosis</th>
                                      <th scope="col">Bullosis</th>
                                      <th scope="col">Ulceras</th>
                                      <th scope="col">Necrosis</th>
                                      <th scope="col">Grietas</th>
                                      <th scope="col">Lesion</th>
                                      <th scope="col">Anhidrosis</th>
                                      <th scope="col">Tiñas</th>
                                      <th scope="col">Infecciones</th>
                                      <th scope="col">Dedosgar</th>
                                      <th scope="col">Dedosmar</th>
                                      <th scope="col">Hallux</th>
                                      <th scope="col">Infraductos</th>
                                      <th scope="col">Supraductos</th>
                                      <th scope="col">Hipercargas</th>
                                      <th scope="col">Pulsope</th>
                                      <th scope="col">Pulsoti</th>
                                      <th scope="col">Llenado</th>
                                      <th scope="col">Edena</th>
                                      <th scope="col">Sensacion</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php if($izquierda!=FALSE){
                                              foreach ($izquierda->result() as $fila3){
                                     ?>
                                    <tr>
                                      <th scope="row"><?php echo "$fila3->Plantar";?></th>
                                      <td><?php echo "$fila3->Dorsal"; ?></td>
                                      <td><?php echo "$fila3->Talar"; ?></td>
                                      <td><?php echo "$fila3->Onicrocriptosis"; ?></td>
                                      <td><?php echo "$fila3->Onicomicosis"; ?></td>
                                      <td><?php echo "$fila3->Onicograifosis"; ?></td>
                                      <td><?php echo "$fila3->Bullosis"; ?></td>
                                      <td><?php echo "$fila3->Ulceras"; ?></td>
                                      <td><?php echo "$fila3->Necrosis"; ?></td>
                                      <td><?php echo "$fila3->Grietas"; ?></td>
                                      <td><?php echo "$fila3->Lesion"; ?></td>
                                      <td><?php echo "$fila3->Anhidrosis"; ?></td>
                                      <td><?php echo "$fila3->Tiñas"; ?></td>
                                      <td><?php echo "$fila3->Infecciones"; ?></td>
                                      <td><?php echo "$fila3->Dedosgar"; ?></td>
                                      <td><?php echo "$fila3->Dedosmar"; ?></td>
                                      <td><?php echo "$fila3->Hallux"; ?></td>
                                      <td><?php echo "$fila3->Infraductos"; ?></td>
                                      <td><?php echo "$fila3->Supraductos"; ?></td>
                                      <td><?php echo "$fila3->Hipercargas"; ?></td>
                                      <td><?php echo "$fila3->Pulsope"; ?></td>
                                      <td><?php echo "$fila3->Pulsoti"; ?></td>
                                      <td><?php echo "$fila3->Llenado"; ?></td>
                                      <td><?php echo "$fila3->Edena"; ?></td>
                                      <td><?php echo "$fila3->Sensacion"; ?></td>

                                    </tr>
                                    <?php
                                    }
                                  } ?>
                                  </tbody>
                                  </table>
                            </div>
                        </div><!-- inicio card-body-->
                      </div>
                      </div><!-- inicio card-->
                    </div><!-- inicio contenedor-->
                      </div>
                      <br>
                  </div>
               </div>
               </div>
        </div>
      </div>
    </div>
  </div>


</body>
<?php } else
redirect('/Welcome/index/', 'refresh');
?>
