<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'/third_party/Spout/Autoloader/autoload.php';

 use Box\Spout\Reader\ReaderFactory;
 use Box\Spout\Common\Type;
class Welcome extends CI_Controller {

    function __construct()
    {
     parent::__construct();
     $this->load->model('Bases_model');
     $this->load->library('grocery_CRUD');
     //$this->load->library('encrypt');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

public function hash(){
  $this->load->view('hash');
}
public function cargahash(){
  $data= array(
    'user'=>$this->input->post('user'),
    'id'=>$this->input->post('id')
  );
  $pass=$this->input->post('pass');
$hash=password_hash($pass, PASSWORD_DEFAULT);
$data['pass']=$hash;
$this->Bases_model->agregahash($data);
    //echo $hash;
}

  public function index(){
      $this->load->view('index2');
  }

  public function validaLoginv2() {
    $data = array(
             'user'=> $this->input->post('user'),'password'=>$this->input->post('pass'));
             $usuarios=$this->Bases_model->obtenerDoc($data);
             if($usuarios==true){
               foreach ($usuarios->result() as $value) {
                 $hash=$value->psw;
               }
               $usuario=password_verify($data['password'], $hash);
             //$usuarios = $this->Bases_model->validaLogin($data);
             if($usuario==TRUE){
               $datasession="";
               foreach($usuarios->result() as $row){
                   $datasession = array(
                   'user'=> $row->usrdoc,'password'=> $row->psw, 'usr'=>TRUE, 'paciente'=>null, 'check'=>FALSE, 'tipo'=>0);
               }
               $doc=$data['user'];
               $idDoc=$this->Bases_model->getidDoc($doc);
               foreach ($idDoc->result() as $fila){
                 $id=$fila->idDoctor;
                 }
                 $this->session->set_userdata($datasession);
                 $this->session->set_userdata('tipo',$id);
                  if($id==3 || $id==5){
                    echo "Bienvenida Doctora ",$doc;
                  }else {
                      echo "Bienvenido Doctor ",$doc;
                  }

           }else {
             return false;
           }
         }else {
           return false;
         }
}


  public function bienvenida(){
    $this->session->set_userdata('check', FALSE);
    $this->load->view('head');
    $data1['da']=2;
    $this->load->view('encabezado',$data1);
    $tipo=$_SESSION['tipo'];
    $datacit['citas']=$this->Bases_model->ViewCitas($tipo);
    $this->load->view('calendario',$datacit);

    if($tipo==1){
    $data['pacientes']=$this->Bases_model->getPacientes();
    $this->load->view('home',$data);
    }
    if($tipo==2){
    $data['pacientes']=$this->Bases_model->getPacientesPod($tipo);
    $this->load->view('hpodo',$data);
    }
    if($tipo==3){
    $data['pacientes']=$this->Bases_model->getPacientesNut($tipo);
    //$data['pacientes']=$this->Bases_model->getPacientes();
    $this->load->view('hnutri',$data);
    }
    if($tipo==4){
    $data['pacientes']=$this->Bases_model->getPacientesPsy($tipo);
    $this->load->view('hpsico',$data);
    }
    if($tipo==5){
    $lis['lis']=$this->Bases_model->listado();
    $this->load->view('listado',$lis);
    }
    $this->load->view('footer');
  }


    public function cerrarSesion()
    {
     $datasession = array('nivel' => '');
        $this->session->unset_userdata($datasession);
        unset($_SESSION['paciente'],$_SESSION['check']);
       $this->session->sess_destroy();
       redirect('/Welcome/index/', 'refresh');
     }


/*---------------------------------------
--------------------------------------------
-------------------------------------------*/

public function buscadorv2(){
  if($this->session->userdata('check')==FALSE){
  $busca=$this->input->post('pacientes');
  //$paciente = $this->Bases_model->getperson($busca);
  $paciente= $this->Bases_model->getFicha($busca);
  $ante= $this->Bases_model->getAnte($busca);
  $medicament = $this->Bases_model->getmedicament($busca);
  $imagen=$this->Bases_model->ViewImg($busca);
  $pdf=$this->Bases_model->Viewpdf($busca);
            if($paciente!=FALSE){
                $this->session->set_userdata('paciente',$busca);
                $this->session->set_userdata('check',TRUE);
                $data['pacientes']=$paciente;
                $data['antecedentes']=$ante;
                $data['medicament']=$medicament;
                $data['imagen']=$imagen;
                $data['pdf']=$pdf;
                $data1['da']=1;
                $data1['idp']=$busca;
                $this->load->view('head');
                $this->load->view('encabezado',$data1);
                $tipo=$_SESSION['tipo'];
                $datacit['citas']=$this->Bases_model->ViewCitas($tipo);
                $this->load->view('calendario',$datacit);
                $datacitas['pacientes']=$this->Bases_model->getPacientes();
                $this->load->view('citas',$datacitas);
                 $this->load->view('buscador',$data);
                 $this->load->view('footer');

               }else {
                   redirect('/Welcome/bienvenida/', 'refresh');
               }
   }else {
     $busca=$_SESSION['paciente'];
     //$paciente = $this->Bases_model->getperson($busca);
     $paciente= $this->Bases_model->getFicha($busca);
     $ante= $this->Bases_model->getAnte($busca);
     $medicament = $this->Bases_model->getmedicament($busca);
     $imagen=$this->Bases_model->ViewImg($busca);
     $pdf=$this->Bases_model->Viewpdf($busca);
     $this->session->set_userdata('paciente',$busca);
     $this->session->set_userdata('check',TRUE);
     $data['pacientes']=$paciente;
     $data['antecedentes']=$ante;
     $data['medicament']=$medicament;
     $data['imagen']=$imagen;
     $data['pdf']=$pdf;
     $data1['da']=1;
     $data1['idp']=$busca;
     $this->load->view('head');
     $this->load->view('encabezado',$data1);
     $tipo=$_SESSION['tipo'];
     $datacit['citas']=$this->Bases_model->ViewCitas($tipo);
     $this->load->view('calendario',$datacit);
     $datacitas['pacientes']=$this->Bases_model->getPacientes();
     $this->load->view('citas',$datacitas);
      $this->load->view('buscador',$data);
      $this->load->view('footer');
   }
}

/*aqui se esta usando ajax en funciones*/
public function agregatipo(){
    $data=array(
          'idp'=> $this->input->post('idp'),
          'idpaci'=> $this->input->post('idpaci'),
          'tipo'=> $this->input->post('id')
    );
    if ($data['tipo']==2) {
      $mens="Podología";
      $this->Bases_model->agregaMpodo($data);
    }
    if ($data['tipo']==3) {
      $mens="Nutriología";
      $this->Bases_model->agregaMnutri($data);
    }
    if ($data['tipo']==4) {
      $mens="Psicología";
      $this->Bases_model->agregaMpsico($data);
    }
    if ($data['tipo']==5) {
      $mens="Odontología";
      $this->Bases_model->agregaModon($data);
    }
    echo $mens;

}
public function quitartipo(){
  $data=array(
        'idp'=> $this->input->post('idp'),
        'idpaci'=> $this->input->post('idpaci'),
        'tipo'=> null
  );
  $ti= $this->input->post('id');
  if ($ti==2) {
    $mens="Podología";
    $this->Bases_model->quitaMpodo($data);
  }
  if ($ti==3) {
    $mens="Nutriología";
    $this->Bases_model->quitaMnutri($data);
  }
  if ($ti==4) {
    $mens="Psicología";
    $this->Bases_model->quitaMpsico($data);
  }
  if ($ti==5) {
    $mens="Odontología";
    $this->Bases_model->quitaModon($data);
  }

  echo $mens;
}

public function notasv2(){
  $data = array(
           'notas'=> $this->input->post('notas'),'id'=>$this->input->post('idp'));
   $this->Bases_model->SetNotas($data);

   //$this->Bases_model->SetNotas($data);
  //$this->db->insert('items', $data);
  //echo 'Added successfully.';
}

public function interv2(){
  $data = array(
           'interconsultas'=> $this->input->post('interconsultas'),'id'=>$this->input->post('idp'));
  $this->Bases_model->SetInter($data);
}

public function agregarmedica(){

  $data = array(
           'nombre'=>$this->input->post('nombre'),
           'dosis'=>$this->input->post('dosis'),
           'horario'=>$this->input->post('horario'),
           'observaciones'=>$this->input->post('observaciones'),
           'idpaci'=> $this->input->post('idpaci'),
           'idp'=> $this->input->post('idp'));
           $this->Bases_model->Agmedicamen($data);
           $idd=$this->Bases_model->ObtidM();
           foreach ($idd->result() as $fila){
             $id=$fila->idMedicamentos;
          }
         echo $id;


}

public function editarMedica(){
  $data = array(
           'idm'=>$this->input->post('idm'),
           'nombre'=>$this->input->post('nombre'),
           'dosis'=>$this->input->post('dosis'),
           'horario'=>$this->input->post('horario'),
           'observaciones'=>$this->input->post('observa'),
           'idpaci'=> $this->input->post('idpaci'),
           'idp'=> $this->input->post('idp'));
           $this->Bases_model->Edmedicamen($data);
}

public function delete(){
  $delete=$this->input->post('idde');
  $this->Bases_model->deleteMedicam($delete);
}

/*----------------Nutricion--------------------------
-------------------------------------------
-------------------------------------------*/
   public function nutri($id=0){
     if($this->session->userdata('check')==FALSE){
     $busca=$this->input->post('pacientes');
     //$paciente = $this->Bases_model->getperson($busca);
     $paciente= $this->Bases_model->getpersonutri($busca);
     $actividad= $this->Bases_model->getActividad($busca);
     $dieta = $this->Bases_model->getDieta($busca);
     $frecuencia=$this->Bases_model->getFrecuencia($busca);
               if($paciente!=FALSE){
                   $this->session->set_userdata('paciente',$busca);
                   $this->session->set_userdata('check',TRUE);
                   $data['pacientes']=$paciente;
                   $data['actividad']=$actividad;
                   $data['dieta']=$dieta;
                   $data['frecuencia']=$frecuencia;
                   $data1['da']=1;
                   $data1['idp']=$busca;
                   $this->load->view('head');
                   $this->load->view('encabezado',$data1);
                   $tipo=$_SESSION['tipo'];
                   $datacit['citas']=$this->Bases_model->ViewCitas($tipo);
                   $this->load->view('calendario',$datacit);
                   $datacitas['pacientes']=$this->Bases_model->getPacientesNut($tipo);
                   $this->load->view('citas',$datacitas);
                    $this->load->view('nutri',$data);
                    $this->load->view('footer');

                  }else {
                      redirect('/Welcome/bienvenida/', 'refresh');
                  }
      }else {
        $busca=$_SESSION['paciente'];
        //$paciente = $this->Bases_model->getperson($busca);
         $paciente= $this->Bases_model->getpersonutri($busca);
        $actividad= $this->Bases_model->getActividad($busca);
        $dieta = $this->Bases_model->getDieta($busca);
        $frecuencia=$this->Bases_model->getFrecuencia($busca);
        $this->session->set_userdata('paciente',$busca);
        $this->session->set_userdata('check',TRUE);
        $data['pacientes']=$paciente;
        $data['actividad']=$actividad;
        $data['dieta']=$dieta;
        $data['frecuencia']=$frecuencia;
        $data1['da']=1;
        $data1['idp']=$busca;
        $this->load->view('head');
        $this->load->view('encabezado',$data1);
        $tipo=$_SESSION['tipo'];
        $datacit['citas']=$this->Bases_model->ViewCitas($tipo);
        $this->load->view('calendario',$datacit);
        $datacitas['pacientes']=$this->Bases_model->getPacientes();
        $this->load->view('citas',$datacitas);
         $this->load->view('nutri',$data);
         $this->load->view('footer');
      }
   }
   /*----------------Odontologia--------------------------
   -------------------------------------------
   -------------------------------------------*/
   public function odonto(){
         $data1['da']=1;
         $data1['idp']=0;
         $this->load->view('head');
         $this->load->view('encabezado',$data1);
          $this->load->view('odonto');
          $this->load->view('footer');
   }
   /*----------------Podologia--------------------------
   -------------------------------------------
   -------------------------------------------*/
   public function podo(){
     if($this->session->userdata('check')==FALSE){
     $busca=$this->input->post('pacientes');
     //$paciente = $this->Bases_model->getperson($busca);
     $paciente= $this->Bases_model->getpersonpodo($busca);
     $derecha= $this->Bases_model->getpieD($busca);
     $izquierda = $this->Bases_model->getpieI($busca);
               if($paciente!=FALSE){
                   $this->session->set_userdata('paciente',$busca);
                   $this->session->set_userdata('check',TRUE);
                   $data['pacientes']=$paciente;
                   $data['derecha']=$derecha;
                   $data['izquierda']=$izquierda;
                   $data1['da']=1;
                   $data1['idp']=$busca;
                   $this->load->view('head');
                   $this->load->view('encabezado',$data1);
                   $tipo=$_SESSION['tipo'];
                   $datacit['citas']=$this->Bases_model->ViewCitas($tipo);
                   $this->load->view('calendario',$datacit);
                   $datacitas['pacientes']=$this->Bases_model->getPacientesNut($tipo);
                   $this->load->view('citas',$datacitas);
                    $this->load->view('podo',$data);
                    $this->load->view('footer');

                  }else {
                      redirect('/Welcome/bienvenida/', 'refresh');
                  }
      }else {
        $busca=$_SESSION['paciente'];
        //$paciente = $this->Bases_model->getperson($busca);     $paciente= $this->Bases_model->getpersonpodo($busca);
        $paciente= $this->Bases_model->getpersonpodo($busca);
        $derecha= $this->Bases_model->getpieD($busca);
       $izquierda = $this->Bases_model->getpieI($busca);
        $this->session->set_userdata('paciente',$busca);
        $this->session->set_userdata('check',TRUE);
        $data['pacientes']=$paciente;
        $data['derecha']=$derecha;
        $data['izquierda']=$izquierda;
        $data1['da']=1;
        $data1['idp']=$busca;
        $this->load->view('head');
        $this->load->view('encabezado',$data1);
        $tipo=$_SESSION['tipo'];
        $datacit['citas']=$this->Bases_model->ViewCitas($tipo);
        $this->load->view('calendario',$datacit);
        $datacitas['pacientes']=$this->Bases_model->getPacientes();
        $this->load->view('citas',$datacitas);
         $this->load->view('podo',$data);
         $this->load->view('footer');
      }
   }

/*----------------Psicologia--------------------------
   -------------------------------------------
   -------------------------------------------*/
   public function psico(){
       if($this->session->userdata('check')==FALSE){
       $busca=$this->input->post('pacientes');
       //$paciente = $this->Bases_model->getperson($busca);
       $paciente= $this->Bases_model->getpersonpsico($busca);
                 if($paciente!=FALSE){
                     $this->session->set_userdata('paciente',$busca);
                     $this->session->set_userdata('check',TRUE);
                     $data['pacientes']=$paciente;
                     $data1['da']=1;
                     $data1['idp']=$busca;
                     $this->load->view('head');
                     $this->load->view('encabezado',$data1);
                     $tipo=$_SESSION['tipo'];
                     $datacit['citas']=$this->Bases_model->ViewCitas($tipo);
                     $this->load->view('calendario',$datacit);
                     $datacitas['pacientes']=$this->Bases_model->getPacientesNut($tipo);
                     $this->load->view('citas',$datacitas);
                      $this->load->view('psico',$data);
                      $this->load->view('footer');

                    }else {
                        redirect('/Welcome/bienvenida/', 'refresh');
                    }
        }else {
          $busca=$_SESSION['paciente'];
          //$paciente = $this->Bases_model->getperson($busca);     $paciente= $this->Bases_model->getpersonpodo($busca);
          $paciente= $this->Bases_model->getpersonpsico($busca);
          $this->session->set_userdata('paciente',$busca);
          $this->session->set_userdata('check',TRUE);
          $data['pacientes']=$paciente;
          $data1['da']=1;
          $data1['idp']=$busca;
          $this->load->view('head');
          $this->load->view('encabezado',$data1);
          $tipo=$_SESSION['tipo'];
          $datacit['citas']=$this->Bases_model->ViewCitas($tipo);
          $this->load->view('calendario',$datacit);
          $datacitas['pacientes']=$this->Bases_model->getPacientes();
          $this->load->view('citas',$datacitas);
           $this->load->view('psico',$data);
           $this->load->view('footer');
        }
     }



    public function registro(){
      $this->load->view('head');
      $data1['da']=0;
      $data1['idp']=0;
      $this->load->view('encabezado',$data1);
      $tipo=$_SESSION['tipo'];
      $datacit['citas']=$this->Bases_model->ViewCitas($tipo);
      $this->load->view('calendario',$datacit);
      $datacitas['pacientes']=$this->Bases_model->getPacientes();
      $this->load->view('citas',$datacitas);
      $this->load->view('registro');
      $this->load->view('footer');
    }

    public function insertarBD(){
                $per['nombre'] = $this->input->post('nombre');
                $per['fecha'] = $this->input->post('fecha');
                $per['edad'] = $this->input->post('edad');
                $per['genero'] = $this->input->post('genero');
                $per['nacimiento'] = $this->input->post('nacimiento');
                $per['residencia'] = $this->input->post('residencia');
                $per['domicilio'] = $this->input->post('domicilio');
                $per['ocupacion'] = $this->input->post('ocupacion');
                $per['estado'] = $this->input->post('estado');
                $per['escolaridad'] = $this->input->post('escolaridad');
                $per['religion'] = $this->input->post('religion');
                $per['telefono'] = $this->input->post('telefono');
                $per['email'] = $this->input->post('correo');

                $this->Bases_model-> insertarper($per);
                $data=$this->Bases_model->getid();
                foreach ($data->result() as $fila){
                  $id=$fila->idPersona;
               }
                  echo $id;

    }

    public function registropas($id=null){
      $data['idp']=$id;
      $this->load->view('head');
      $data1['da']=0;
      $data1['idp']=0;
      $this->load->view('encabezado',$data1);
      $this->load->view('registropas',$data);
      $this->load->view('footer');
    }

    public function insertarpasBD(){
                $pas['idp'] = $this->input->post('idp');
                $pas['Diagnostico'] = $this->input->post('diagnostico');
                $pas['Padecimiento'] = $this->input->post('padecimiento');
                $pas['Exploracion'] = $this->input->post('exploracion');
                $pas['Notas'] = $this->input->post('notas');
                $pas['Interconsultas'] = $this->input->post('interconsultas');
                $this->Bases_model->insertarpas($pas);
                //$data['idPersona']=$this->Bases_model->getid();
                $id=$pas['idp'];
                echo $id;

    }

    public function registroant($id=null){
      $data['idPersona']=$id;
      $data['idPaciente']=$this->Bases_model->getidpas();
        $this->load->view('head');
        $data1['da']=0;
        $data1['idp']=0;
        $this->load->view('encabezado',$data1);
        $this->load->view('registroant',$data);
        $this->load->view('footer');

    }

    public function insertarant(){
                //primero insersion
                $pat['idp'] =$this->input->post('idPersona');
                $pat['idpas'] =$this->input->post('idpas');
                $pat['Diabetes'] = $this->input->post('Diabetes');
                if (($pat['Diabetes'])==NULL){
                  $pat['Diabetes']='no';
                }
                $pat['Cardiopatia'] = $this->input->post('Cardiopatia');
                if (($pat['Cardiopatia'])==NULL){
                  $pat['Cardiopatia']='no';
                }
                $pat['Hipotiroidismo'] = $this->input->post('Hipotiroidismo');
                if (($pat['Hipotiroidismo'])==NULL){
                  $pat['Hipotiroidismo']='no';
                }
                $pat['Dislipidemia'] = $this->input->post('Dislipidemia');
                if (($pat['Dislipidemia'])==NULL){
                  $pat['Dislipidemia']='no';
                }
                $pat['Hepaticas'] = $this->input->post('Hepaticas');
                if (($pat['Hepaticas'])==NULL){
                  $pat['Hepaticas']='no';
                }
                $pat['Convulsion'] = $this->input->post('Convulsion');
                if (($pat['Convulsion'])==NULL){
                  $pat['Convulsion']='no';
                }
                $pat['Cancer'] = $this->input->post('Cancer');
                if (($pat['Cancer'])==NULL){
                  $pat['Cancer']='no';
                }
                $pat['Transfucionales'] = $this->input->post('Transfusionales');
                if (($pat['Transfucionales'])==NULL){
                  $pat['Transfucionales']='no';
                }
                $pat['Quirurgicos'] = $this->input->post('Quirurgicos');
                if (($pat['Quirurgicos'])==NULL){
                  $pat['Quirurgicos']='no';
                }
                $pat['Hipertension'] = $this->input->post('Hipertension');
                if (($pat['Hipertension'])==NULL){
                  $pat['Hipertension']='no';
                }
                $pat['Insuficiencia'] = $this->input->post('Insuficiencia');
                if (($pat['Insuficiencia'])==NULL){
                  $pat['Insuficiencia']='no';
                }
                $pat['Hipertiroidismo'] = $this->input->post('Hipertiroidismo');
                if (($pat['Hipertiroidismo'])==NULL){
                  $pat['Hipertiroidismo']='no';
                }
                $pat['Marcapasos'] = $this->input->post('Marcapasos');
                if (($pat['Marcapasos'])==NULL){
                  $pat['Marcapasos']='no';
                }
                $pat['Renales'] = $this->input->post('Renales');
                if (($pat['Renales'])==NULL){
                  $pat['Renales']='no';
                }
                $pat['Artritis'] = $this->input->post('Artritis');
                if (($pat['Artritis'])==NULL){
                  $pat['Artritis']='no';
                }
                $pat['Dentales'] = $this->input->post('Dentales');
                if (($pat['Dentales'])==NULL){
                  $pat['Dentales']='no';
                }
                $pat['Traumaticos'] = $this->input->post('Traumaticos');
                if (($pat['Traumaticos'])==NULL){
                  $pat['Traumaticos']='no';
                }
                $pat['Alergicos'] = $this->input->post('Alergico');
                if (($pat['Alergicos'])==NULL){
                  $pat['Alergicos']='no';
                }
                $this->Bases_model->insertarpat($pat);

                //segunda insersion
                $patno['idp'] = $pat['idp'];
                $patno['idpas'] = $pat['idpas'];
                $patno['Peso'] = $this->input->post('Peso');
                if (($patno['Peso'])==NULL){
                  $patno['Peso']='no';
                }
                $patno['Obesidad'] = $this->input->post('Obesidad');
                if (($patno['Obesidad'])==NULL){
                  $patno['Obesidad']='no';
                }
                $patno['Dieta'] = $this->input->post('Dieta');
                if (($patno['Dieta'])==NULL){
                  $patno['Dieta']='no';
                }
                $patno['Ejercicio'] = $this->input->post('Ejercicio');
                if (($patno['Ejercicio'])==NULL){
                  $patno['Ejercicio']='no';
                }
                $patno['Fumar'] = $this->input->post('Fumar');
                if (($patno['Fumar'])==NULL){
                  $patno['Fumar']='no';
                }
                $patno['Bebida'] = $this->input->post('Bebida');
                if (($patno['Bebida'])==NULL){
                  $patno['Bebida']='no';
                }
                $patno['Control'] = $this->input->post('Control');
                if (($patno['Control'])==NULL){
                  $patno['Control']='no';
                }
                $patno['Drogas'] = $this->input->post('Drogas');
                if (($patno['Drogas'])==NULL){
                  $patno['Drogas']='no';
                }
                $patno['Humo'] = $this->input->post('Humo');
                if (($patno['Humo'])==NULL){
                  $patno['Humo']='no';
                }
                $patno['Viaje'] = $this->input->post('Viaje');
                if (($patno['Viaje'])==NULL){
                  $patno['Viaje']='no';
                }
                $patno['Trabajos'] = $this->input->post('Trabajados');
                if (($patno['Trabajos'])==NULL){
                  $patno['Trabajos']='no';
                }
                $patno['Casa'] = $this->input->post('Casa');
                if (($patno['Casa'])==NULL){
                  $patno['Casa']='no';
                }
                $patno['Sangre'] = $this->input->post('Sangre');
                if (($patno['Sangre'])==NULL){
                  $patno['Sangre']='no';
                }
                $patno['Mascotas'] = $this->input->post('Mascotas');
                if (($patno['Mascotas'])==NULL){
                  $patno['Mascotas']='no';
                }
                $patno['Seguro'] = $this->input->post('Seguro');
                if (($patno['Seguro'])==NULL){
                  $patno['Seguro']='no';
                }
                $patno['Vacunacion'] = $this->input->post('Vacunacion');
                if (($patno['Vacunacion'])==NULL){
                  $patno['Vacunacion']='no';
                }
                $patno['Desparacitacion'] = $this->input->post('Desparacitacion');
                if (($patno['Desparacitacion'])==NULL){
                  $patno['Desparacitacion']='no';
                }
                $this->Bases_model->insertarpatno($patno);

                //tercera insersion
                $at['idp'] = $pat['idp'];
                $at['idpas'] = $pat['idpas'];
                $at['Familiar'] = $this->input->post('Parentesco');
                $at['Estado'] = $this->input->post('vivir');
                $at['Enfermedad'] = $this->input->post('Enfermedad');
                //echo $at['idp'].$at['idpas'];
                $this->Bases_model->insertarat($at);
                //-------------------------------------no-----
                /*echo "<script>
                       alert('Registro exitoso');
                       window.location.href='http://localhost/diabe/DiabeMedica/index.php/Welcome/bienvenida/';
                   </script>";
                   */
    }

  public function uploimg(){
    $idp=$_SESSION['paciente'];
    $nombre_img=$_FILES['imagen']['name'];
    $tipo=$_FILES['imagen']['type'];
    $tam=$_FILES['imagen']['size'];

    if (!empty($nombre_img)) {
      if (($_FILES['imagen']['type']=="image/gif")
        ||  ($_FILES['imagen']['type']=="image/jpeg")
        ||  ($_FILES['imagen']['type']=="image/jpg")
        ||  ($_FILES['imagen']['type']=="image/png")
        ) {
          //--------------------------verifica si es un archivo de imagen
          //---------------------------------------------------------------------------------------------------------------
       $busca=$this->Bases_model->getidpaci($idp);
       foreach ($busca->result() as $value) {
         $idpa=$value->idPaciente;
       }
       $carpeta = $_SERVER['DOCUMENT_ROOT'].'/diabeproto/static/analisis/'.$idp.'/';
          if (!file_exists($carpeta)) {
              mkdir($carpeta, 0777, true);
          }
       // Nombre Temporal del Archivo
       $dir="static/analisis/".$idp."/";
       $directorio = $_SERVER['DOCUMENT_ROOT'].'/diabeproto/static/analisis/'.$idp.'/';
       move_uploaded_file($_FILES['imagen']['tmp_name'],$directorio.$nombre_img);
       $data= array('dir'=>$dir.$nombre_img,'id'=>$idp,'idp'=>$idpa);
       $this->Bases_model->Uploimg($data);
       redirect('/Welcome/buscadorv2/', 'refresh');
     }else
      if(($_FILES['imagen']['type']=="application/pdf") ||  ($_FILES['imagen']['type']=="application/msword")){
        //--------------------------verifica si es un archivo $pdf
        //---------------------------------------------------------------------------------------------------------------
       $busca=$this->Bases_model->getidpaci($idp);
       foreach ($busca->result() as $value) {
         $idpa=$value->idPaciente;
       }
       $carpeta = $_SERVER['DOCUMENT_ROOT'].'/diabeproto/static/analisis/'.$idp.'/';
          if (!file_exists($carpeta)) {
              mkdir($carpeta, 0777, true);
          }
       // Nombre Temporal del Archivo
       $dir="static/analisis/".$idp."/";
       $directorio = $_SERVER['DOCUMENT_ROOT'].'/diabeproto/static/analisis/'.$idp.'/';
       move_uploaded_file($_FILES['imagen']['tmp_name'],$directorio.$nombre_img);
       $data= array('dir'=>$dir.$nombre_img,'id'=>$idp,'idp'=>$idpa);
       $this->Bases_model->Uplopdf($data);
       redirect('/Welcome/buscadorv2/', 'refresh');
     }else{
       echo "<script>
              alert('el archivo es invalido');
              window.location.href='http://localhost/diabeproto/index.php/Welcome/buscadorv2/';
          </script>";}
    }else{
      echo "<script>
             alert('no se eligio el archivo');
             window.location.href='http://localhost/diabeproto/index.php/Welcome/buscadorv2/';
         </script>";
    }
  }

      public function agregarcitas(){
        $nombre=$this->input->post('citapac');
        $fecha=$this->input->post('fechat');
        $doc=$_SESSION['tipo'];
        $data = array(
                        'nombre'=>$nombre,'fecha'=>$fecha,
                        'doc' => $doc);
       $this->Bases_model->SetCitas($data);
       $busca=$this->Bases_model->getidcita($doc);
       foreach ($busca->result() as $value) {
         $id=$value->idCalendario;
       }
      echo $id;
      }

      public function deletecita(){
        $id=$this->input->post('idde');
          $this->Bases_model->deletecitaBD($id);
      }


      //----------seccion de grafica--------------------------------
      /*
     public function grafica(){
       $id=$this->session->userdata('paciente');

       $idp=$this->Bases_model->getidpaci($id);
       foreach ($idp->result() as $fila){
         $idpa=$fila->idPaciente;
         }
       $dat = array(
       'pac'=>$idpa,'per'=>$id);

       //var_dump($dat);
       $data['graficas']=$this->Bases_model->getDatGraf($dat);
       $data['person']=$id;
       $data['pacien']=$idpa;
       $data1['da']=3;
       //$dd['opp']=0;
       $this->load->view('head');
       $this->load->view('encabezado',$data1);
       $this->load->view('datosgraf',$data);
       //$this->load->view('canvas',$dd);
       $this->load->view('footer');
     }
*/

     public function agregardatosgra(){
       $data = array(
                'glu'=>$this->input->post('glu'),
                'fecha'=>$this->input->post('fecha'),
                'idpaci'=> $this->input->post('idpaci'),
                'idp'=> $this->input->post('idp'));
                $this->Bases_model->AgDatgraf($data);
                $idd=$this->Bases_model->ObtidGr();
                foreach ($idd->result() as $fila){
                  $id=$fila->idGraf;
               }
              echo $id;

     }

     public function editarDatgraf(){
       $data = array(
                'idm'=>$this->input->post('idm'),
                'glu'=>$this->input->post('glu'),
                'fecha'=>$this->input->post('fecha'),
                'idpaci'=> $this->input->post('idpaci'),
                'idp'=> $this->input->post('idp'));
                $this->Bases_model->EdDatgraf($data);

     }

     public function deletedatgraf(){
       $delete=$this->input->post('idde');
       $this->Bases_model->deletedatosG($delete);
     }

     public function grafica(){
       $id=$this->session->userdata('paciente');

       $idp=$this->Bases_model->getidpaci($id);
       foreach ($idp->result() as $fila){
         $idpa=$fila->idPaciente;
         }
       $dat = array(
       'pac'=>$idpa,'per'=>$id);

       //var_dump($dat);
       $dd['tipo']=$this->input->post('tipo');
       $data['graficas']=$this->Bases_model->getDatGraf($dat);
       $data['person']=$id;
       $data['pacien']=$idpa;
       $data1['da']=3;
       $dd['graf']=$data['graficas'];
       $this->load->view('head');
       $this->load->view('encabezado',$data1);
       $tipo=$_SESSION['tipo'];
       $datacit['citas']=$this->Bases_model->ViewCitas($tipo);
       $this->load->view('calendario',$datacit);
       $datacitas['pacientes']=$this->Bases_model->getPacientes();
       $this->load->view('citas',$datacitas);
       $this->load->view('datosgraf',$data);
       $this->load->view('graficas',$dd);
       $this->load->view('footer');
     }

    public function iframe($opp=0){
      $data['tipo']=$opp;
      $this->load->view('head');
       $this->load->view('graficas',$data);
    }



     public function iframe33(){
       $this->load->view('head');
       $this->load->view('graficas')
       ;
     }
 //------------------seccion de graficas



//foto--------------------------------------------

public function foto(){
  $idp=$_SESSION['paciente'];
  $nombre_img=$_FILES['imagen']['name'];
  $tipo=$_FILES['imagen']['type'];
  $tam=$_FILES['imagen']['size'];

  if (!empty($nombre_img)) {
    if (($_FILES['imagen']['type']=="image/gif")
      ||  ($_FILES['imagen']['type']=="image/jpeg")
      ||  ($_FILES['imagen']['type']=="image/jpg")
      ||  ($_FILES['imagen']['type']=="image/png")
      ) {
        //--------------------------verifica si es un archivo de imagen
        //---------------------------------------------------------------------------------------------------------------
     $busca=$this->Bases_model->getidpaci($idp);
     foreach ($busca->result() as $value) {
       $idpa=$value->idPaciente;
     }
     $carpeta = $_SERVER['DOCUMENT_ROOT'].'/diabeproto/static/foto/'.$idp.'/';
        if (!file_exists($carpeta)) {
            mkdir($carpeta, 0777, true);
        }
     // Nombre Temporal del Archivo
     $dir="static/foto/".$idp."/";
     $directorio = $_SERVER['DOCUMENT_ROOT'].'/diabeproto/static/foto/'.$idp.'/';
     move_uploaded_file($_FILES['imagen']['tmp_name'],$directorio.$nombre_img);
     $data= array('dir'=>$dir.$nombre_img,'id'=>$idp,'idp'=>$idpa);
     $this->Bases_model->Uplofoto($data);
     redirect('/Welcome/buscadorv2/', 'refresh');
   }else{
     echo "<script>
            alert('el archivo es invalido');
            window.location.href='http://localhost/diabeproto/index.php/Welcome/buscadorv2/';
        </script>";}
  }else{
    echo "<script>
           alert('no se eligio el archivo');
           window.location.href='http://localhost/diabeproto/index.php/Welcome/buscadorv2/';
       </script>";
  }
}

//Listar pacientes

//Opciones de insertas mas especialidades
public function agregar($id=null){
  $data['idPersona']=$id;
  $data['idPaciente']=$this->Bases_model->getidpas();
    $this->load->view('head');
    $data1['da']=0;
    $data1['idp']=0;
  $this->load->view('head');
  $this->load->view('encabezado',$data1);
  $this->load->view('asignar',$data);
  $this->load->view('footer');
}


//Registro nutricion
public function registronutri($id=null){
  $data['idPersona']=$this->Bases_model->getid();
  $data['idPaciente']=$this->Bases_model->getidpas();
    $this->load->view('head');
    $data1['da']=0;
    $data1['idp']=0;
    $this->load->view('encabezado',$data1);
  $this->load->view('registronutri',$data);
  $this->load->view('footer');
}

public function registronutri2($id=null){
  $data['idNutricion']=$this->Bases_model->getidnu();
  $this->load->view('head');
  $data1['da']=0;
  $data1['idp']=0;
  $this->load->view('encabezado',$data1);
  $this->load->view('registronutri2',$data);
  $this->load->view('footer');
}

public function insertarnutri(){
     //primera insersion
      $nutri['idp'] =$this->input->post('idPersona');
      $nutri['idpas'] =$this->input->post('idpas');
      $nutri['Diagnostico'] = $this->input->post('diagnostico');
      $nutri['Peso'] = $this->input->post('peso');
      $nutri['Cintura'] = $this->input->post('cintura');
      $nutri['Cadera'] = $this->input->post('cadera');
      $nutri['Notas'] = $this->input->post('notas');
      $this->Bases_model->insertarnutri($nutri);
         echo "<script>
                alert('Paciente Asignado a nutricion');
                window.location.href='http://localhost/diabeproto/index.php/Welcome/registronutri2';
            </script>";
}

public function insertarnutri2(){

        //**segunda insersion
          $acti['idNutricion'] =$this->input->post('idNutricion');
          $acti['Tipo'] = $this->input->post('tipo');
          $acti['Frecuencia'] = $this->input->post('frecuencia');
          $this->Bases_model->insertaracti($acti);

        //tercera insersion
          $dieta['idNutricion'] =$this->input->post('idNutricion');
          $dieta['Desayuno'] = $this->input->post('desayuno');
          $dieta['Colacion1'] = $this->input->post('colacion1');
          $dieta['Comida'] = $this->input->post('comida');
          $dieta['Colacion2'] = $this->input->post('colacion2');
          $dieta['Cena'] = $this->input->post('cena');
          $this->Bases_model->insertardieta($dieta);

          //Cuarta insersion
           $frecc['idNutricion'] =$this->input->post('idNutricion');
           $frecc['Frutas'] = $this->input->post('frutas');
           $frecc['Verduras'] = $this->input->post('verduras');
           $frecc['Cereales'] = $this->input->post('cereales');
           $frecc['AOA'] = $this->input->post('aoa');
           $frecc['Lacteos'] = $this->input->post('lacteos');
           $frecc['Aceites'] = $this->input->post('aceites');
           $frecc['Azucares'] = $this->input->post('azucares');
           $frecc['Postres'] = $this->input->post('postres');
           $frecc['Jugos'] = $this->input->post('jugos');
           $frecc['AguaN'] = $this->input->post('aguan');
           $frecc['AguaS'] = $this->input->post('aguas');
           $frecc['Cafe'] = $this->input->post('cafe');
           $frecc['Te'] = $this->input->post('te');
           $frecc['Otros'] = $this->input->post('otros');
           $frecc['Alcohol'] = $this->input->post('alcohol');
          // $frecc[''] = $this->input->post('');
           $this->Bases_model->insertarfrec($frecc);
           echo "<script>
                  alert('Paciente Asignado a nutricion');
                  window.location.href='http://localhost/diabeproto/index.php/Welcome/agregar';
              </script>";
}

//Registro podologia
public function registropodo($id=null){
  $data['idPersona']=$this->Bases_model->getid();;
  $data['idPaciente']=$this->Bases_model->getidpas();
    $this->load->view('head');
    $data1['da']=0;
    $data1['idp']=0;
    $this->load->view('encabezado',$data1);
  $this->load->view('registropodo',$data);
  $this->load->view('footer');
}

public function registropodo2($id=null){
  $data['idPodologia']=$this->Bases_model->getidpo();
    $this->load->view('head');
    $data1['da']=0;
    $data1['idp']=0;
    $this->load->view('encabezado',$data1);
  $this->load->view('registropodo2',$data);
  $this->load->view('footer');
}


public function insertarpodo(){
  //podologia
  $podo['idp'] =$this->input->post('idPersona');
  $podo['idpas'] =$this->input->post('idpas');
  $podo['Motivo'] = $this->input->post('motivo');
  $podo['Diagnostico'] = $this->input->post('diagnostico');
  $podo['Observaciones'] = $this->input->post('observaciones');
  $podo['Tratamiento'] = $this->input->post('tratamiento');
  $podo['Recomendaciones'] = $this->input->post('recomendaciones');
  $podo['Notas'] = $this->input->post('notas');
  $this->Bases_model->insertarpodo($podo);
  echo "<script>
         alert('Paciente Asignado a Podologia');
         window.location.href='http://localhost/diabeproto/index.php/Welcome/registropodo2';
     </script>";
}

public function insertarpodo2(){

    //pie_der
    $pied['idPodologia'] =$this->input->post('idPodologia');
    $pied['Plantar'] = $this->input->post('plantar');
    $pied['Dorsal'] = $this->input->post('dorsal');
    $pied['Talar'] = $this->input->post('talar');
    $pied['Onicrocriptosis'] = $this->input->post('onicrocriptosis');
    $pied['Onicomicosis'] = $this->input->post('onicomicosis');
    $pied['Onicograifosis'] = $this->input->post('onicograifosis');
    $pied['Bullosis'] = $this->input->post('bullosis');
    $pied['Ulceras'] = $this->input->post('ulceras');
    $pied['Necrosis'] = $this->input->post('necrosis');
    $pied['Grietas'] = $this->input->post('grietas');
    $pied['Lesion'] = $this->input->post('lesion');
    $pied['Anhidrosis'] = $this->input->post('anhidrosis');
    $pied['Tiñas'] = $this->input->post('tiñas');
    $pied['Infecciones'] = $this->input->post('infecciones');
    $pied['Dedosmar'] = $this->input->post('dedosmar');
    $pied['Hallux'] = $this->input->post('hallux');
    $pied['Infraductos'] = $this->input->post('infraductos');
    $pied['Supraductos'] = $this->input->post('supraductos');
    $pied['Hipercargas'] = $this->input->post('hipercargas');
    $pied['Pulsope'] = $this->input->post('pulsope');
    $pied['Pulsoti'] = $this->input->post('pulsoti');
    $pied['Llenado'] = $this->input->post('llenado');
    $pied['Edena'] = $this->input->post('edena');
    $pied['Sensacion'] = $this->input->post('sensacion');
    $this->Bases_model->insertarpied($pied);

    //pie_izq
    $piei['idPodologia'] =$this->input->post('idPodologia');
    $piei['Plantar'] = $this->input->post('plantari');
    $piei['Dorsal'] = $this->input->post('dorsali');
    $piei['Talar'] = $this->input->post('talari');
    $piei['Onicrocriptosis'] = $this->input->post('onicrocriptosisi');
    $piei['Onicomicosis'] = $this->input->post('onicomicosisi');
    $piei['Onicograifosis'] = $this->input->post('onicograifosisi');
    $piei['Bullosis'] = $this->input->post('bullosisi');
    $piei['Ulceras'] = $this->input->post('ulcerasi');
    $piei['Necrosis'] = $this->input->post('necrosisi');
    $piei['Grietas'] = $this->input->post('grietasi');
    $piei['Lesion'] = $this->input->post('lesioni');
    $piei['Anhidrosis'] = $this->input->post('anhidrosisi');
    $piei['Tiñas'] = $this->input->post('tiñasi');
    $piei['Infecciones'] = $this->input->post('infeccionesi');
    $piei['Dedosmar'] = $this->input->post('dedosmari');
    $piei['Hallux'] = $this->input->post('halluxi');
    $piei['Infraductos'] = $this->input->post('infraductosi');
    $piei['Supraductos'] = $this->input->post('supraductosi');
    $piei['Hipercargas'] = $this->input->post('hipercargasi');
    $piei['Pulsope'] = $this->input->post('pulsopei');
    $piei['Pulsoti'] = $this->input->post('pulsotii');
    $piei['Llenado'] = $this->input->post('llenadoi');
    $piei['Edena'] = $this->input->post('edenai');
    $piei['Sensacion'] = $this->input->post('sensacioni');
    $this->Bases_model->insertarpiei($piei);
    echo "<script>
           alert('Paciente Asignado a Podologia');
           window.location.href='http://localhost/diabeproto/index.php/Welcome/agregar';
       </script>";
}

//Registro psicologia
public function registropsico($id=null){
  $data['idPersona']=$this->Bases_model->getid();;
  $data['idPaciente']=$this->Bases_model->getidpas();
    $this->load->view('head');
    $data1['da']=0;
    $data1['idp']=0;
    $this->load->view('encabezado',$data1);
  $this->load->view('registropsico',$data);
  $this->load->view('footer');
}

public function insertarpsico(){
  $psi['idp'] =$this->input->post('idPersona');
  $psi['idpas'] =$this->input->post('idpas');
  $psi['Notas'] = $this->input->post('notas');
  $this->Bases_model->insertarpsi($psi);
  echo "<script>
         alert('Paciente Asignado a Psicologia');
         window.location.href='http://localhost/diabeproto/index.php/Welcome/agregar';
     </script>";
}

public function listado(){
  $this->session->set_userdata('check', FALSE);
  $this->load->view('head');
  $data1['da']=2;
  $this->load->view('encabezado',$data1);
  $lis['lis']=$this->Bases_model->listado();
  $this->load->view('listado',$lis);
    $this->load->view('footer');
}

}
