// seccion de waves
Waves.init();
Waves.attach('.wvs');

//seccion de select2
$(document).ready(function() {
  $('#pacientes').select2({
    placeholder: 'Escriba el nombre del paciente',
    language: "es",
    theme: "classic",
    width: 'resolve'});
});
$(document).ready(function() {
$('#pacientescal').select2({
  placeholder: 'Escriba el nombre del paciente',
  language: "es",
  theme: "classic",
  width: 'resolve'});
});
$(document).ready(function() {
$('#pacientescali').select2({
  placeholder: 'Escriba el nombre del paciente',
  language: "es",
  theme: "classic",
  width: 'resolve'});
});
//calendario y otras funciones de extras
$('#inputcalendar').datetimepicker({format: 'HH:MM yyyy/mm/dd', footer: true, modal: true, uiLibrary: 'bootstrap4', iconsLibrary: 'fontawesome', size: 'small' });

//funcion para editar el texto
$(document).ready(function () {
    $("#editor1").editor({
 uiLibrary: 'bootstrap4',
 iconsLibrary: 'fontawesome'
});
});

$(document).ready(function () {
    $("#editor2").editor({
 uiLibrary: 'bootstrap4',
 iconsLibrary: 'fontawesome'
});
});

//----------------------------funcion de animaciones----------------------------------
function anim(el){
  //alert('hola');
  var animationName ='animated shake fast';
  var animationend= 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oAnimationEnd animationend';
  $(el).addClass(animationName).one(animationend,function(){
    $(this).removeClass(animationName);
  });
}

//--------------sidebar-----------------
$(document).ready(function () {
    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });
    //funcion para el boton con la flecha para cerrar el sidebar
    $('#dismiss, .overlay').on('click', function () {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
        document.getElementById("sib").value = "0";
    });
    //funcion para mostrar y quitar el sidebar desde el icono
    $('#sidebarCollapse').on('click', function () {
      var sib= $("input[name='sib']").val();
      //si el valor es igual a "0" muestra el sidebar si es igual a "1" lo oculta
      if(sib==0){
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        document.getElementById("sib").value = "1";
      }else {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
        document.getElementById("sib").value = "0";
      }

    });
});
//-----funcion para los tipo de medicos____________________
function marktipo(ele){
  var ch=ele.checked;
  var idpaci = $("input[name='idpaci']").val();
  var idp = $("input[name='idp']").val();
    if(ch){
      //alert(ch);
      var id=ele.value;
      $.ajax({
        url:'http://localhost/diabeproto/Welcome/agregatipo',
        type:'POST',
        data:{id:id, idp:idp, idpaci:idpaci},
        error: function() {
           alert('hubo algun error');
        },
        success: function(data){
            //document.getElementById("podo").value="2";
            alert("agregado "+data);
        }
      });
      }else {
        //alert("nn");
        var id=ele.value;
        $.ajax({
          url:'http://localhost/diabeproto/Welcome/quitartipo',
          type:'POST',
          data:{id:id, idp:idp, idpaci:idpaci},
          error: function() {
             alert('hubo algun error');
          },
          success: function(data){
              //document.getElementById("podo").value=id;
              alert("quitado "+data);
          }
        });
      }
}

//-------------modal------------------
function onClick(element) {
  document.getElementById("img01").data = element.data;
  document.getElementById("modal01").style.display = "block";
}

function verpdf(element) {
  //var v= element.value;
  document.getElementById("pdf01").src = element.value;
  //alert(v);
  document.getElementById("modal02").style.display = "block";
}




//estas funciones sirven para activar el tooltip para luego usarlo----------------------
  $('#nombremed').tooltip('hide');
  $('#dosismed').tooltip('hide');
  $('#horariomed').tooltip('hide');

  //------------funciones para agregar editar y borrar datos------------

     //-----------agregar
        $('#agregame').click(function(){
          document.getElementById("cer").style.display = "inline-block";
          document.getElementById("agre").style.display = "block";
        });
        //cerrar formulario
        $('#cer').click(function(){
          document.getElementById("nombremed").value = "";
          document.getElementById("dosismed").value = "";
          document.getElementById("horariomed").value = "";
          document.getElementById("Observacionesmed").value = "";
          document.getElementById("agre").style.display = "none";
            document.getElementById("cer").style.display = "none";
        });


        $('#guardarme').click(function(){
          //se obtiene los valores requeridos
           var nombre = $("input[name='nombremed']").val();
           var dosis = $("input[name='dosismed']").val();
           var horario = $("input[name='horariomed']").val();
           var observa = $("textarea[name='Observacionesmed']").val();
           var idpaci = $("input[name='idpaci']").val();
           var idp = $("input[name='idp']").val();
           var im = $("input[name='im']").val();
           //var idmed = $("input[name='ultimoM']").val();;

           //alert(im);
           // verificamos que los campos no esten vacios (para ciertos campos: nombre, dosis, horario)
           //se actvara el tooltip y se marcara en rojo la casilla erronea
           //con style.backgroundColor cambiamos el color del campo y title es para el texto que mostrara el tooltip
           //opction de tooltip (enable) para activar, (show) para mostrar, (dispose) para desactivar-borrar
           //------------nombre --------------------------------------
           if(new String(nombre).valueOf() == new String("").valueOf()){
             document.getElementById("nombremed").style.backgroundColor = "red";
             document.getElementById("nombremed").style.color = "white";
             document.getElementById("nombremed").title="campo vacio";
             $('#nombremed').tooltip('enable');
             $('#nombremed').tooltip('show');
           }else{

             document.getElementById("nombremed").style.backgroundColor = "white";
             document.getElementById("nombremed").style.color = "black";
              document.getElementById("nombremed").title="";
              $('#nombremed').tooltip('dispose');
           }
           //---------dosis--------------------------------
           if(new String(dosis).valueOf() == new String("").valueOf()){
             document.getElementById("dosismed").style.backgroundColor = "red";
             document.getElementById("dosismed").style.color = "white";
             document.getElementById("dosismed").title="campo vacio";
             $('#dosismed').tooltip('enable');
             $('#dosismed').tooltip('show');
           }else{
             document.getElementById("dosismed").style.backgroundColor = "white";
             document.getElementById("dosismed").style.color = "black";
              document.getElementById("dosismed").title="";
              $('#dosismed').tooltip('dispose');
           }
           //--------horario--------------------------------------------
           if(new String(horario).valueOf() == new String("").valueOf()){
             document.getElementById("horariomed").style.backgroundColor = "red";
             document.getElementById("horariomed").style.color = "white";
             document.getElementById("horariomed").title="campo vacio";
             $('#horariomed').tooltip('enable');
             $('#horariomed').tooltip('show');
           }else{
             document.getElementById("horariomed").style.backgroundColor = "white";
             document.getElementById("horariomed").style.color = "black";
              document.getElementById("horariomed").title="";
              $('#horariomed').tooltip('dispose');
           }
           //verifica que los 3 campos esten llenos y luego ejecuta el ajax
           if((new String(nombre).valueOf() != new String("").valueOf()) && (new String(dosis).valueOf() != new String("").valueOf()) &&
           (new String(horario).valueOf() != new String("").valueOf())){
            $.ajax({
               url: 'http://localhost/diabeproto/Welcome/agregarmedica',
               type: 'POST',
               data: { nombre: nombre, dosis: dosis, horario: horario, observa: observa, idpaci: idpaci, idp: idp, im: im},
               error: function() {
                  alert('hubo algun error');
               },
               success: function(data) {
                 //una vez insertado el Medicamento creamos varias variables para generar un codigo html que se desplegara
                 //justo despues de un mensaje de aviso y asi mostrara el nuevo Medicamento
                 var td1=++im;
                 var td2=++im;
                 var td3=++im;
                 var td4=++im;
                 var td5=++im;
                 var idmedM=data;
                 var con="me"+idmedM;
                 //alert(td1+","+td2+","+td3+","+td4+","+td5+","+idmedM);
                 var pri="<th scope=\"row\" ><input type=\"texto\" name=\""+td1+"\" id=\""+td1+"\" size=\"13\" value=\""+nombre+"\" disabled></th>";
                 var seg="<td><input type=\"texto\" name=\""+td2+"\" id=\""+td2+"\" size=\"13\" value=\""+dosis+"\" disabled></td>";
                 var ter="<td><input type=\"texto\" name=\""+td3+"\" id=\""+td3+"\" size=\"13\" value=\""+horario+"\" disabled></td>";
                 var cua="<td><textarea type=\"texto\" name=\""+td4+"\" id=\""+td4+"\" cols=\"13\" value=\""+observa+"\" disabled>"+observa+"</textarea></td>";
                 var but12="<td style=\"border:0;\"><button id=\"i"+td5+"\" value=\""+td5+"\" onClick=\"edit(this)\"><i class=\"far fa-edit fa-2x blue\"></i></button> <button id=\"j"+td5+"\" value=\""+idmedM+"\" onClick=\"saveEdit(this)\" style=\"display:none\"><i class=\"fas fa-save fa-2x blue\"></i></button> </td>";
                 var but3="<td style=\"border:0;\"><button id=\"z"+td5+"\" value=\""+idmedM+"\" onClick=\"alerta(this)\"><i class=\"fas fa-trash fa-2x blue\"></i></button></td>";
                 $(".bodymedica").append("<tr id=\""+con+"\" >"+pri+seg+ter+cua+but12+but3+"</tr>");
                 var imN=td5;
                 document.getElementById("im").value=imN;
                 document.getElementById("ultimoM").value=idmedM;
           alert("Medicamento registrado");

               }
            });
          }
        });


  //-----------editar
        function edit(element){
          //se toma el valor del boton((nn es el total) se le restan cuatro por los campos dados) y usamos el id para quitarlo despues
          var nn=element.getAttribute("value");
          var ide=element.getAttribute("id");
          var ii=nn-4;
          //alert(nn+ii);
          //se concatena para obtener el id especifico y luego se desplegamos un boton y escondemos el otro
          var j="j";
          var jj=j.concat(nn);
          document.getElementById(ide).style.display="none";
          document.getElementById(jj).style.display="block";
          document.getElementById("eddi").value=nn;
          //se hace un ciclo para activar los input
          for (var i = ii; i <= nn; i++) {
            document.getElementById(i).disabled = false;
            document.getElementById(i).size = "13";
            document.getElementById(i).col = "13";
          }
        }

        function saveEdit(element){
          //se toma el valor del boton((nn es el total) se le restan cuatro por los campos dados) y id
      var nn=document.getElementById("eddi").value;
      var ide=element.getAttribute("id");
      var idm=element.getAttribute("value");
      var idpaci = $("input[name='idpaci']").val();
      var idp = $("input[name='idp']").val();
      var ii=nn-4;
      var kk=ii;
      //alert(kk);
      //se concatena para obtener el id especifico y luego se desplegamos un boton y escondemos el otro
  var j="i";
  var jj=j.concat(nn);
  //se hace un ciclo para activar los input
      var uno=ii;
      var dos=++ii;
      var tres=++ii;
      var cuatro=++ii;
      var nombre=document.getElementById(uno).value;
      var dosis=document.getElementById(dos).value;
      var horario=document.getElementById(tres).value;
      var observa=document.getElementById(cuatro).value;
      //alert(nombre+dosis+horario+observa);
      // verificamos que los campos no esten vacios (para ciertos campos: nombre, dosis, horario)
      //se actvara el tooltip y se marcara en rojo la casilla erronea
      //con style.backgroundColor cambiamos el color del campo y title es para el texto que mostrara el tooltip
      //opction de tooltip (enable) para activar, (show) para mostrar, (dispose) para desactivar-borrar
      //------------nombre --------------------------------------
      if(new String(nombre).valueOf() == new String("").valueOf()){
        document.getElementById(uno).style.backgroundColor = "red";
        document.getElementById(uno).style.color = "white";
        document.getElementById(uno).title="campo vacio";
        $("#"+uno).tooltip('enable');
        $("#"+uno).tooltip('show');
      }else{

        document.getElementById(uno).style.backgroundColor = "white";
        document.getElementById(uno).style.color = "black";
         document.getElementById(uno).title="";
         $("#"+uno).tooltip('dispose');
      }
      //---------dosis--------------------------------
      if(new String(dosis).valueOf() == new String("").valueOf()){
        document.getElementById(dos).style.backgroundColor = "red";
        document.getElementById(dos).style.color = "white";
        document.getElementById(dos).title="campo vacio";
        $("#"+dos).tooltip('enable');
        $("#"+dos).tooltip('show');
      }else{
        document.getElementById(dos).style.backgroundColor = "white";
        document.getElementById(dos).style.color = "black";
         document.getElementById(dos).title="";
         $("#"+dos).tooltip('dispose');
      }
      //--------horario--------------------------------------------
      if(new String(horario).valueOf() == new String("").valueOf()){
        document.getElementById(tres).style.backgroundColor = "red";
        document.getElementById(tres).style.color = "white";
        document.getElementById(tres).title="campo vacio";
        $("#"+tres).tooltip('enable');
        $("#"+tres).tooltip('show');
      }else{
        document.getElementById(tres).style.backgroundColor = "white";
        document.getElementById(tres).style.color = "black";
         document.getElementById(tres).title="";
         $("#"+tres).tooltip('dispose');
      }
      //verifica que los 3 campos esten llenos y luego ejecuta el ajax
            if((new String(nombre).valueOf() != new String("").valueOf()) && (new String(dosis).valueOf() != new String("").valueOf()) &&
            (new String(horario).valueOf() != new String("").valueOf())){
        $.ajax({
             url: 'http://localhost/diabeproto/Welcome/editarMedica',
             type: 'POST',
             data: {idm: idm, nombre: nombre, dosis: dosis, horario: horario, observa: observa, idpaci: idpaci, idp: idp, ide: ide, jj: jj, kk: kk, nn: nn},
             error: function() {
                alert('hubo algun error');
             },
             success: function(data) {
                 alert("Medicamento editado");
               document.getElementById(ide).style.display="none";
               document.getElementById(jj).style.display="block";
               for (var p = kk; p <= nn; p++) {
                 document.getElementById(p).disabled = true;
                 document.getElementById(p).size = "13";
                 document.getElementById(i).col = "13";
               }

             }
        });
      }

        }



  //----------------borrar
  function alerta(elemento)
      {
      //se tomo el valor del boton y se guardo en input
      var val=elemento.getAttribute("value");
      document.getElementById("check").value=val;
      var opcion = confirm("Seguro que quiere eliminar");
      //se pide confirmacion y si es aceptado toma el valor del input y se ejecuta el ajax
      if (opcion == true) {
        var idde=document.getElementById("check").value;
        $.ajax({
           url: 'http://localhost/diabeproto/Welcome/delete',
           type: 'POST',
           data: {idde: idde},
           error: function() {
              alert('hubo algun error');
           },
           success: function(data) {
             var con="me"+idde;
             //funcion para remover elementos
             $("#"+con).remove();
             //document.getElementById(con).style.display="none";
                alert("Medicamento eliminado");
           }
        });
  	}
  }

//-------notas--------------
//se toman los valores de los input y se mandan por el ajax
    $('#nota-button').click(function(){
       var idp = $("input[name='idp']").val();
       var notas = $("textarea[name='notas']").val();
        $.ajax({
           url: 'http://localhost/diabeproto/Welcome/notasv2',
           type: 'POST',
           data: {idp: idp, notas: notas},
           error: function() {
              alert('hubo algun error');
           },
           success: function(data) {
                alert("Nota guardada");
           }
        });
    });
//---interconsultas-----------------
//se toman los valores de los input y se mandan por el ajax
    $('#inter-button').click(function(){
       var idp = $("input[name='idp']").val();
       var interconsultas = $("textarea[name='interconsultas']").val();
        $.ajax({
           url: 'http://localhost/diabeproto/Welcome/interv2',
           type: 'POST',
           data: {idp: idp, interconsultas: interconsultas},
           error: function() {
              alert('hubo algun error');
           },
           success: function(data) {
                alert("Interconsulta guardada");
           }
        });
    });
    //<!--funciones para grafica-->
  //  <!--funciones para agregar editar y borrar datos-->

  //-----------agregar


     $('#chartse').click(function(){
       //se obtiene los valores requeridos
        var glu = $("input[name='glu1']").val();
        var fecha = $("input[name='dia1']").val();
        var idpaci = $("input[name='idpacig']").val();
        var idp = $("input[name='idpg']").val();
        var im = $("input[name='img']").val();
        //var idmed = $("input[name='ultimoM']").val();;

        //alert(glu+" "+fecha+" "+idpaci+" "+idp+" "+im);
        // verificamos que los campos no esten vacios (para ciertos campos: nombre, dosis, horario)
        //se actvara el tooltip y se marcara en rojo la casilla erronea
        //con style.backgroundColor cambiamos el color del campo y title es para el texto que mostrara el tooltip
        //opction de tooltip (enable) para activar, (show) para mostrar, (dispose) para desactivar-borrar

        //verifica que los 3 campos esten llenos y luego ejecuta el ajax

         $.ajax({
            url: 'http://localhost/diabeproto/Welcome/agregardatosgra',
            type: 'POST',
            data: { glu: glu, fecha: fecha, idpaci: idpaci, idp: idp, im: im},
            error: function() {
               alert('hubo algun error');
            },
            success: function(data) {
              //una vez insertado el Medicamento creamos varias variables para generar un codigo html que se desplegara
              //justo despues de un mensaje de aviso y asi mostrara el nuevo Medicamento
              var td1=++im;
              var td2=++im;
              var td5=++im;
              var idmedM=data;
              var con="graf"+idmedM;
              //alert(td1+","+td2+","+td3+","+td4+","+td5+","+idmedM);
              var pri="<td><input type=\"texto\" name=\""+td1+"\" id=\""+td1+"\" value=\""+glu+"\" disabled></td>";
              var seg="<td><input type=\"texto\" name=\""+td2+"\" id=\""+td2+"\" value=\""+fecha+"\" disabled></td>";
              var but12="<td style=\"border:0;\"><button class=\"btt-graf-e\" id=\"r"+td5+"\" value=\""+td5+"\" onClick=\"editG(this)\"><i class=\"far fa-edit fa-2x blue\"></i></button> <button class=\"btt-graf\" id=\"t"+td5+"\" value=\""+idmedM+"\" onClick=\"saveEditG(this)\" style=\"display:none\"><i class=\"fas fa-save fa-2x blue\"></i></button>";
              var but3="<button class=\"btt-graf\" id=\"y"+td5+"\" value=\""+idmedM+"\" onClick=\"alertaG(this)\"><i class=\"fas fa-trash fa-2x blue\"></i></button></td>";
              $(".bodygra").append("<tr id=\""+con+"\">"+pri+seg+but12+but3+"</tr>");
              var imN=td5;
              document.getElementById("img").value=imN;
              //document.getElementById("ultimoMg").value=idmedM;
        alert("Datos guardados");

            }
         });

     });


  //-----------editar
     function editG(element){
       //se toma el valor del boton((nn es el total) se le restan cuatro por los campos dados) y usamos el id para quitarlo despues
       var nn=element.getAttribute("value");
       var ide=element.getAttribute("id");
       var ii=nn-2;
       //alert(nn+ii);
       //se concatena para obtener el id especifico y luego se desplegamos un boton y escondemos el otro
       var t="t";
       var tt=t.concat(nn);
       document.getElementById(ide).style.display="none";
       document.getElementById(tt).style.display="inline-block";
       document.getElementById("eddig").value=nn;
       //se hace un ciclo para activar los input
       for (var i = ii; i <= nn; i++) {
         document.getElementById(i).disabled = false;
         //document.getElementById(i).size = "13";
         //document.getElementById(i).col = "13";
       }
     }

     function saveEditG(element){
       //se toma el valor del boton((nn es el total) se le restan cuatro por los campos dados) y id
   var nn=document.getElementById("eddig").value;
   var ide=element.getAttribute("id");
   var idm=element.getAttribute("value");
   var idpaci = $("input[name='idpacig']").val();
   var idp = $("input[name='idpg']").val();
   var ii=nn-2;
   var kk=ii;
   //alert(kk+" "+nn);
   //se concatena para obtener el id especifico y luego se desplegamos un boton y escondemos el otro
  var r="r";
  var rr=r.concat(nn);
  //se hace un ciclo para activar los input
   var uno=ii;
   var dos=++ii;
   var glu=document.getElementById(uno).value;
   var fecha=document.getElementById(dos).value;

   //alert(nombre+dosis+horario+observa);
   // verificamos que los campos no esten vacios (para ciertos campos: nombre, dosis, horario)
   //se actvara el tooltip y se marcara en rojo la casilla erronea
   //con style.backgroundColor cambiamos el color del campo y title es para el texto que mostrara el tooltip
   //opction de tooltip (enable) para activar, (show) para mostrar, (dispose) para desactivar-borrar

   //verifica que los 3 campos esten llenos y luego ejecuta el ajax

     $.ajax({
          url: 'http://localhost/diabeproto/Welcome/editarDatgraf',
          type: 'POST',
          data: {idm: idm, glu: glu, fecha: fecha, idpaci: idpaci, idp: idp, ide: ide, rr: rr, kk: kk, nn: nn},
          error: function() {
             alert('hubo algun error');
          },
          success: function(data) {
              alert("Datos editados");
            document.getElementById(ide).style.display="none";
            document.getElementById(rr).style.display="inline-block";
            for (var p = kk; p <= nn; p++) {
              document.getElementById(p).disabled = true;
              //document.getElementById(p).size = "13";
              //document.getElementById(i).col = "13";
            }

          }
     });


     }



  //----------------borrar
  function alertaG(elemento)
   {
   //se tomo el valor del boton y se guardo en input
   var val=elemento.getAttribute("value");
   document.getElementById("checkg").value=val;
   //alert(val);
   var opcion = confirm("Seguro que quiere eliminar");
   //se pide confirmacion y si es aceptado toma el valor del input y se ejecuta el ajax
   if (opcion == true) {
     var idde=document.getElementById("checkg").value;
     $.ajax({
        url: 'http://localhost/diabeproto/Welcome/deletedatgraf',
        type: 'POST',
        data: {idde: idde},
        error: function() {
           alert('hubo algun error');
        },
        success: function(data) {
          var con="graf"+idde;
          //funcion para remover elementos
          $("#"+con).remove();
          //document.getElementById(con).style.display="none";
             alert("Datos eliminados");
        }
     });
  }
  }
  //<!--______________________________funciones para seccion de citas_____________________________-->
  //-------------------fechas-----------------------------
  function creacita(ele){
    //se tomo el valor del boton y se guardo en input
    var fechat = $("input[name='fechacita']").val();
    var citapac = $("select[name='pacientescita']").val();
  //alert(citapac);


  if((new String(fechat).valueOf() != new String("").valueOf()) && (new String(citapac).valueOf() != new String("").valueOf()) ){
    $.ajax({
       url: 'http://localhost/diabeproto/Welcome/agregarcitas',
       type: 'POST',
       data: {fechat: fechat, citapac: citapac},
       error: function() {
          alert('hubo algun error');
       },
       success: function(data) {
            var idCita=data;
            //var con=idCita;
            //alert(td1+","+td2+","+td3+","+td4+","+td5+","+idmedM);
            lab1="<label class=\"font-min\">"+fechat+"</label>";
            lab2="<label class=\"font-min\">"+citapac+"</label>";
            var pri="<div class=\"col-8 border\" id=\"dcit"+idCita+"\">"+lab1+lab2+" </div>";
              var but="<div class=\"col-4\" id=\"bcit"+idCita+"\"><button value=\""+idCita+"\" onClick=\"delcita(this)\"><i class=\"fas fa-trash fa-1x blue\"></i></button></div>";
            $("#tablacita").append(pri+but);
            alert('Cita guardada');
       }
    });
  }else {
    alert('faltan campos por llenar');
  }

  }

  function delcita(ele){

    var idde = ele.value;
    //alert(idde);
    var opcion = confirm("Seguro que quiere eliminar");
    //se pide confirmacion y si es aceptado toma el valor del input y se ejecuta el ajax
    if (opcion == true) {
      //var idde=document.getElementById("checkg").value;
      $.ajax({
         url: 'http://localhost/diabeproto/Welcome/deletecita',
         type: 'POST',
         data: {idde: idde},
         error: function() {
            alert('hubo algun error');
         },
         success: function(data) {
           var con="dcit"+idde;
           var con1="bcit"+idde;
           //funcion para remover elementos
           $("#"+con).remove();
            $("#"+con1).remove();
           //document.getElementById(con).style.display="none";
              alert("cita eliminada");
         }
      });
   }

  }


  function dateCita(){
  //var fechat document.getElementById("inputcalendar").value;
  a//lert('hola');
  var fechat = $("label[class='pruebacitas']").val();
  //var fechauno = new Date();
  //var fechados = new Date(fechat);
  var Hoy = new Date();//Fecha actual del sistema

  var Fecha1 = new Date(fechat);
  var AnyoFecha = Fecha1.getFullYear();
  var MesFecha = Fecha1.getMonth();
  var DiaFecha = Fecha1.getDate();
  var HoraFecha=Fecha1.getHours();
  var MinFecha=Fecha1.getMinutes();

  var AnyoHoy = Hoy.getFullYear();
  var MesHoy = Hoy.getMonth();
  var DiaHoy = Hoy.getDate();
  var HoraHoy=Hoy.getHours();
  var MinHoy=Hoy.getMinutes();
  //alert(Fecha1);

              if (AnyoFecha == AnyoHoy && MesFecha == MesHoy && DiaFecha == DiaHoy && HoraFecha == HoraHoy && MinFecha == MinHoy){
                   alert ("Has introducido la fecha de Hoy");
              }

  //alert(fecha1);
  }

  //------seccion de registro----------------------
  //-----------------------------------------------
  //-----------------------------------------------

  function butEnab(ele){
    //alert("hola");
    document.getElementById("guardaficha").disabled = false;
  }

  function validar_email( email )
  {
      var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email) ? true : false;
  }


  function guardaficha(){
    //se obtiene los valores requeridos
     var nombre = $("input[name='nombre']").val();
     var fecha = $("input[name='fecha']").val();
     var edad = $("input[name='edad']").val();
     var nacimiento = $("input[name='nacimiento']").val();
     var genero = $("input[name='genero']:checked").val();
     var residencia = $("input[name='residencia']").val();
     var domicilio = $("input[name='domicilio']").val();
     var ocupacion = $("input[name='ocupacion']").val();
     var estado = $("input[name='estado']:checked").val();
     var escolaridad = $("input[name='escolaridad']").val();
     var religion = $("input[name='religion']").val();
     var telefono = $("input[name='telefono']").val();
     var correo = $("input[name='correo']").val();
     //var idmed = $("input[name='ultimoM']").val();;
     var idcorreo=0;
     //alert(genero+estado);
     // verificamos que los campos no esten vacios (para ciertos campos: nombre, dosis, horario)
     //se actvara el tooltip y se marcara en rojo la casilla erronea
     //con style.backgroundColor cambiamos el color del campo y title es para el texto que mostrara el tooltip
     //opction de tooltip (enable) para activar, (show) para mostrar, (dispose) para desactivar-borrar
     //------------nombre --------------------------------------
     if(new String(nombre).valueOf() == new String("").valueOf()){
       document.getElementById("nombre").style.backgroundColor = "red";
       document.getElementById("nombre").style.color = "white";
       document.getElementById("nombre").title="campo vacio";
       document.getElementById("nombre").focus();
       $('#nombre').tooltip('enable');
       $('#nombre').tooltip('show');
     }else{

       document.getElementById("nombre").style.backgroundColor = "white";
       document.getElementById("nombre").style.color = "black";
        document.getElementById("nombre").title="";
        $('#nombre').tooltip('dispose');
     }
     //---------fecha--------------------------------
     if(new String(fecha).valueOf() == new String("").valueOf()){
       document.getElementById("fecha").style.backgroundColor = "red";
       document.getElementById("fecha").style.color = "white";
       document.getElementById("fecha").title="campo vacio";
       document.getElementById("fecha").focus();
       $('#fecha').tooltip('enable');
       $('#fecha').tooltip('show');
     }else{
       document.getElementById("fecha").style.backgroundColor = "white";
       document.getElementById("fecha").style.color = "black";
        document.getElementById("fecha").title="";
        $('#fecha').tooltip('dispose');
     }
     //--------edad--------------------------------------------
     if(new String(edad).valueOf() == new String("").valueOf()){
       document.getElementById("edad").style.backgroundColor = "red";
       document.getElementById("edad").style.color = "white";
       document.getElementById("edad").title="campo vacio";
       document.getElementById("edad").focus();
       $('#edad').tooltip('enable');
       $('#edad').tooltip('show');
     }else{
       document.getElementById("edad").style.backgroundColor = "white";
       document.getElementById("edad").style.color = "black";
        document.getElementById("edad").title="";
        $('#edad').tooltip('dispose');
     }
     //--------nacimiento--------------------------------------------
     if(new String(nacimiento).valueOf() == new String("").valueOf()){
       document.getElementById("nacimiento").style.backgroundColor = "red";
       document.getElementById("nacimiento").style.color = "white";
       document.getElementById("nacimiento").title="campo vacio";
       document.getElementById("nacimiento").focus();
       $('#nacimiento').tooltip('enable');
       $('#nacimiento').tooltip('show');
     }else{
       document.getElementById("nacimiento").style.backgroundColor = "white";
       document.getElementById("nacimiento").style.color = "black";
        document.getElementById("nacimiento").title="";
        $('#nacimiento').tooltip('dispose');
     }
     //--------genero--------------------------------------------
     /*if(new String(genero).valueOf() == new String("").valueOf()){
       document.getElementById("genero").style.backgroundColor = "red";
       document.getElementById("genero").style.color = "white";
       document.getElementById("genero").title="campo vacio";
       $('#genero').tooltip('enable');
       $('#genero').tooltip('show');
     }else{
       document.getElementById("genero").style.backgroundColor = "white";
       document.getElementById("genero").style.color = "black";
        document.getElementById("genero").title="";
        $('#genero').tooltip('dispose');
     }
     */
     //--------residencia--------------------------------------------
     if(new String(residencia).valueOf() == new String("").valueOf()){
       document.getElementById("residencia").style.backgroundColor = "red";
       document.getElementById("residencia").style.color = "white";
       document.getElementById("residencia").title="campo vacio";
       document.getElementById("residencia").focus();
       $('#residencia').tooltip('enable');
       $('#residencia').tooltip('show');
     }else{
       document.getElementById("residencia").style.backgroundColor = "white";
       document.getElementById("residencia").style.color = "black";
        document.getElementById("residencia").title="";
        $('#residencia').tooltip('dispose');
     }
     //--------domicilio--------------------------------------------
     if(new String(domicilio).valueOf() == new String("").valueOf()){
       document.getElementById("domicilio").style.backgroundColor = "red";
       document.getElementById("domicilio").style.color = "white";
       document.getElementById("domicilio").title="campo vacio";
       document.getElementById("domicilio").focus();
       $('#domicilio').tooltip('enable');
       $('#domicilio').tooltip('show');
     }else{
       document.getElementById("domicilio").style.backgroundColor = "white";
       document.getElementById("domicilio").style.color = "black";
        document.getElementById("domicilio").title="";
        $('#domicilio').tooltip('dispose');
     }
     //--------ocupacion--------------------------------------------
     if(new String(ocupacion).valueOf() == new String("").valueOf()){
       document.getElementById("ocupacion").style.backgroundColor = "red";
       document.getElementById("ocupacion").style.color = "white";
       document.getElementById("ocupacion").title="campo vacio";
       document.getElementById("ocupacion").focus();
       $('#ocupacion').tooltip('enable');
       $('#ocupacion').tooltip('show');
     }else{
       document.getElementById("ocupacion").style.backgroundColor = "white";
       document.getElementById("ocupacion").style.color = "black";
        document.getElementById("ocupacion").title="";
        $('#ocupacion').tooltip('dispose');
     }
     //--------estado--------------------------------------------
     /*if(new String(estado).valueOf() == new String("").valueOf()){
       document.getElementById("estado").style.backgroundColor = "red";
       document.getElementById("estado").style.color = "white";
       document.getElementById("estado").title="campo vacio";
       $('#estado').tooltip('enable');
       $('#estado').tooltip('show');
     }else{
       document.getElementById("estado").style.backgroundColor = "white";
       document.getElementById("estado").style.color = "black";
        document.getElementById("estado").title="";
        $('#estado').tooltip('dispose');
     }
     */
     //--------escolaridad--------------------------------------------
     if(new String(escolaridad).valueOf() == new String("").valueOf()){
       document.getElementById("escolaridad").style.backgroundColor = "red";
       document.getElementById("escolaridad").style.color = "white";
       document.getElementById("escolaridad").title="campo vacio";
       document.getElementById("escolaridad").focus();
       $('#escolaridad').tooltip('enable');
       $('#escolaridad').tooltip('show');
     }else{
       document.getElementById("escolaridad").style.backgroundColor = "white";
       document.getElementById("escolaridad").style.color = "black";
        document.getElementById("escolaridad").title="";
        $('#escolaridad').tooltip('dispose');
     }
     //--------religion--------------------------------------------
     if(new String(religion).valueOf() == new String("").valueOf()){
       document.getElementById("religion").style.backgroundColor = "red";
       document.getElementById("religion").style.color = "white";
       document.getElementById("religion").title="campo vacio";
       document.getElementById("religion").focus();
       $('#religion').tooltip('enable');
       $('#religion').tooltip('show');
     }else{
       document.getElementById("religion").style.backgroundColor = "white";
       document.getElementById("religion").style.color = "black";
        document.getElementById("religion").title="";
        $('#religion').tooltip('dispose');
     }
     //--------telefono--------------------------------------------
     if(new String(telefono).valueOf() == new String("").valueOf()){
       document.getElementById("telefono").style.backgroundColor = "red";
       document.getElementById("telefono").style.color = "white";
       document.getElementById("telefono").title="campo vacio";
       document.getElementById("telefono").focus();
       $('#telefono').tooltip('enable');
       $('#telefono').tooltip('show');
     }else{
       document.getElementById("telefono").style.backgroundColor = "white";
       document.getElementById("telefono").style.color = "black";
        document.getElementById("telefono").title="";
        $('#telefono').tooltip('dispose');
     }
     //--------correo--------------------------------------------
     if(new String(correo).valueOf() == new String("").valueOf()){
       document.getElementById("correo").style.backgroundColor = "red";
       document.getElementById("correo").style.color = "white";
       document.getElementById("correo").title="campo vacio";
       document.getElementById("correo").focus();
       $('#correo').tooltip('enable');
       $('#correo').tooltip('show');
     }else{
       document.getElementById("correo").style.backgroundColor = "white";
       document.getElementById("correo").style.color = "black";
        document.getElementById("correo").title="";
        $('#correo').tooltip('dispose');
          if(validar_email(correo)){
            idcorreo=1;
          }else{
            document.getElementById("correo").style.backgroundColor = "red";
            document.getElementById("correo").style.color = "white";
            document.getElementById("correo").title="el email es incorrecto";
            $('#correo').tooltip('enable');
            $('#correo').tooltip('show');
          }
     }



     //verifica que los 3 campos esten llenos y luego ejecuta el ajax
     if((new String(nombre).valueOf() != new String("").valueOf()) && (new String(edad).valueOf() != new String("").valueOf()) &&
     (new String(fecha).valueOf() != new String("").valueOf()) && (new String(nacimiento).valueOf() != new String("").valueOf()) && (new String(genero).valueOf() != new String("").valueOf()) &&
     (new String(residencia).valueOf() != new String("").valueOf()) && (new String(domicilio).valueOf() != new String("").valueOf()) && (new String(ocupacion).valueOf() != new String("").valueOf()) &&
     (new String(estado).valueOf() != new String("").valueOf()) && (new String(escolaridad).valueOf() != new String("").valueOf()) && (new String(religion).valueOf() != new String("").valueOf()) &&
     (new String(telefono).valueOf() != new String("").valueOf()) && idcorreo==1 ){
      $.ajax({
         url: 'http://localhost/diabeproto/Welcome/insertarBD',
         type: 'POST',
         data: { nombre: nombre, fecha: fecha, edad: edad, nacimiento: nacimiento, genero: genero, residencia: residencia, domicilio: domicilio, ocupacion:ocupacion, estado:estado, escolaridad:escolaridad, religion:religion, telefono:telefono, correo:correo},
         error: function() {
            alert('hubo algun error');
         },
         success: function(data) {

            alert("datos registrados");
            window.location.replace("http://localhost/diabeproto/Welcome/registropas/"+data);
         }
      });
    }
  }

  //------------------------------------------------------------------------------------
  //------------------------------------------------------------------------------------
  //------------------------------------------------------------------------------------
  //---------------paciente-------------------------
  function butEnab2(ele){
    //alert("hola");
    document.getElementById("guardafichapaci").disabled = false;
  }

  function guardafichapaci(){
    //se obtiene los valores requeridos
     var diagnostico = $("input[name='diagnostico']").val();
     var padecimiento = $("input[name='padecimiento']").val();
     var exploracion = $("input[name='exploracion']").val();
     var notas = $("input[name='notas']").val();
     var interconsultas = $("input[name='interconsultas']").val();
     var idp = $("input[name='idp']").val();

     //var idmed = $("input[name='ultimoM']").val();;
     //var idcorreo=0;
     //alert(genero+estado);
     // verificamos que los campos no esten vacios (para ciertos campos: nombre, dosis, horario)
     //se actvara el tooltip y se marcara en rojo la casilla erronea
     //con style.backgroundColor cambiamos el color del campo y title es para el texto que mostrara el tooltip
     //opction de tooltip (enable) para activar, (show) para mostrar, (dispose) para desactivar-borrar
     //------------diagnostico --------------------------------------
     if(new String(diagnostico).valueOf() == new String("").valueOf()){
       document.getElementById("diagnostico").style.backgroundColor = "red";
       document.getElementById("diagnostico").style.color = "white";
       document.getElementById("diagnostico").title="campo vacio";
       document.getElementById("diagnostico").focus();
       $('#diagnostico').tooltip('enable');
       $('#diagnostico').tooltip('show');
     }else{

       document.getElementById("diagnostico").style.backgroundColor = "white";
       document.getElementById("diagnostico").style.color = "black";
        document.getElementById("diagnostico").title="";
        $('#diagnostico').tooltip('dispose');
     }
     //---------padecimiento--------------------------------
     if(new String(padecimiento).valueOf() == new String("").valueOf()){
       document.getElementById("padecimiento").style.backgroundColor = "red";
       document.getElementById("padecimiento").style.color = "white";
       document.getElementById("padecimiento").title="campo vacio";
       document.getElementById("padecimiento").focus();
       $('#padecimiento').tooltip('enable');
       $('#padecimiento').tooltip('show');
     }else{
       document.getElementById("padecimiento").style.backgroundColor = "white";
       document.getElementById("padecimiento").style.color = "black";
        document.getElementById("padecimiento").title="";
        $('#padecimiento').tooltip('dispose');
     }

     //verifica que los 3 campos esten llenos y luego ejecuta el ajax
     if((new String(diagnostico).valueOf() != new String("").valueOf()) && (new String(padecimiento).valueOf() != new String("").valueOf())){
      $.ajax({
         url: 'http://localhost/diabeproto/Welcome/insertarpasBD',
         type: 'POST',
         data: {idp:idp, diagnostico: diagnostico, padecimiento: padecimiento, exploracion:exploracion, notas:notas, interconsultas:interconsultas},
         error: function() {
            alert('hubo algun error');
         },
         success: function(data) {

            alert("datos registrados");
            window.location.replace("http://localhost/diabeproto/Welcome/registroant/"+data);
         }
      });
    }
  }
  //------------------------------------------------------------------------------------
  //------------------------------------------------------------------------------------
  //------------------------------------------------------------------------------------
  //---------------antecedentes-------------------------
  function butEnab3(ele){
    //alert("hola");
    document.getElementById("guardafichaante").disabled = false;
  }

  function guardafichaante(){
    //se obtiene los valores requeridos
     var idPersona = $("input[name='idPersona']").val();
     var idpas = $("input[name='idpas']").val();
     var Diabetes = $("input:checkbox[name='Diabetes']:checked").val();
     var Cardiopatia = $("input:checkbox[name='Cardiopatia']:checked").val();
     var Hipotiroidismo = $("input:checkbox[name='Hipotiroidismo']:checked").val();
     var Dislipidemia = $("input:checkbox[name='Dislipidemia']:checked").val();
     var Hepaticas = $("input:checkbox[name='Hepaticas']:checked").val();
     var Convulsion = $("input:checkbox[name='Convulsion']:checked").val();
     var Cancer = $("input:checkbox[name='Cancer']").val();
     var Transfusionales = $("input:checkbox[name='Transfusionales']:checked").val();
     var Quirurgicos = $("input:checkbox[name='Quirurgicos']:checked").val();
     var Hipertension = $("input:checkbox[name='Hipertension']:checked").val();
     var Insuficiencia = $("input:checkbox[name='Insuficiencia']:checked").val();
     var Hipertiroidismo = $("input:checkbox[name='Hipertiroidismo']:checked").val();
     var Marcapasos = $("input:checkbox[name='Marcapasos']:checked").val();
     var Renales = $("input:checkbox[name='Renales']:checked").val();
     var Artritis = $("input:checkbox[name='Artritis']:checked").val();
     var Dentales = $("input:checkbox[name='Dentales']:checked").val();
     var Traumaticos = $("input:checkbox[name='Traumaticos']:checked").val();
     var Alergico = $("input:checkbox[name='Alergico']:checked").val();
     var Peso = $("input:checkbox[name='Peso']:checked").val();
     var Obesidad = $("input:checkbox[name='Obesidad']:checked").val();
     var Dieta = $("input:checkbox[name='Dieta']:checked").val();
     var Ejercicio = $("input:checkbox[name='Ejercicio']:checked").val();
     var Fumar = $("input:checkbox[name='Fumar']:checked").val();
     var Bebida = $("input:checkbox[name='Bebida']:checked").val();
     var Control = $("input:checkbox[name='Control']:checked").val();
     var Drogas = $("input:checkbox[name='Drogas']:checked").val();
     var Humo = $("input:checkbox[name='Humo']:checked").val();
     var Viaje = $("input:checkbox[name='Viaje']:checked").val();
     var Trabajados = $("input:checkbox[name='Trabajados']:checked").val();
     var Casa = $("input:checkbox[name='Casa']:checked").val();
     var Sangre = $("input:checkbox[name='Sangre']:checked").val();
     var Mascotas = $("input:checkbox[name='Mascotas']:checked").val();
     var Seguro = $("input:checkbox[name='Seguro']:checked").val();
     var Vacunacion = $("input:checkbox[name='Vacunacion']:checked").val();
     var Desparacitacion = $("input:checkbox[name='Desparacitacion']:checked").val();
     var Parentesco = $("input[name='Parentesco']").val();
     var vivir = $("input:radio[name='vivir']:checked").val();
     var Enfermedad = $("input[name='Enfermedad']").val();
     //var idmed = $("input[name='ultimoM']").val();;
     //var idcorreo=0;
     if (Diabetes==null) {
       Diabetes = "";
     }
     if (Cardiopatia==null) {
       Cardiopatia = "";
     }
     if (Hipotiroidismo==null) {
       Hipotiroidismo = "";
     }
     if (Dislipidemia==null) {
       Dislipidemia = "";
     }
     if (Hepaticas==null) {
       Hepaticas = "";
     }
     if (Convulsion==null) {
       Convulsion = "";
     }
     if (Cancer==null) {
       Cancer = "";
     }
     if (Transfusionales==null) {
       Transfusionales = "";
     }
     if (Quirurgicos==null) {
       Quirurgicos = "";
     }
     if (Hipertension==null) {
       Hipertension = "";
     }
     if (Insuficiencia==null) {
       Insuficiencia = "";
     }
     if (Hipertiroidismo==null) {
       Hipertiroidismo = "";
     }
     if (Marcapasos==null) {
       Marcapasos = "";
     }
     if (Renales==null) {
       Renales = "";
     }
     if (Artritis==null) {
       Artritis = "";
     }
     if (Dentales==null) {
       Dentales = "";
     }
     if (Traumaticos==null) {
       Traumaticos = "";
     }
     if (Alergico==null) {
       Alergico = "";
     }
     if (Peso==null) {
       Peso = "";
     }
     if (Obesidad==null) {
       Obesidad = "";
     }
     if (Dieta==null) {
       Dieta = "";
     }
     if (Ejercicio==null) {
       Ejercicio = "";
     }
     if (Fumar==null) {
       Fumar = "";
     }
     if (Bebida==null) {
       Bebida = "";
     }
     if (Control==null) {
       Control = "";
     }
     if (Drogas==null) {
       Drogas = "";
     }
     if (Humo==null) {
       Humo = "";
     }
     if (Viaje==null) {
       Viaje = "";
     }
     if (Trabajados==null) {
       Trabajados = "";
     }
     if (Casa==null) {
       Casa = "";
     }
     if (Sangre==null) {
       Sangre = "";
     }
     if (Mascotas==null) {
       Mascotas = "";
     }
     if (Seguro==null) {
       Seguro = "";
     }
     if (Vacunacion==null) {
      Vacunacion = "";
     }
     if (Desparacitacion==null) {
       Desparacitacion = "";
     }
     // verificamos que los campos no esten vacios (para ciertos campos: nombre, dosis, horario)
     //se actvara el tooltip y se marcara en rojo la casilla erronea
     //con style.backgroundColor cambiamos el color del campo y title es para el texto que mostrara el tooltip
     //opction de tooltip (enable) para activar, (show) para mostrar, (dispose) para desactivar-borrar
     //verifica que los 3 campos esten llenos y luego ejecuta el ajax

      $.ajax({
         url: 'http://localhost/diabeproto/Welcome/insertarant',
         type: 'POST',
         data:
         {idPersona:idPersona,idpas:idpas,Diabetes:Diabetes,Cardiopatia:Cardiopatia,Hipotiroidismo:Hipotiroidismo,Dislipidemia:Dislipidemia,Hepaticas:Hepaticas,
          Convulsion:Convulsion,Cancer:Cancer,Transfusionales:Transfusionales,Quirurgicos:Quirurgicos,Hipertension:Hipertension,Insuficiencia:Insuficiencia,
          Hipertiroidismo:Hipertiroidismo,Marcapasos:Marcapasos,Renales:Renales,Artritis:Artritis,Dentales:Dentales,Traumaticos:Traumaticos,
          Alergico:Alergico,Peso:Peso,Obesidad:Obesidad,Dieta:Dieta,Ejercicio:Ejercicio,Fumar:Fumar,Bebida:Bebida,Control:Control,Drogas:Drogas,
          Humo:Humo,Viaje:Viaje,Trabajados:Trabajados,Casa:Casa,Sangre:Sangre,Mascotas:Mascotas,Seguro:Seguro,Vacunacion:Vacunacion,Desparacitacion:Desparacitacion,
          Parentesco:Parentesco,vivir:vivir,Enfermedad:Enfermedad},
         error: function() {
            alert('hubo algun error');
         },
         success: function(data) {

            alert(data);

            var opcion = confirm("Añadir una especialidad");
            if(opcion==true){
            window.location.replace("http://localhost/diabeproto/Welcome/agregar");
          }else {
            window.location.replace("http://localhost/diabeproto/Welcome/listado");
          }

         }
      });

  }
