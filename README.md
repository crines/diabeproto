﻿# App DiabeProto

Aplicación :office:


## Herramientas utilizadas:

  * **CodeIgniter 3.x** -
  CodeIgniter is a powerful PHP framework with a very small footprint, built for developers who need a simple and elegant toolkit to create full-featured web applications.

      [Descargar](https://github.com/bcit-ci/CodeIgniter/archive/3.1.8.zip)
  [Documentación](https://www.codeigniter.com/docs)
  [Query results](https://www.codeigniter.com/userguide2/database/results.html)

  * **Bootstrap 4** -
  Bootstrap is an open source toolkit for developing with HTML, CSS, and JS.

      [Descargar](https://github.com/twbs/bootstrap/releases/download/v4.1.2/bootstrap-4.1.2-dist.zip)
  [Documentación](https://getbootstrap.com/docs/4.1/getting-started/introduction/)

  * **Chart.js** -
  Simple yet flexible JavaScript charting for designers & developers

      [Descargar](https://github.com/chartjs/Chart.js)
  [Documentación](http://www.chartjs.org/docs/latest/)

  * **PHP 5.6.x**

  * **Animate.css** -
  animate.css is a bunch of cool, fun, and cross-browser animations for you to use in your projects. Great for emphasis, home pages, sliders, and general just-add-water-awesomeness.

      [Descargar](https://raw.githubusercontent.com/daneden/animate.css/master/animate.css)
  [Basic Usage](https://github.com/daneden/animate.css)





  * Markdown [Tutorial](https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf)
